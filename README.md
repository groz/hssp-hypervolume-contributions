# Programs for Hypervolume Computations

C++14 code for : 
- hssp in dimension d=2
- hssp for very specific cases
- computing hypervolume contributions in dimension d=3
- computing minimal-weight triples in weighted hypergraphs

(HSSP = Hypervolume Subset Selection Problem).


# Compilation

Requires C++14 with the boost library installed.

Compilation instruction:
(from within the src directory)
g++ -std=c++1y -O3 -I . main.cpp


# External code

The shark directory includes the shark ML library (version 3.1 from 2016). 
This is a 3rd party software to which we compare one of our program.
While this makes a large part of this code repository, we did not write these pr
ograms 
and in fact we only use a small fraction of these programs; the ones that deal w
ith hypervolume computations. 
The programs in shark are only used from
hssp2d/sharklibrary_hssp2d.cpp 
so they can be left out if that file above is commented out.


# License

The source code is provided as-is under an MIT License. If it is useful to you,
please cite the [paper][1]


[1]: "B.Groz, S.Maniu. Hypervolume Subset Selection with Small Subsets, to appear in Evolutionary Computation"
