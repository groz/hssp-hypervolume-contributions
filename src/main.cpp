#define NDEBUG
#include <iostream>
#include "utils/print_helpers.hpp"
#include "utils/time_utils.hpp"
#include "experiments/req1_hssp2d_runtime_varying_k.hpp"
#include "experiments/req1_hssp2d_runtime_varying_n.hpp"
#include "experiments/req1_hssp2d_runtime_n100000_k100.hpp"
#include "experiments/req2_hssp_k2d3_runtime_varying_truncsize.hpp"
#include "experiments/req2_hssp_k2d3_runtime_varying_n.hpp"
#include "experiments/req3_hssp_k2d4_runtime_varying_n.hpp"
#include "experiments/req3_hssp_k2d4_runtime_varying_n_and_truncsize.hpp"
#include "experiments/req4_hssp_contribs_runtime_varying_n.hpp"
#include "experiments/req5_hssp_contribs_varying_depth_runtime.hpp"
#include "experiments/req6_hssp_largek_pairs_triples.hpp"
#include "weighted_hypergraph/min_triple.h"
#include "tests/test_contribution_stability_rotation.hpp"
#include "tests/test_hull.hpp"
#include "tests/test_updatable_hull.hpp"
#include "tests/test_hssp_2d.hpp"
#include "tests/test_contrib_shark.hpp"
#include "tests/test_rangetree.hpp"
#include "tests/test_triples.hpp"

int main() {
// tests::test_hsspd3_contrib();
// std::cout<<tests::test_contrib_shark();
// std::cout<<tests::test_hssp_2d_small_instance();
// std::cout << tests::test_hull_small_instance<double>();
// std::cout << tests::test_updatable_hull_small_instance<double>();
// std::cout << tests::test_hull_small_instance<int>();

//
// experiment::running_time_hssp2d_varying_k();
// experiment::running_time_hssp2d_varying_n();
// experiment::running_time_hssp2d_n100000_k100();
// experiment::running_time_hsspk2d3_varying_truncsize();
// experiment::running_time_hsspk2d3_range_varying_n();
// experiment::running_time_hsspk2d4_range_varying_n_and_truncsize();
// experiment::running_time_hsspk2d4_range_varying_n();
// experiment::running_time_hsspd3_contrib_varying_n_d();
// experiment::running_time_hsspd3_contrib_varying_depth();
// experiment::running_time_hssp_largek();
// experiment::running_time_hsspd3_contrib_struct();
}
