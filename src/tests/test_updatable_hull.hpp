/*
* test_rangetree.hpp
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef TEST_UPDATABLE_HULL_H_
#define TEST_UPDATABLE_HULL_H_

// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <cstdlib>

#include "utils/generators.hpp"
#include "utils/naive_implementations/naive_hssp.h"
#include "utils/parsers.hpp"
#include "utils/print_helpers.hpp"
#include "utils/skyline.h"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"



// #include "hull/cvxhull_struct.hpp"
// #include "hull/cvxhull_double_struct.hpp"
#include "hull/upperhull_updatable_double_naive.hpp"
#include "utils/point_representation.hpp"


namespace tests {



template <typename hullp>
bool samecoord(const hullp &h1, const hullp &h2) {return h1.coord0 == h2.coord0 && h1.coord1 == h2.coord1;};

template <typename ValueType>
bool test_updatable_hull_small_instance() {
	typedef utils::Hull_Point_With_Slope<ValueType> hullp;
	typedef std::vector<hullp> Container;
	typedef utils::Point2d<ValueType> HullQueryPoint;
	typedef typename hull::UpperHull_Set2<ValueType, hullp, HullQueryPoint> Hull_class;
	
	double max_val = std::numeric_limits<double>::max();
	
	bool correct = true;
	hullp p1 = {-9,4,2,max_val};// <-
	hullp p2 = {-8,5,15,max_val};
	hullp p3 = {-8,6,15,max_val};
	hullp p4 = {-7,10,14,max_val};// <- left slope: 3
	hullp p5 = {-6,11,100,max_val};
	hullp p6 = {-5,13,14,max_val};// <- left slope: 1.5
	hullp p7 = {0,14,14,max_val};// <- left slope: .2
	Container skyvec = {p1, p2, p3, p4, p5, p6, p7};
	auto h = Hull_class();
	for (auto p : skyvec) {
		h.add_point(p);
	}
	if (h.get_hull_points().size() != 4) {
		correct = false;
	} else if (!(samecoord(h.get_hull_points()[0], p1) && samecoord(h.get_hull_points()[1], p4) && samecoord(h.get_hull_points()[2], p6) && samecoord(h.get_hull_points()[3], p7))) {
		correct = false;
	}
	for (auto p: h.get_hull_points()) {
		std::cout<<point_to_string(p)<<std::endl;
	}

	HullQueryPoint q1 = {-10,1};
	HullQueryPoint q2 = {-1,1};
	HullQueryPoint q3 = {-1,10};
	if (!(samecoord(h.Extreme(q1), p1) && samecoord(h.Extreme(q2), p6) && samecoord(h.Extreme(q3), p7))) correct = false;
	return correct;
}

}  // namespace tests


#endif /*TEST_UPDATABLE_HULL_H_*/