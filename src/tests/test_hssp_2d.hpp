/*
* test_hssp_2d.hpp
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef TEST_HSSP2D_H_
#define TEST_HSSP2D_H_

// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <utility>

#include "hssp2d/contrib_matrix.hpp"
#include "hssp2d/hssp2d_extreme.hpp"
#include "hssp2d/hssp2d_smawk.hpp"
#include "shark_externals/interface_with_shark.hpp"
#include "shark_externals/sharklibrary_hssp2d.cpp"
#include "utils/generators.hpp"
#include "utils/other_utils.hpp"
#include "utils/parsers.hpp"
#include "utils/print_helpers.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"



namespace tests {

bool test_hssp_2d_small_instance() {
	typedef double T;
	typedef std::array<T, 2> Point;
	typedef std::vector<Point> Container;
	
	bool correct = true;
	Point p1 = {1,22};
	Point p2 = {2,15};
	Point p3 = {4,10};
	Point p4 = {5,3};
	Point p5 = {10,1};
	Container skyvec = {p1, p2, p3, p4, p5};
	/* Total hypervolume of this instance: 22+15+10*2+3+1*5 = 65 */
	std::vector<std::size_t> result_indexes({});
	std::vector<std::size_t> result_indexes2({});
	
	for (int k: {1,2,3,4,5}) {
		result_indexes.clear();
		result_indexes2.clear();
		T res1 = hssp2d::hssp2dextremeValueOnly<Point>(skyvec, k);
		T res2 = hssp2d::hssp2dextreme<Point>(skyvec, result_indexes, k);
		T res3 = hssp2d::hssp2dsmawkValueOnly<Point>(skyvec, k);
		T res4 = hssp2d::hssp2dsmawk<Point>(skyvec, result_indexes2, k);

		if (res1 != res2 || res3 != res4 || (res1 + res3 != 65)) correct = false;
	}
	
	int k = 1;
	result_indexes.clear();
	T res = hssp2d::hssp2dextreme<Point>(skyvec, result_indexes, k);
	if (res != 40 || result_indexes != std::vector<std::size_t>({2})) correct = false;

	k = 2;
	result_indexes.clear();
	res = hssp2d::hssp2dextreme<Point>(skyvec, result_indexes, k);
	std::sort(result_indexes.begin(), result_indexes.end());
	// std::cout<<utils::to_string<std::vector<long>>(result_indexes)<<std::endl;;
	if (res != 52 || result_indexes != std::vector<std::size_t>({0,2})) correct = false;
	return correct;
}

}  // namespace tests


#endif /*TEST_HSSP2D_H_*/