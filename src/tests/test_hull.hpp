/*
* test_rangetree.hpp
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef TEST_HULL_H_
#define TEST_HULL_H_

// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <cstdlib>

#include "utils/generators.hpp"
#include "utils/naive_implementations/naive_hssp.h"
#include "utils/parsers.hpp"
#include "utils/print_helpers.hpp"
#include "utils/skyline.h"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"


#include "hull/cvxhull.hpp"
#include "hull/cvxhull_double.hpp"



namespace tests {

template <typename T>
struct HullPoint {
public:
	T coord0;
	T coord1;
	int index;
	HullPoint() {};
	HullPoint(const HullPoint &h): coord0(h.coord0), coord1(h.coord1), index(h.index) {};
	HullPoint(T x, T y, T i) : coord0(x), coord1(y), index(i) {};
	std::string to_string() {
		return std::to_string(coord0) + ", " + std::to_string(coord1) + ", " + std::to_string(index);
	}
};

template <typename T>
bool operator== (const HullPoint<T> &h1, const HullPoint<T> &h2) {return h1.coord0 == h2.coord0 && h1.coord1 == h2.coord1 && h1.index == h2.index;};

template <typename ValueType>
bool test_hull_small_instance() {
	typedef HullPoint<ValueType> hullp;
	typedef std::vector<hullp> Container;
	
	bool correct = true;
	hullp p1 = {-9,4,20};// <-
	hullp p2 = {-8,5,15};
	hullp p3 = {-8,6,15};
	hullp p4 = {-7,10,14};// <- left slope: 3
	hullp p5 = {-6,11,100};
	hullp p6 = {-5,13,14};// <- left slope: 1.5
	hullp p7 = {0,14,14};// <- left slope: .2
	Container skyvec = {p1, p2, p3, p4, p5, p6, p7};
	auto h = hull::ConvexHull<ValueType, hullp, hullp, Container>(skyvec);
	if (h.get_hull_points().size() != 4) {
		correct = false;
	} else if (!(h.get_hull_points()[0] == p1 && h.get_hull_points()[1] == p4 && h.get_hull_points()[2] == p6 && h.get_hull_points()[3] == p7)) {
		correct = false;
	}
	
	hullp q1 = {-10,1,1};
	hullp q2 = {-1,1,1};
	hullp q3 = {-1,10,1};
	// std::cout<<p.to_string()<<std::endl;
	// for (auto p: h.get_hull_points()) {
	// 	std::cout<<p.to_string()<<std::endl;
	// }
	if (!(h.Extreme(q1) == p1 && h.Extreme(q2) == p6 && h.Extreme(q3) == p7)) correct = false;
	return correct;
}

}  // namespace tests


#endif /*TEST_HULL_H_*/