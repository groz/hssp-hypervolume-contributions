/*
* test_rangetree.hpp
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef TEST_RANGETREE_H_
#define TEST_RANGETREE_H_

// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <cstdlib>

#include "utils/generators.hpp"
#include "utils/naive_implementations/naive_hssp.h"
#include "utils/parsers.hpp"
#include "utils/print_helpers.hpp"
#include "utils/skyline.h"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"

#include "rangetree/range_struct_trunc_array.hpp"
#include "rangetree/range_struct_trunc_array_inc.hpp"
#include "rangetree/range_struct_trunc_array_lower.hpp"
#include "rangetree/range_struct_trunc_array_lower_incremental.hpp"
#include "rangetree/range_struct_trunc_array_lower_incremental2.hpp"



namespace tests {

bool test_rangetree_small_instance() {
	typedef double T;
	typedef long IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	
	int truncsize = 1;
	bool correct = true;
	Point p1 = {10,22,20};
	Point p2 = {11,21,15};
	Point p3 = {30,10,14};
	Container skyvec = {p1, p2, p3};
	/* Total hypervolume of this instance: 22+15+10*2+3+1*5 = 65 */
	std::vector<IndexType> result_indexes({});
	std::vector<IndexType> result_indexes2({});
	
	// 10*22*20 = 4400
	// 30*10*14 = 4200
	// 8600- 10*10*14 = 7200
	std::tuple<Point, Point, T> res = rangetree::range_struct_lower<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
	if (res != std::make_tuple(p1,p3,7200) && res != std::make_tuple(p3,p1,7200)) correct = false;
	
	res = rangetree::range_struct_lower_inc<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
	if (res != std::make_tuple(p1,p3,7200) && res != std::make_tuple(p3,p1,7200)) correct = false;

	res = rangetree::range_struct<Point, Container>(skyvec.begin(), skyvec.end(), 3, truncsize);
	if (res != std::make_tuple(p1,p3,7200) && res != std::make_tuple(p3,p1,7200)) correct = false;
	
	return correct;
}
	
	
bool test_rangetree_mult_instance() {
	typedef double T;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints-req2.txt");
	std::string const outfilename("results-req2.csv");
	int dim = 3;
	
	int nbpoint_max = 100;
	int trunc_max = 10;
	int repet_max = 1;
	int seed_max = 4;
	bool correct = true;

	for (int truncsize = 1; truncsize <= trunc_max; truncsize *= 2) {
		for (int nbpoint = 2; nbpoint <= nbpoint_max; nbpoint += 10) {
			for (int seed = 1; seed <= seed_max; ++seed) {
				for (std::string distrib : {"distr_convex"}) {
				// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
					Container skyvec;
					if (distrib == "distr_hyperplane") {
						utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
					}
					if (distrib == "distr_convex") {
						utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					if (distrib == "distr_concave") {
						utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					utils::parse_file_to_list_of_arrays_size3(infilename, skyvec);

					for (int repet = 1; repet <= repet_max; ++ repet) {
						// rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "naive", distrib);
						// rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rlower", distrib);
						std::tuple<Point, Point, T> res_low_inc = rangetree::range_struct_lower_inc<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
						std::tuple<Point, Point, T> res_low = rangetree::range_struct_lower<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
						std::tuple<Point, Point, T> res_struct = rangetree::range_struct<Point, Container>(skyvec.begin(), skyvec.end(), dim, truncsize);
						std::tuple<Point, Point, T> res_struct_inc = rangetree::range_struct_inc<Point, Container>(skyvec.begin(), skyvec.end(), dim, truncsize);
						// std::tuple<Point, Point, T> res_low_inc2 = rangetree2::range_struct_lower_inc<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
					
						if (std::abs(std::get<2>(res_low_inc) - std::get<2>(res_low)) > .001) {correct = false;};
						// if (std::abs(std::get<2>(res_low_inc) - std::get<2>(res_struct)) > .001) {correct = false;};
						if (((std::get<0>(res_low_inc) != std::get<0>(res_low)) || (std::get<1>(res_low_inc) != std::get<1>(res_low)))
						&& ((std::get<0>(res_low_inc) != std::get<1>(res_low)) || (std::get<1>(res_low_inc) != std::get<0>(res_low)))) {
							std::cout<<"discrepancy 3d inc with 3d"<<seed<<", "<<truncsize<<", "<<nbpoint<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<0>(res_low_inc))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<1>(res_low_inc))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<0>(res_low))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<1>(res_low))<<std::endl;
							std::cout<<"lower_inc:"<<std::get<2>(res_low_inc)<<",lower: "<<std::get<2>(res_low)<<std::endl;
							std::pair<T, std::vector<Point>> res_naive = utils::naive_hssp_k2<Point>(skyvec, dim);
							std::cout<<"res_naive:"<<std::get<0>(res_naive)<<std::endl;
							correct = false;
						};
						if (((std::get<0>(res_low_inc) != std::get<0>(res_struct)) || (std::get<1>(res_low_inc) != std::get<1>(res_struct)))
						&& ((std::get<0>(res_low_inc) != std::get<1>(res_struct)) || (std::get<1>(res_low_inc) != std::get<0>(res_struct)))) {
							std::cout<<"discrepancy 3d inc with non3d"<<seed<<", "<<truncsize<<", "<<nbpoint<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<0>(res_low_inc))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<1>(res_low_inc))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<0>(res_struct))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<1>(res_struct))<<std::endl;
							std::cout<<std::get<2>(res_low_inc)<<", "<<std::get<2>(res_struct)<<std::endl;
							std::pair<T, std::vector<Point>> res_naive = utils::naive_hssp_k2<Point>(skyvec, dim);
							std::cout<<"res_naive:"<<std::get<0>(res_naive)<<std::endl;
							correct = false;
						};
						if (((std::get<0>(res_low_inc) != std::get<0>(res_struct_inc)) || (std::get<1>(res_low_inc) != std::get<1>(res_struct_inc)))
						&& ((std::get<0>(res_low_inc) != std::get<1>(res_struct_inc)) || (std::get<1>(res_low_inc) != std::get<0>(res_struct_inc)))) {
							std::cout<<"discrepancy 3d inc with non3d inc"<<seed<<", "<<truncsize<<", "<<nbpoint<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<0>(res_low_inc))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<1>(res_low_inc))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<0>(res_struct_inc))<<std::endl;
							std::cout<<utils::to_string<Point>(std::get<1>(res_struct_inc))<<std::endl;
							std::cout<<std::get<2>(res_low_inc)<<", "<<std::get<2>(res_struct_inc)<<std::endl;
							std::pair<T, std::vector<Point>> res_naive = utils::naive_hssp_k2<Point>(skyvec, dim);
							std::cout<<"res_naive:"<<std::get<0>(res_naive)<<std::endl;
							correct = false;
						};

					}
				}
			}
		}
	}
	return correct;
}
}  // namespace tests


#endif /*TEST_RANGETREE_H_*/