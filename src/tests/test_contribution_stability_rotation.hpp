/*
* test_contribution_stability_rotation.cpp
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef TEST_CONTRIBUTION_STABILITY_ROTATION_H_
#define TEST_CONTRIBUTION_STABILITY_ROTATION_H_

// #define NDEBUG
#include <cassert>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <utility>

#include "hssp_contribution/hypvcontributions.h"
#include "hssp2d/contrib_matrix.hpp"
#include "utils/generators.hpp"
#include "utils/naive_implementations/naive_hssp.h"
#include "utils/parsers.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"




namespace tests {


template<typename Point, class Container, typename IndexType>
void test_rotation(Container skyvec, int seed, int nbpoint, int nbrepet, int contribdepth) {
	typedef typename Point::value_type T;
	typedef typename hssp_contribution::ContribPoint<T> RCell;
	typedef typename std::unordered_map<std::pair<T,T>,RCell> ContainerOutput;
	typedef typename std::vector<RCell> VectorOutput;

	auto valid = utils::hssp3dk2_validate_input<Point, IndexType>(skyvec);
	if (valid.first) {
		Container skyvec2;
		Container skyvec3;
		std::transform(skyvec.begin(), skyvec.end(), std::back_inserter(skyvec2), [] ( Point p ) {Point q = p; std::swap(q[0], q[1]); std::swap(q[0], q[2]); return q;} ); // 0=>1, 1=>2, 2=>0
		std::transform(skyvec.begin(), skyvec.end(), std::back_inserter(skyvec3), [] ( Point p ) {Point q = p; std::swap(q[0], q[2]); std::swap(q[0], q[1]); return q;} ); // 0=>2, 2=>1, 1=>0

		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(seed) + "," + std::to_string(nbrepet) + "," + std::to_string(contribdepth);



		// NB if changing dim then change function parse_file_to_list_of_arrays_size4

		//	std::string outfilename_single = outfilename + "_single";
		ContainerOutput contribs_v1;
		ContainerOutput contribs_v2;
		ContainerOutput contribs_v3;
		VectorOutput v1;
		VectorOutput v2;
		VectorOutput v3;

		auto res1 = hssp_contribution::hypervolume_contributions<Point, ContainerOutput>(contribdepth, skyvec);
		auto res2 = hssp_contribution::hypervolume_contributions<Point, ContainerOutput>(contribdepth, skyvec);
		auto res3 = hssp_contribution::hypervolume_contributions<Point, ContainerOutput>(contribdepth, skyvec);

		contribs_v1 = res1.second;
		contribs_v2 = res2.second;
		contribs_v3 = res3.second;

		for (auto c : contribs_v1) { v1.push_back(c.second); }
		for (auto c : contribs_v2) { v2.push_back(c.second); }
		for (auto c : contribs_v3) { v3.push_back(c.second); }
		std::sort(v1.begin(), v1.end(), [] (RCell lhs, RCell rhs) { return lhs.cumulative_vol > rhs.cumulative_vol;});
		std::sort(v2.begin(), v2.end(), [] (RCell lhs, RCell rhs) { return lhs.cumulative_vol > rhs.cumulative_vol;});
		std::sort(v3.begin(), v3.end(), [] (RCell lhs, RCell rhs) { return lhs.cumulative_vol > rhs.cumulative_vol;});
		bool bug = false;
		if (v1.size() != v2.size() || v1.size() != v3.size()) {
			std::cout<<"dif size"<<std::endl;
			bug = true;
		}
		else {
			std::string s;
			for (auto c1 = v1.begin(), c2 = v2.begin(), c3 = v3.begin(); !bug && c1 != v1.end(); ++c1, ++c2, ++c3) {
				s += "cell1: " + c1->to_string() + "\ncell2: " + c2->to_string() + "\ncell3: " + c3->to_string() + "\n";
				if (!std::equal(c1->cumulative_vol.begin(), c1->cumulative_vol.end(), c2->cumulative_vol.begin()) || !std::equal(c1->cumulative_vol.begin(), c1->cumulative_vol.end(), c3->cumulative_vol.begin())) {
					std::cout<<s<<std::endl;
					std::cout<<"dif vol"<<std::endl;
					std::cout<<s<<std::endl;
					bug = true;
				}
			}
		}

	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<",seed:"<<seed<<",nbpoint:"<<nbpoint<<std::endl;
	}
}

void test_hsspd3_contrib() {
	typedef double T;
	typedef int IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints.txt");
	std::string const outfilename("results.csv");
	int dim = 3;
	for (int nbpoint = 5; nbpoint <= 1000; nbpoint *= 2) {
		for (int contribdepth: {1,2,3,5,10}) {
			for (int seed = 1; seed <= 3; ++seed) {
				utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
				std::vector<Point> skyvec;
				std::vector<std::vector<T>> skyvecb;
				utils::parse_file_to_list_of_vectors(infilename, skyvecb);
				utils::parse_file_to_list_of_arrays_size3<Point>(infilename, skyvec);
				//			std::cout << "\n---------------------\nskyline points are:"<< std::endl;
				//			print_pointlist<Point>(skyvec);
				for (int nbrepet = 1; nbrepet <= 1; ++ nbrepet) {
					test_rotation<Point, Container, IndexType>(skyvec, seed, nbpoint, nbrepet, contribdepth);
				}
			}
		}
	}
}

}  // namespace tests

#endif /* TEST_CONTRIBUTION_STABILITY_ROTATION_H_*/