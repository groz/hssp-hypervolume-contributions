/*
 * main.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 */

#ifndef TEST_TRIPLE_H_
#define TEST_TRIPLE_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>

#include "hssp_contribution/hypvcontributions.h"
#include "hssp2d/contrib_matrix.hpp"
#include "utils/generators.hpp"
#include "utils/parsers.hpp"
#include "utils/naive_implementations/naive_min_triple.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"
#include "weighted_hypergraph/min_triple.h"
#include "weighted_hypergraph/weighted_hypergraph.h"



namespace tests {
    
template<typename Graph, typename Point, typename IndexType>
bool test_minpair_from_graph(Graph g) {
    typedef typename Point::value_type T;
    typedef typename std::pair<T, std::vector<IndexType>> ResMinWeight;
    bool correct = true;
    ResMinWeight res1;
    ResMinWeight res2;
    res1 = utils::compute_min_pair_naive<T, IndexType>(g);
    res2 = weighted_hypergraph::compute_min_pair<T, IndexType>(g);
   // std::cout<<"pair naive: "<<res1.first<<std::endl;
   // std::cout<<"pair: "<<res2.first<<std::endl;
    if (res1.first != res2.first) correct = false;
    return correct;
}
    
template<typename Graph, typename Point, typename IndexType>
bool test_mintriple_from_graph(Graph g) {
    typedef typename Point::value_type T;
    typedef typename std::pair<T, std::vector<IndexType>> ResMinWeight;
    bool correct = true;
    ResMinWeight res1;
    ResMinWeight res2;
    res1 = utils::compute_min_triple_naive<T, IndexType>(g);
    res2 = weighted_hypergraph::compute_min_triple<T, IndexType>(g);
   // std::cout<<"triple naive: "<<res1.first<<std::endl;
   // std::cout<<"triple: "<<res2.first<<std::endl;
    if (res1.first != res2.first) correct = false;
    return correct;
}
    
template<typename Graph, typename ResContrib, typename T>
Graph build_graph_from_contribs(int nbpoint, ResContrib contribs) {
    typedef typename hssp_contribution::ContribPoint<T> RCell;
    Graph g(nbpoint);
    for (auto mapitem : contribs.second) {
        RCell c = mapitem.second;
        std::vector<unsigned long> s;
        s = {};
        int j = c.up_points.size() - c.cumulative_vol.size();
        if (c.up_points.size() >= 1) {
            unsigned long pt0 = c.up_points[0];
            s.push_back(pt0);
            if (j == 0) {
                T w0 = c.cumulative_vol[0];
                g.add_hyperedge_sorted(s, w0);
            }
        }
        if (c.up_points.size() >= 2) {
            unsigned long pt1 = c.up_points[1];
            s.push_back(pt1);
            std::sort(s.begin(), s.end());
            if (j <= 1) {
                T w1 = c.cumulative_vol[1 - j];
                g.add_hyperedge_sorted(s, w1);
            }
        }
        if (c.up_points.size() >= 3) {
            unsigned long pt2 = c.up_points[2];
            s.push_back(pt2);
            std::sort(s.begin(), s.end());
            if (j <= 2) {
                T w2 = c.cumulative_vol[2 - j];
                g.add_hyperedge_sorted(s, w2);
            }
        }
                   // std::cout<<"cell: "<<c.to_string()<<std::endl;
                   // std::cout<<"----\ntemporary graph: "<<g.to_string()<<"\n----"<<std::endl;
    }
    return g;
}
template<typename Point, typename IndexType>
bool test_minweight(std::vector<Point> skyvec, int seed, int nbpoint,std::string headersuffix) {
	typedef typename Point::value_type T;
	typedef typename hssp_contribution::ContribPoint<T> RCell;
	typedef typename std::unordered_map<std::pair<T,T>,RCell> ContainerOutput;
	typedef typename std::pair<std::vector<size_t>, ContainerOutput> ResContrib;
	typedef typename weighted_hypergraph::WGraph<T, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>> Graph;
	bool correct = true;
	
	auto valid = utils::hssp3dk2_validate_input<Point, IndexType>(skyvec);
	if (valid.first) {
		int contribdepth = 3;
		ResContrib contribs = hssp_contribution::hypervolume_contributions<Point, ContainerOutput>(contribdepth, skyvec);
        Graph g = build_graph_from_contribs<Graph, ResContrib, T>(nbpoint, contribs);
//        std::cout<<"graph: "<<g.to_string()<<std::endl;
        correct = correct && test_minpair_from_graph<Graph, Point, IndexType>(g);
        correct = correct && test_mintriple_from_graph<Graph, Point, IndexType>(g);
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<",seed:"<<seed<<",nbpoint:"<<nbpoint<<",distrib:"<<headersuffix<<std::endl;
	}
	return correct;
}

    
// For multiple instances, builds the graph of contributions then checks that minpair and mintriple algorithm return same result as enumerating naively every pair/triple of nodes in the graph.
bool test_hssp_largek() {
	typedef double T;
	typedef unsigned long IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints_largek_varn.txt");
	std::string const outfilename("results_largek_varn.csv");
	int dim = 3;

	int nbpoint_max = 50;
	int repet_max = 1;
	int seed_max = 4;
	bool correct = true;
	
	for (int nbpoint = 3; nbpoint <= nbpoint_max; nbpoint += 1) {
		for (int seed = 1; seed <= seed_max; ++seed) {
			for (std::string distrib : {"distr_hyperplane"}) {
			// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
				Container skyvec;
				if (distrib == "distr_hyperplane") {
					utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
				}
				if (distrib == "distr_convex") {
					utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
				}
				if (distrib == "distr_concave") {
					utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
				}
				utils::parse_file_to_list_of_arrays_size3(infilename, skyvec);

				for (int repet = 1; repet <= repet_max && correct; ++ repet) {
					correct = correct && test_minweight<Point, IndexType>(skyvec, seed, nbpoint, distrib);
					if (!correct) {
						std::cout<<"nbpoint: "<<nbpoint<<"seed: "<<seed<<std::endl;
						break;
					}
				}
			}
		}
	}
	return correct;
}

}  // namespace test

#endif /* TEST_TRIPLE_H_*/
