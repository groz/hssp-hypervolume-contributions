/*
* test_contrib_shark.hpp
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef TEST_CONTRIB_SHARK_H_
#define TEST_CONTRIB_SHARK_H_

// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <utility>

#include "hssp2d/contrib_matrix.hpp"
#include "shark_externals/interface_with_shark.hpp"
#include "shark_externals/sharklibrary_contributions.hpp"
#include "utils/generators.hpp"
#include "utils/other_utils.hpp"
#include "utils/parsers.hpp"
#include "utils/print_helpers.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"

#include <shark/Core/utility/KeyValuePair.h>


namespace tests {

bool test_contrib_shark() {
	typedef double T;
	typedef long IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;

	int k = 5;
	
	bool correct = true;
	Point p1 = {1,10,10};
	Point p2 = {2,8,9};
	Point p3 = {4,4,8};
	Point p4 = {3,5,4};
	Point p5 = {12,12,2};

	Point refpoint;
	
	Container skyvec = {p1, p2, p3, p4, p5};
	Container skyvec_minimization;
	utils::convert_maximization_to_positive_minimization_with_reference_array3d(skyvec, skyvec_minimization, refpoint);
	std::vector<shark_interface::VectorType3d> vect_shark = shark_interface::convert_vec_to_VectorType3d<Point>(skyvec_minimization);
	shark_interface::VectorType3d refpoint_shark(refpoint[0], refpoint[1], refpoint[2]);
	
	/* Total hypervolume of this instance: 22+15+10*2+3+1*5 = 65 */
	std::vector<IndexType> result_indexes({});
	std::vector<IndexType> result_indexes2({});
	shark::HypervolumeContribution3D c;
	std::vector<shark::KeyValuePair<double,std::size_t> > res = c.smallest(vect_shark, k, refpoint_shark);
	if (res[4].key != 234) correct = false;
	return correct;
}
} /* TEST_CONTRIB_SHARK_H_ */

#endif /* TEST_CONTRIB_SHARK_H_ */