Requirements:

- must be compiled with C++14 (or later) 
- requires that boost library be installed




Compilation instruction:
(from within the src directory)
g++ -std=c++1y -O3 -I . main.cpp




------------------------------
The shark directory includes the shark ML library (version 3.1 from 2016). 
This is a 3rd party software to which we compare one of our program.
While this makes a large part of this code repository, we did not write these pr
ograms 
and in fact we only use a small fraction of these programs; the ones that deal w
ith hypervolume computations. 
The programs in shark are only used from
hssp2d/sharklibrary_hssp2d.cpp 
so they can be left out if that file above is commented out.