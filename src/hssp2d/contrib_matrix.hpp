/*
 * contribMatrix.h
 *
 *  Created on: Sep 6, 2016
 *      Author: bgroz
 *
 *      Takes a set (vector) of 2D points as input, computes and stores the contribution of consecutive subsets.
 *      The input points might should be 2D (if they have more than 2 coordinates, only the 2 first are considered anyway).
 */

#ifndef CONTRIBMATRIX_H_
#define CONTRIBMATRIX_H_

#include <algorithm>  // for max, is_sorted...
#include <limits>
#include <string>
#include <vector>

#include <cassert>

namespace hssp2d {

// WARNING: susceptible to overflow, e.g. for int.
template<class Point, typename Index_type>
class ContribMatrix {
	typedef typename Point::value_type T;
public:
	// Takes a set (vector) of points as input, computes and stores the contribution of consecutive subsets.
	// Assumes input is a 2D skyline sorted by increasing x-coordinate.
	//  Complexity: O(n).
	ContribMatrix(const std::vector<Point> &input_points);


	// Returns the contributions of {points_[i], ..., points_[j-1]}.
	// One can verify that contrib(i,i) == 0.
	// But assumes i<=j: contrib(j,i) with j>i will return incorrect results instead of, say, 0.
	// Remark: special case; contrib(0,n) is the hypervolume.
	T Contrib(Index_type i, Index_type j) const;

	std::string to_string() const;
private:
	Index_type n;
	std::vector<Point> points_;  // container for the input points.
	std::vector<T> contribs_to_;  // contribs_to_[i] records the contribution of {points_[0], ..., points_[i-1]}: 0 if i=0.
	std::vector<T> contribs_from_;  // contribs_from_[i] records the contribution of {points_[i], ..., points_[n-1]}: 0 if i=n.
};



// -------------------------------------------
// Implementation:

template<class Point, typename Index_type>
ContribMatrix<Point, Index_type>::ContribMatrix(const std::vector<Point> &input_points) {
	assert(input_points[0][1] < std::numeric_limits<T>::max()/input_points.back()[0]);  // Otherwise risk of overflow.
	assert(std::is_sorted(input_points.begin(), input_points.end(), [](Point p, Point q){return p[0] <= q[0]; }));
	assert(std::is_sorted(input_points.begin(), input_points.end(), [](Point p, Point q){return p[1] >= q[1]; }));
	points_= std::vector<Point>(input_points);
	n = points_.size();
	contribs_to_ = std::vector<T>(n+1, -1);
	contribs_from_ = std::vector<T>(n+1, -1);


	contribs_to_[0] = 0;
	if (n == 1) {
		contribs_to_[1] = points_[0][0] * points_[0][1];
	}
	else { // n >= 2
		for (Index_type i = 1; i < n; ++i) {
			contribs_to_[i] = contribs_to_[i-1] + points_[i-1][0] * (points_[i-1][1] - points_[i][1]);
		}
		contribs_to_[n] = contribs_to_[n-1] + points_[n-1][0] * points_[n-1][1];
	}
	contribs_from_[n] = 0;
	if (n == 1) {
		contribs_from_[0] = points_[0][0] * points_[0][1];
	}
	else { // n >= 2
		for (Index_type i = n-1; i > 0; --i) {
			contribs_from_[i] = contribs_from_[i+1] + points_[i][1] * (points_[i][0] - points_[i-1][0]);
		}
		contribs_from_[0] = contribs_from_[1] + points_[0][1] * points_[0][0];
	}
}

template<class Point, typename Index_type>
typename Point::value_type ContribMatrix<Point, Index_type>::Contrib(Index_type i, Index_type j) const {
	assert(0 <= i && j<=n);
	assert(i <= j);
	return contribs_to_[j] + contribs_from_[i] - contribs_to_[n] + ( (i == 0 or j == n)? 0: points_[i-1][0]*points_[j][1]);
}

template<class Point, typename Index_type>
std::string ContribMatrix<Point, Index_type>::to_string() const {
	std::string s="";
	s+= "contribs_to_: {";
	for (Index_type i = 0; i <= n; ++i) {
		s += std::to_string(contribs_to_[i]);
		if (i < n) {s += ",";}
	}
	s+="}\n";
	s+= "contribs_from_: {";
	for (Index_type i = 0; i <= n; ++i) {
		s += std::to_string(contribs_from_[i]);
		if (i < n) {s += ",";}
	}
	s+="}\n";
	for (Index_type i = 0; i <= n; ++i) {
		for (Index_type j = 0; j <= n; ++j) {
			if (j<i) {
				s += "-\t";
			}
			else {
				s += std::to_string(Contrib(i,j))+"\t";
			}
		}
		s += "\n";
	}
	return s;
}

}  // namespace hssp2d
#endif /* CONTRIBMATRIX_H_ */
