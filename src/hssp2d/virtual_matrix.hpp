/*
 * virtual_matrix.cpp
 *
 *  Created on: Aug 25, 2016
 *      Author: bgroz
 *
 *
 *      Simulates value_at on matrix (p[i]+c.value_at(i,j)), shifting rows and columns as explained in hssp2d_smawk.
 *
 */

#ifndef VIRTUAL_MATRIX_CONV_H_
#define VIRTUAL_MATRIX_CONV_H_

// #include <limits>  // Already in contrib_matrix
// #include <string>  // Already in contrib_matrix
// #include <vector>  // Already in contrib_matrix

#include "hssp2d/contrib_matrix.hpp"

namespace hssp2d {
	
template<class Point, typename Index_type>
class VirtualMatrix{
	typedef typename Point::value_type T;
	public:
	VirtualMatrix(const ContribMatrix<Point, Index_type> &c, std::vector<typename Point::value_type> &p, Index_type rs, Index_type cs):
		contrib_matrix_(c), program_values_(p), row_shift_(rs), col_shift_(cs) { }

	T Lookup(Index_type i, Index_type j) const {  // Returns value at (i,j) in matrix G.
		if (i > j) {
			return std::numeric_limits<T>::max();
		}
		else {
			return program_values_[i] + contrib_matrix_.Contrib(i+row_shift_, j+col_shift_);
		}
	}
	std::string to_string() const {
		return "---------------------\n VirtualMatrix: row shift:"+std::to_string(row_shift_)+" col shift:"+std::to_string(col_shift_)+"\n"+contrib_matrix_.to_string()+"=====================";
	}
private:
	const ContribMatrix<Point, Index_type> &contrib_matrix_;
	std::vector<T> &program_values_;
	const Index_type row_shift_;
	const Index_type col_shift_;
};

}  // namespace hssp2d
#endif /* VIRTUAL_MATRIX_CONV_H_ */
