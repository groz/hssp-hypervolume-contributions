/*
 * hssp2d_extreme.cpp
 *
 *  Created on: Aug 25, 2016
 *      Author: bgroz
 */

#ifndef HSSP2D_EXTREME_H_
#define HSSP2D_EXTREME_H_

#include <algorithm>
#include <iostream>
#include <iterator>
#include <tuple>
#include <utility>
// #include <vector>  // Already in upperhull_incremental

#include <boost/functional/hash.hpp>

#include "utils/point_representation.hpp"
#include "utils/print_helpers.hpp"
#include "hull/upperhull_incremental.hpp"

// Computes HSSP of input vector of n points in dimension 2.
// Define P(l,j) = max {\hypv(S) | S\subseteq {points[0],\dots,points[j+l-1]}, points[j+l-1]\in S, \abs{S}=l}.  || computed for j\in {0,...,n-k}.
// The algorithm computes all P(l,j)_j from the P(l-1,j)_j using extreme point queries on convex hull.
// Complexity O(k*(n-k)).

// Assumptions:
// - input vector is a skyline ordered by first coordinate.
// - k should be at least 1 and at most n-1 (it actually makes little sense to ask for k=n, so reasonable range is 1<=k<=n-1, but algo still returns correct value on 0 and n)
// - std::size_t is large enough to index all points in the convex hull: i.e., k(n-k+1) points
//   	(an std::size_t identifier will be appended to points in the hull, which contain a quadratic number of points).
// - container types have max_size large enough (and system has enough free memory) to contain their data
//		(in particular, vector<std::size_t> must contain k(n-k+1) indexes for pg_sol,
//		and std::vector<HullPoint> must contain k(n-k+1) hullpoints for cvxh )
// See hssp2dValidateInput to check input.


namespace hssp2d {

template<typename Point>
typename Point::value_type hssp2dextreme(const std::vector<Point> &points, std::vector<std::size_t> &result_set_indexes, unsigned long k) {
	typedef typename Point::value_type T;
	typedef utils::Point2d<T> QueryPoint;
	typedef utils::PointWithId2d<T> HullPoint;
	typedef hull::UpperHull<T, HullPoint, QueryPoint> Hull_class;
	
	std::size_t n = points.size();
	if (k == 0) {return 0;} // Getting rid of an absurd case.

	std::vector<std::size_t> pg_sol((n+1-k)*k,-1);  // Stores index of partial solution being completed
	std::vector<T> q(n+1-k, -1);  // Only stores the value of the completed partial solution, cf invariant below. To recover the solution, we use pg_sol.
	for (std::size_t i = 0; i < n+1-k; ++i) {
		q[i] = points[i][0] * points[i][1];
	}

	for (std::size_t l = 1; l < k; ++l) {
		Hull_class cvxh;
		for (std::size_t j = 0; j < n+1-k; ++j) {
			// Invariant: q[j] records P(l,j) at this point.
			cvxh.add_point(HullPoint(points[j+l-1][0], q[j], j));
			HullPoint extreme_point = cvxh.Extreme(QueryPoint(-points[j+l][1], 1));

			q[j] = extreme_point.coord1 + points[j+l][1] * (points[j+l][0] - extreme_point.coord0);

			pg_sol[l*(n+1-k)+j] = (l-1)*(n+1-k) + extreme_point.id;

			// Invariant: q[j] records at this point: P(l+1,j)  || computed for j\in {0,...,n-k}.
			// pg_sol[l*(n+1-k)+j] records solution of P(l+1, j): the solution
			//    -> contains point j+l which is computed as l*(n+1-k)+j - l*(n-k),
			//    -> together with pg_sol[i] where i = pg_sol[l*(n+1-k)+j].
		}
	}

	auto res = std::max_element(q.begin(), q.end());
	std::size_t sol_idx = std::distance(q.begin(), res);  // sol_idx + k-1 is the index in {0,...,n-1} of the last point from the solution.
	sol_idx += (k-1)*(n+1-k);
	for (std::size_t l = k-1; l > 0; --l) {
		// Invariant: sol_idx - l*(n-k) is the index of the last point from the solution, excepting the points already added to result_set_indexes.
		result_set_indexes.push_back(sol_idx - l*(n-k));
		sol_idx = pg_sol[sol_idx];
	}
	result_set_indexes.push_back(sol_idx);
	sol_idx = pg_sol[sol_idx];

	return (*res);
}


// Complexity in time is similar to the program returning also the solution, but
// complexity in space is O(n) instead of O(k*n-k). In fact, it is even O(n-k) if we don't consider input.
template<typename Point>
typename Point::value_type hssp2dextremeValueOnly(const std::vector<Point> &points, unsigned long k) {
	typedef typename Point::value_type T;
	typedef utils::Point2d<T> QueryPoint;
	typedef utils::Point2d<T> HullPoint;
	typedef hull::UpperHull<T, HullPoint, QueryPoint> Hull_class;
	
	std::size_t n = points.size();
	if (k == 0) {return 0;} // Getting rid of an absurd case.
	
	std::vector<T> q(n+1-k, -1);
	for (std::size_t i = 0; i < n+1-k; ++i) {
		q[i] = points[i][0] * points[i][1];
	}
	for (std::size_t l = 1; l < k; ++l) {
		// Invariant: q[j] records P(l,j) at this point:
		Hull_class cvxh;
		for (std::size_t j = 0; j < n+1-k; ++j) {
			cvxh.add_point(HullPoint(points[j+l-1][0], q[j]));
			HullPoint extreme_point = cvxh.Extreme(QueryPoint(-points[j+l][1], 1));

			
			// q[j] = std::get<1>(extreme_point) + points[j+l][1] * (points[j+l][0] - std::get<0>(extreme_point));
			q[j] = extreme_point.coord1 + points[j+l][1] * (points[j+l][0] - extreme_point.coord0);
		}
		// Invariant: q[j] records at this point: P(l+1,j)  || computed for j\in {0,...,n-k}.
	}
	return (*std::max_element(q.begin(), q.end()));
}

}  // namespace hssp2d

#endif /* HSSP2D_EXTREME_H_ */
