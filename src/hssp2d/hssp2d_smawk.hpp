/*
 * hssp2d_smawk.cpp
 *
 *  Created on: Aug 24, 2016
 *      Author: bgroz
 */

#ifndef HSSP2d_SMAWK_H_
#define HSSP2d_SMAWK_H_

// #include <algorithm>  // Already in contrib_matrix
#include <fstream>
#include <iostream>
#include <iterator>
// #include <limits>  // Already in contrib_matrix
// #include <string>  // Already in contrib_matrix
#include <utility>  // for pair
// #include <vector>  // Already in contrib_matrix


#include "hssp2d/contrib_matrix.hpp"
#include "hssp2d/virtual_matrix.hpp"
#include "hssp2d/smawk_with_value.hpp"

// Computes HSSP of input vector of n points in dimension 2.
// Value returned: total contributions of the n-k unselected point.
// Complexity O(k (n-k)).

// Some differences with indices in paper because input is {p[0],...p[n-1]}, and contribs lookup different.
// We define Q(l,j) = minimal contrib in {p[0],...,p[j+l-1]} of unselected points when we select l points including p[j+l-1]
//    (plus virtual point (0,+infty) in addition to these l points).
// Q(1,j) = contribs of {p[0],...,p[j-1]}.      (with convention that this is 0 if j=0)  || computed for j\in {0,...,n-k}
// Q(l,j) = min {Q(l-1,i) + contribs of {p[i+l-1],...,p[j+l-2]} | i\in {0,j} }.   || computed for j\in {0,...,n-k}

// Assumptions:
// - input vector is a skyline ordered by first coordinate.
// - k should be at least 1 and at most n-1 (it actually makes little sense to ask for k=n, so reasonable range is 1<=k<=n-1, but algo still returns correct value on n)
// - std::size_t is large enough to reach k(n-k+1)     (cf pg_sol).
// - container types have max_size large enough (and system has enough free memory) to contain their data
//		(in particular, vector<std::size_t> must contain k(n-k+1) indexes for pg_sol,
//		and std::vector<HullPoint> must contain k(n-k+1) hullpoints for cvxh )
// See hssp2dValidateInput to check input.

namespace hssp2d {
	
template<typename Point>
typename Point::value_type  hssp2dsmawk(const std::vector<Point> &input_points, std::vector<std::size_t> &result_set_indexes, unsigned long k) {
	typedef typename Point::value_type T;

	std::size_t n = input_points.size();
	if (k == 0) {return 0;} // Getting rid of an absurd case.
	ContribMatrix<Point, std::size_t> contrib_matr(input_points);
	std::vector<std::size_t> pg_sol((n+1-k)*k, -1);  // Stores index of partial solution being completed
	std::vector<T> pg_values(n+1-k,0);  // Only stores the value of the completed partial solution, cf invariant below. To recover the solution, we use pg_sol.
	for (std::size_t j = 0; j < n+1-k; ++j) {
		pg_values[j] = contrib_matr.Contrib(0,j);  // At this point, pg_values[j] = Q(1,j) = Contribs of {points[0],...,points[j-1]}.
	}

	for (std::size_t l = 1; l < k; ++l) {
		// Invariant: pg_values[j] records Q(l,j) at this point.
		std::vector<std::pair<std::size_t,T>> col_minima(n+1-k, {-1,0});

		// Smawk computes for j=0 to n-k: col_min[j] = min { v_matr[i,j] | 0 <= i }
		// so we define v_matr[i,j] = pg_value[i] + contribs of {p[i+l],...,p[j+l-1]} for i in {0,...,j}. Special cases are: pg_value[i] + 0 if i=j, and +infty if i>j.
		VirtualMatrix<Point, std::size_t> v_matrix(contrib_matr, pg_values, l, l);
		SMAWK<Point, std::size_t>(n+1-k, n+1-k, col_minima, v_matrix);

		for (std::size_t j = 0; j < n+1-k; ++j) {
			pg_values[j] = col_minima[j].second;
			pg_sol[l*(n+1-k)+j] = (l-1)*(n+1-k) + col_minima[j].first;
		}
		// Invariant: pg_values[j] records Q(l+1,j) at this point.
		// pg_sol[l*(n+1-k)+j] records solution of Q(l+1, j): contains point j+l = l*(n+1-k)+j - l*(n-k), together with pg_sol[i] where i = pg_sol[l*(n+1-k)+j].
	}

	T mincontrib = std::numeric_limits<T>::max();
	std::size_t sol_idx = n-k;
	for (std::size_t i = 0; i < n+1-k; ++i) {  // Rk: we could alternatively compute mincontrib and sol_idx using std::max_element with custom comparison function, together with std::distance.
		if (mincontrib > pg_values[i] + contrib_matr.Contrib(i+k, n)) {
			mincontrib = pg_values[i] + contrib_matr.Contrib(i+k, n);
			sol_idx = i;
		}
	}  // sol_idx + k-1 is the index in {0,...,n-1} of the last point from the solution.
	sol_idx += (k-1)*(n+1-k);
	for (std::size_t l = k-1; l > 0; --l) {
		// Invariant: sol_idx - l*(n-k) is the index of the last point from the solution, excepting the points already added to result_set_indexes.
		result_set_indexes.push_back(sol_idx - l*(n-k));
		sol_idx = pg_sol[sol_idx];
	}
	result_set_indexes.push_back(sol_idx);
	sol_idx = pg_sol[sol_idx];

	return mincontrib;
}


template<typename Point>
typename Point::value_type  hssp2dsmawkValueOnly(const std::vector<Point> &input_points, unsigned long k) {
	typedef typename Point::value_type T;

	std::size_t n = input_points.size();
	if (k == 0) {return 0;} // Getting rid of an absurd case.
	ContribMatrix<Point, std::size_t> contrib_matr(input_points);
	std::vector<T> pg_values(n+1-k, 0);
	for (std::size_t j = 0; j < n+1-k; ++j) {
		pg_values[j] = contrib_matr.Contrib(0,j);  // At this point, pg_values[j] = Q(1,j) = Contribs of {points[0],...,points[j-1]}.
	}
	//	print_vect<T>(pg_values, "a");

	for (std::size_t l = 1; l < k; ++l) {
		// At this point, pg_values[j] records Q(l,j).
		std::vector<std::pair<std::size_t,T>> col_minima(n+1-k, {0,0});

		// Smawk computes for j=0 to n-k: col_min[j] = min { v_matr[i,j] | 0 <= i }
		// so we define v_matr[i,j] = pg_value[i] + contribs of {p[i+l],...,p[j+l-1]} for i in {0,...,j}. Special cases are: pg_value[i] + 0 if i=j, and +infty if i>j.
		VirtualMatrix<Point, std::size_t> v_matrix(contrib_matr, pg_values, l, l);
		SMAWK<Point, std::size_t>(n+1-k, n+1-k, col_minima, v_matrix);

		//		std::cout<<v_matrix.to_string();
		//		print_vectpair<int,T>(col_minima);

		for (std::size_t j = 0; j < n+1-k; ++j) {
			pg_values[j] = col_minima[j].second;
		}
		// At this point, pg_values[j] records Q(l+1,j).
		//		std::cout << v_matrix.to_string() << std::endl;
	}
	T mincontrib = std::numeric_limits<T>::max();
	//	print_vect<T>(pg_values, "b");

	for (std::size_t i = 0; i < n+1-k; ++i) {
		mincontrib = std::min(mincontrib, pg_values[i] + contrib_matr.Contrib(i+k, n));
	}

	return mincontrib;
}

}  // namespace hssp2d

#endif /* HSSP2d_SMAWK_H_ */
