/*
 * smawkWithValue.h
 *
 *  Created on: Sep 15, 2016
 *      Author: bgroz
 *
 *
 *  Inspired from Eppstein's python code and from Hongcheng Zhu (zhuhcheng@gmail.com).
 *
 *  Implements the SMAWK algorithm from:
 *  A. Aggarwal, M. Klawe, S. Moran, P. Shor and R. Wilber,
 *  Geometric applications of a matrix searching algorithm,
 *  Algorithmica 2, pp. 195-208 (1987).
 *
 *  adapts the original algorithm to column minima instead of row maxima, and different quadrangle inequality.
 *  Returns the minimum value of each row for a totally monotone matrix A[][] in linear time.
 *  Assumes input matrix is totally monotone, i.e. : for any 0<=i1<i2<n, 0<=j1<j2<m,
 *  A[i1][j1]>A[i2][j1] implies A[i1][j2]>A[i2][j2] : column  minima are monotonically non-decreasing in every submatrix.


 *  assumes nb_cols>0.
 *  matrix: provides O(1) lookup.
 *  col_minima: returns array storing for each col the index of the row that contains its minimum (taking smallest index in case of ties), and the corresponding value.
 *  Complexity: O(m) when n<=m. When n>m becomes O(n+m) but in that case there are better algorithm in O(m(1+log(n/m)).

 *  The algorithm first deletes some rows to reduce the number of column to not greater than the number of rows,
 *  using the following two facts:
 *  1. If A[a][a]<=A[a+1][a], then for any 0<=i<=a A[a][i]<=A[a+1][i], so the elements A[a+1][i]s are all "dead".
 *  2. If A[a][a]>A[a+1][a], then for any a<=i<n A[a][i]>A[a+1][i], so the elements A[a][i]s are all "dead".
 *  Then it partitions the cols into two parts even cols and odd cols, and recursively calculates the col minima of even cols.
 */

#ifndef SMAWK_H_
#define SMAWK_H_


#include <utility>
// #include <vector>  // Already in virtual_matrix

#include "hssp2d/virtual_matrix.hpp"

#include <cassert>

namespace hssp2d {

template<class Point, typename Index_type>
void SMAWK(Index_type nb_rows, Index_type nb_cols, std::vector<std::pair<Index_type,typename Point::value_type>> &col_minima, const VirtualMatrix<Point, Index_type> &matrix);



// ******************************************
// ***********  IMPLEMENTATION:  ************



// Assumes row_indices is not empty.
// nb_cols is the initial number of rows, to obtain the number of preserved cols we must divide by inc.
// inc: col increment: the cols kept are cols number 0, inc, 2*inc, 3*inc...
// col: index of columns preserved in the recursive subroutines (initially all).
// rk: we could replace inc by a list col_indices as in Eppstein,
// but this is not necessary for cols as we keep even cols (unlike rows for which preserved rows cannot be predicted).
template<class Point, typename Index_type>
void SMAWK_aux(Index_type nb_cols_total, Index_type inc, std::vector<Index_type> row_indices, std::vector<std::pair<Index_type, typename Point::value_type>> &col_minima, const VirtualMatrix<Point, Index_type> &virt_matrix)
{
	typedef typename Point::value_type T;
	// Reduce phase: make number of rows at most equal to number of cols.
	//	const std::vector<Index_type>::size_type nb_cols = (nb_cols_total+inc-1)/inc; // number of preserved cols = ceil(nb_cols divided by inc)
	const Index_type nb_cols = (nb_cols_total+inc-1)/inc; // number of preserved cols = ceil(nb_cols divided by inc)
	std::vector<Index_type> row_stack;
	for (auto elt : row_indices)
	{
		while(row_stack.size() &&
				virt_matrix.Lookup(row_stack.back(), inc*(row_stack.size()-1)) > virt_matrix.Lookup(elt, inc*(row_stack.size()-1)))
			row_stack.pop_back();
		if((Index_type)row_stack.size() < nb_cols) row_stack.push_back(elt);
	}
	row_indices = row_stack;


	assert((Index_type)row_indices.size() <= nb_cols);
	assert(row_indices.size());
	// Base case.
	if(nb_cols == 1)
	{
		col_minima[0] = {row_indices[0], virt_matrix.Lookup(row_indices[0], 0)};  // Because in that case the number of row indices after reduction is also 1.
	}
	// Recursive phase: compute minima in every even col (well, odd if you number from 1 as in maths).
	else {
		SMAWK_aux(nb_cols_total, inc<<1, row_indices, col_minima, virt_matrix);
		for (Index_type i = inc, r = 0; i < nb_cols_total; i += 2*inc)
		{
			// [pre,next] is the interval of rows (indices of the row in the initial matrix - as opposed to row_index) in which we col_minima[i] may be located.
			// exploiting the property that col_minima is non-decreasing with i and we know its value for both i-inc and i+inc.
			Index_type pre = col_minima[i-inc].first;
			Index_type next = (i+inc < nb_cols_total)?col_minima[i+inc].first:row_indices.back();

			//			// Eppstein's version:
			//			Index_type &res = col_minima[i];
			//			res = {row_indices[r], matrix.value_at(row_indices[r], i)};
			//			while (r < row_indices.size() && row_indices[r] != next) {
			//				++r;
			//				if (matrix.value_at(row_indices[r], i) <= matrix.value_at(res, i)) res = {row_indices[r], matrix.value_at(row_indices[r], i)};
			//			}

			// Zhu's version:
			while (r < (Index_type)row_indices.size() && row_indices[r] < pre) r++;
			// Warnings about Index_type vs size_type probably irrelevant as in practice cannot have too many columns anyway.
			// Solution otherwise would be to replace everywhere Index_type with std::vector<Index_type>::size_type.
			assert(r < (Index_type)row_indices.size() && row_indices[r] >= pre);
			assert(row_indices[r] <= next);
			std::pair<Index_type,T> &res = col_minima[i];
			res = {row_indices[r], virt_matrix.Lookup(row_indices[r], i)};
			while (r < (Index_type)row_indices.size() && row_indices[r] <= next)
			{
				if (virt_matrix.Lookup(row_indices[r], i) <= virt_matrix.Lookup(res.first, i)) res = {row_indices[r], virt_matrix.Lookup(row_indices[r], i)};
				r++;
			}
			r--;
		}
	}
}

template<class Point, typename Index_type>
void SMAWK(Index_type nb_rows, Index_type nb_cols, std::vector<std::pair<Index_type, typename Point::value_type>> &col_minima, const VirtualMatrix<Point, Index_type> &p_matrix)
{
	std::vector<Index_type> rows(nb_rows);
	for(Index_type i = 0; i < nb_rows; i++) rows[i] = i;
	Index_type initial_increment = 1;
	SMAWK_aux(nb_cols, initial_increment, rows, col_minima, p_matrix);
}

}  // namespace hssp2d

#endif /* SMAWK_H_ */
