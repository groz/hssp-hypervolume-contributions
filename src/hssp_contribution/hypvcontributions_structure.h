/*
 * hypvcontributions_structure.h
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 */

#ifndef HYPVCONTRIBUTIONS_STRUCT_H_
#define HYPVCONTRIBUTIONS_STRUCT_H_


// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric> // used for iota
#include <iterator>
#include <limits>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <vector>

#include "hssp_contribution/cell_chain.h"
#include "hssp_contribution/hypvcontributions.h"
#include "utils/print_helpers.hpp"
#include "utils/hash_functions.hpp"
#include "utils/time_utils.hpp"

namespace hssp_contribution{

template<typename T, typename Point>
class Contribs_3d {
public:
	typedef typename hssp_contribution::ContribPoint<T> RCell;
	typedef typename std::unordered_map<std::pair<T,T>, RCell> RCellContainer;
	typedef typename std::vector<size_t> index_map;
	std::pair<index_map, RCellContainer> contribs;
	index_map inverted_index;  // inverted_index[i] gives the rank of input point i by decreasing z coordinate.
	std::vector<Point> input_points;  // points need not be sorted by decreasing z;

	Contribs_3d(int k, std::vector<Point> &pts) {
		// auto t1 = utils::current_clock();
		input_points = pts;
		contribs = hssp_contribution::hypervolume_contributions<Point, RCellContainer>(k, input_points);
		inverted_index = index_map(contribs.first.size());
		std::iota(inverted_index.begin(), inverted_index.end(), 0);
		std::sort(inverted_index.begin(), inverted_index.end(), [&](size_t i1, size_t i2) {
			return contribs.first[i1] < contribs.first[i2];
		});
		// auto t4 = utils::current_clock();
		// std::cout<<"preprocessing (whole): "<<utils::delay_between(t1, t4)<<std::endl;
	}

	T Contribs(std::vector<size_t> pt_indexes) {
		std::unordered_set<size_t> point_indexes;  // Records pt_indexes by their rank among input_points sorted by decreasing z.
		for (auto i : pt_indexes) {
			point_indexes.insert(inverted_index[i]);
		}
		std::vector<T> x_list;
		std::vector<T> y_list;
		for (auto i : pt_indexes) {
			x_list.push_back(input_points[i][0]);
			y_list.push_back(input_points[i][1]);
		}
		std::sort(x_list.begin(), x_list.end());
		T vol_contrib = 0;
		for (auto y : y_list) {
			int index = -1;
			for (auto x : x_list) {
				auto itcontrib = contribs.second.find(std::make_pair(x, y));
				if (itcontrib != contribs.second.end()) {
					while ((index + 1 < (int)itcontrib->second.up_points.size()) &&
							(point_indexes.find(itcontrib->second.up_points[index + 1]) != point_indexes.end())) {
						++index;
					}
					int offset = (int)(itcontrib->second.up_points.size() - itcontrib->second.cumulative_vol.size());
					if (index >= offset) {
						vol_contrib += itcontrib->second.cumulative_vol[index - offset];
					}
				}
				if (index >= 0) {
					--index;
				}
			}
		}
		return vol_contrib;
	}

	std::string to_string() const {
		std::string s;
		s += "input points: \n"+ utils::to_string<Point>(input_points);
		s += "\npoint indexes: "+ utils::to_string<index_map>(contribs.first, " ", "", "");
		s += "\ninverted index: "+ utils::to_string<index_map>(inverted_index, " ", "", "");
		return s;
	}


};

}  // namespace hssp_contribution

#endif /* HYPVCONTRIBUTIONS_STRUCT_H_ */
