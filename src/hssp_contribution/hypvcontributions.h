/*
 * hypvcontributions.h
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 */

#ifndef HYPVCONTRIBUTIONS_CHAIN_SPLICE6_H_
#define HYPVCONTRIBUTIONS_CHAIN_SPLICE6_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric> // used for iota
#include <iterator>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <utility>

#include "hssp_contribution/cell_chain.h"
#include "utils/print_helpers.hpp"
#include "utils/hash_functions.hpp"

namespace hssp_contribution{

// This version stores the result in a map and does not use index_in_output, nb_contribs nor index_parent. The gains obtained by removing those ints from the cells are insignificant.
// Using a map makes it much slower than pushing_back on a vector.???
// This version (compared to version 4) has corrected the uppoints after a split
// Compared with 5 we put every info in the Chain cell : one less indirection during algo but once in last layer we have to copy large vectors for pushing them to set.
// This optimization basically saves a factor 2: copying the Chain cells into map takes less than 1s for (n=1000,k=1000), whereas the additional indirection took twice the time.
// But for (n=10^6,k=3) copying Chain cells into map is half the cost (3s without copying vs 7s with copying).

// Computes joint hypervolume contributions for every set with at most k 3D points.
// Complexity: O(k^2*n + n*log n)
// As a side effect will reorder input points.
// Assumes input is a 3D skyline in general position.
// Stores contributions as std::unordered_map from (up_x, up_y) to the corresponding ContribPoint




// Recommended template parameters for hypervolume_contributions:
// with:
//   typedef  double T; // or int, long. Could be unsigned. Must represent large enough numbers (volumes).
//   typedef typename hssp_contribution::ContribPoint<T> RCell;
// typedef std::array<T, 3> Point;
// typedef typename std::unordered_map<std::pair<T,T>,RCell> ContainerOutput;

// Auxiliary function used to factorize code.
// The auxiliary function:
// - splits cells in layer i according to p.
// - moves the cells from layer i dominated by p in layer i+1, plus a 'copy' in contribs_v.
// - Updates:
//   - the result contribs_v
//   - the layers tempsky
//   - the tree of pointers toward the last layer (but only performs tree insertions: deletions are performed outside, so we can spare the argument leftcell_in_last_skyline).
// - Returns: the new value of
//		{
//		leftcell_in_higher_layer,
//		rightcell_in_higher_layer,
//		leftcell_in_layer_above_higher_layer,
//		rightcell_in_layer_above_higher_layer
//		}
// Remarks:
//	- we do not have to update pointers to_right within cells when they are moved from one layer to the next because we use splice operations which preserve iterators.
//	- insertion hints in the tree are always correct. They are necessary to achieve the complexity claimed.
// Many tests (i == k) deal with the special case of the last layer which:
//	 - has a set on top of it
//	 - triggers insertions into contribs_v.
// Case (i==0) is also set apart because it creates a new cell in addition to splits.
template<typename Point, typename ContainerLayer, typename ContainerLastLayer, typename ContainerOutput>
std::tuple<typename ContainerLayer::iterator,typename  ContainerLayer::iterator,typename  ContainerLayer::iterator,typename  ContainerLayer::iterator>
hypervolume_contributions_iteration_splice(
		int i, // Index of the current layer
		int k,
		int point_index,
		Point p,
		typename ContainerLayer::iterator leftcell_in_higher_layer,  // last cell in higher layer having up_y >= p[1].
		typename ContainerLayer::iterator rightcell_in_higher_layer,  // first cell in higher layer having up_x >= p[0].
		typename ContainerLayer::iterator leftcell_in_layer_above_higher_layer,  // last cell in layer above higher layer having up_y >= p[1].
		typename ContainerLayer::iterator rightcell_in_layer_above_higher_layer,  // first cell in layer above higher layer having up_x >= p[0].
		const typename ContainerLayer::iterator leftcell, // last cell in current layer having up_y >= p[1]. Except for i==0: the first having up_y > p[1].
		const typename ContainerLayer::iterator rightcell, // first cell in current layer having up_x >= p[0]. Except for i==0: the first having up_x > p[0].
		typename ContainerLastLayer::iterator rightcell_in_last_skyline,
		ContainerOutput &contribs_v,
		std::vector<ContainerLayer> &tempsky,
		ContainerLastLayer &tree  // tree of pointers allowing for binary search on the last skyline.
		) {

	ContainerLayer &current_layer = tempsky[i];
	ContainerLayer &higher_layer = tempsky[i+1];
	typedef typename Point::value_type T;
	typedef typename hssp_contribution::Chain_Cell<T> Cell;
	typedef typename hssp_contribution::Tree_Cell<T> TCell;
	typedef typename ContainerLayer::iterator layer_iterator;
	const T max_T_value = std::numeric_limits<T>::max();
	layer_iterator dummy_to_right = tempsky.back().begin();  // Dummy value for to_right, not in proper layer.
	
	assert(leftcell->up_x < rightcell->up_x);
	assert(leftcell->up_y > rightcell->up_y);
	assert(leftcell->up_y >= p[1]);
	assert(std::next(leftcell, 1)->up_y < p[1] || (i==0 && std::next(leftcell, 1)->up_y == p[1]));
	assert(rightcell->up_x >= p[0]);
	assert(std::prev(rightcell, 1)->up_x < p[0] || (i==0 && std::prev(rightcell, 1)->up_x == p[0]));
	assert(leftcell_in_higher_layer->up_y >= p[1] && std::next(leftcell_in_higher_layer, 1)->up_y < p[1]);
	assert(rightcell_in_higher_layer->up_x >= p[0] && std::prev(rightcell_in_higher_layer, 1)->up_x < p[0]);
	assert(leftcell_in_layer_above_higher_layer->up_y >= p[1] && std::next(leftcell_in_layer_above_higher_layer, 1)->up_y < p[1]);
	assert(rightcell_in_layer_above_higher_layer->up_x >= p[0] && std::prev(rightcell_in_layer_above_higher_layer, 1)->up_x < p[0]);
	assert(leftcell_in_layer_above_higher_layer->up_y == p[1] || leftcell_in_layer_above_higher_layer->up_y == max_T_value);
	assert(rightcell_in_layer_above_higher_layer->up_x == p[0] || rightcell_in_layer_above_higher_layer->up_x == max_T_value);


	// Split rightcell: left part (in xy plane, i.e. first part of low_stairs) becomes new cell, right part attributes are updated.
	if (rightcell->up_x != max_T_value) {
		T delta_area = 0;
		T last_y = rightcell->up_y;
		size_t new_low_stairs_size = 0;
		auto idx = rightcell->low_stairs.begin();
		while (idx != rightcell->low_stairs.end() && idx->first < p[0]) {
			delta_area += (p[0] - idx->first) * (last_y - idx->second);
			last_y = idx->second;
			++new_low_stairs_size;
			++idx;
		}
		// assert(idx != rightcell->low_stairs.begin()); Because by definition of rightcell. there is at least one iteration through the while loop above.
		--idx; // idx is now the last point in low_stairs with idx->first < p[0]

		// We could perhaps optimize a bit the following by using pointers swaps, but probably not worth it.
		if (i < k-1) {
			// Insert the left part of the split to the next layer, update neighbours.
			std::list<std::pair<T,T> > leftlows;
			std::copy_n(rightcell->low_stairs.begin(), new_low_stairs_size, std::back_inserter(leftlows));
			auto new_cell = Cell(p[0], rightcell->up_y,
					leftlows,
					delta_area,
					p[2],
					0,
					rightcell,
					rightcell->up_points,
					{});
			new_cell.add_point(point_index);

			assert(*std::prev(rightcell_in_higher_layer, 1) < new_cell && new_cell < *rightcell_in_higher_layer);
			rightcell_in_higher_layer = higher_layer.insert(rightcell_in_higher_layer, new_cell);
			// It is crucial that we update rightcell_in_higher_layer here as that position will be used to insert
			// - the bottom part of the left split
			// - and the dominated part from the current_layer.
			if (i == k-2) {
				auto tree_cell = TCell(p[0], rightcell->up_y, rightcell_in_higher_layer);
				rightcell_in_last_skyline = tree.insert(rightcell_in_last_skyline, tree_cell);
			}
			else {  // i < k-2, updating the to_right pointers toward rightcell that are outdated.
				layer_iterator hcell = std::prev(rightcell_in_layer_above_higher_layer, 1);
				while (hcell->up_y <= rightcell->up_y) {
					assert(hcell->to_right == rightcell);
					hcell->to_right = rightcell_in_higher_layer;
					--hcell;
				}
			}
		}

		(*idx) = {p[0], last_y};
		rightcell->low_stairs.erase(rightcell->low_stairs.begin(), idx);
		rightcell->volume_above += rightcell->a * (rightcell->z - p[2]);
		rightcell->z = p[2];
		rightcell->a -= delta_area;
	}

	// Split leftcell: lower part (in xy plane, i.e. first part of low_stairs) becomes new cell, upper part attributes are updated.
	// This has to come after the rightcell block otherwise the insertion hint cannot be exploited.
	if (leftcell != current_layer.begin()) {
		T delta_area = 0; // area removed from leftcell->a
		size_t new_low_stairs_size = 0; // New size for leftcell->low_stairs ( before we add {lx, p[1]} ).
		auto idx = std::prev(leftcell->low_stairs.end(), 1);
		if (p[1] > leftcell->low_stairs.begin()->second) {
			idx = leftcell->low_stairs.begin();
			delta_area = leftcell->a - (leftcell->up_x - idx->first) * (leftcell->up_y - p[1]);
		}
		else {
			new_low_stairs_size = leftcell->low_stairs.size();
			T last_x = leftcell->up_x;
			while (idx->second < p[1]) { // idx != leftcell->low_stairs.begin() by assumption.
				delta_area += (last_x - idx->first) * (p[1] - idx->second);
				last_x = idx->first;
				--new_low_stairs_size;
				--idx;
			}
			++idx; // idx now first point on low_stairs with idx.second < p[1]
		}
		T lx = idx-> first;  // Saved before iterator invalidated by resize.

		if (i < k-1) {
			std::list<std::pair<T,T> > rightlows;
			std::copy(idx, leftcell->low_stairs.end(), std::back_inserter(rightlows));
			auto new_cell = Cell(leftcell->up_x, p[1],
					rightlows,
					delta_area,
					p[2],
					0,
					leftcell->to_right, // Temporary value for new_cell.to_right.
					leftcell->up_points,
					{});
			new_cell.add_point(point_index);
			assert(*std::prev(rightcell_in_higher_layer, 1) < new_cell && new_cell < *rightcell_in_higher_layer);
			// The invariant holds because we have already removed the portion dominated by the point in tempsky[i+1]
			leftcell_in_higher_layer = higher_layer.insert(rightcell_in_higher_layer, new_cell);
			if (i == k-2) {
				auto tree_cell = TCell(leftcell->up_x, p[1], leftcell_in_higher_layer);
				tree.insert(rightcell_in_last_skyline, tree_cell);
			}
			else {  // i < k-2, updating the to_right pointers toward leftcell that are outdated.
				layer_iterator hcell = leftcell_in_layer_above_higher_layer;
				if (hcell->up_y == max_T_value) {
					++hcell;
				}
				while (hcell->up_x != max_T_value && hcell->to_right == leftcell) {
					assert(hcell != rightcell_in_higher_layer);
					hcell->to_right = leftcell_in_higher_layer;
					++hcell;
				}
			}
		}

		leftcell->low_stairs.resize(new_low_stairs_size);
		leftcell->low_stairs.push_back({lx, p[1]});
		leftcell->volume_above += leftcell->a * (leftcell->z - p[2]);
		leftcell->z = p[2];
		leftcell->a -= delta_area;
	}

	if (i == 0) {
		// Creating new cell dominated by p, moving dominated cells.
		T a = 0;
		T last_y = p[1];
		std::list<std::pair<T,T> > lows = {};
		T last_x = leftcell->up_x;

		auto new_cell = Cell(p[0], p[1],
				{},  // Temporary value for low_stairs
				0,  // Temporary value for a
				p[2],
				0,
				dummy_to_right,
				{point_index},
				{}
				);
		layer_iterator new_cell_it = current_layer.insert(rightcell, new_cell);
		if (k == 1) {
			auto tree_cell = TCell(p[0], p[1], new_cell_it);
			tree.insert(rightcell_in_last_skyline, tree_cell);
		}
		for (layer_iterator domcell = std::next(leftcell, 1); domcell != new_cell_it; ++domcell) {
			if (k > 1) {  // At this point, i < index_max_active_layer because for nbpoint == 0 the range [leftcell, rightcell) is empty.
				assert(domcell->a * (domcell->z - p[2]) >0);
  				domcell->add_contrib(point_index, domcell->volume_above + domcell->a * (domcell->z - p[2]));
				domcell->z = p[2];
				domcell->volume_above = 0;
				domcell->to_right = new_cell_it;
				// There are no lower skylines to update here.
				assert(*std::prev(rightcell_in_higher_layer, 1) < *domcell && *domcell < *rightcell_in_higher_layer);
				if (k == 2) {
					auto tree_cell = TCell(domcell->up_x, domcell->up_y, domcell);
					tree.insert(rightcell_in_last_skyline, tree_cell);
				}
			}
			else {
				domcell->add_last_vol(domcell->volume_above + domcell->a * (domcell->z - p[2]));
				contribs_v.insert(std::make_pair(std::make_pair(domcell->up_x, domcell->up_y), ContribPoint<T>(domcell->up_points, domcell->cumulative_vol)));
			}
			a += (p[0] - last_x) * (last_y - domcell->up_y);
			lows.push_back({last_x, domcell->up_y});
			last_x = domcell->up_x;
			last_y = domcell->up_y;
		}
		if (k > 1) {
			higher_layer.splice(rightcell_in_higher_layer, current_layer, std::next(leftcell, 1), new_cell_it);
		}
		else {
			current_layer.erase(std::next(leftcell, 1), new_cell_it);
		}
		lows.push_back({last_x, rightcell->up_y});
		a += (p[0] - last_x) * (last_y - rightcell->up_y);
		new_cell_it->a = a;
		new_cell_it->low_stairs = lows;

		// Updating the outdated pointers to_right that were dummy toward new_cell_it.
		if (k > 1) {
			// Updating pointers hcell.to_right of every hcell from the range [leftcell_in_higher_layer, rightcell_in_higher_layer);
			layer_iterator hcell = leftcell_in_higher_layer;
			if (hcell->up_y == max_T_value) {
				++hcell;
			}
			while (hcell->up_y > rightcell->up_y) {
				// We do not stop at rightcell_in_higher_layer because if there has been a rightsplit
				// then std::prev(rightcell_in_higher_layer,1).to_right = rightcell instead of new_cell_it, for instance.
				hcell->to_right = new_cell_it;
				++hcell;
			}
		}
	}
	else {  // Case i > 0. Not creating a new cell but (possibly) splitting left/right cells and moving dominated cells to next layer.
		// Deleting and moving dominated cells.
		if (i < k-1) {
			for (layer_iterator domcell = std::next(leftcell, 1); domcell != rightcell; ++domcell) {
				domcell->add_contrib(point_index, domcell->volume_above + domcell->a * (domcell->z - p[2]));
				domcell->z = p[2];
				domcell->volume_above = 0;
				assert(*std::prev(rightcell_in_higher_layer, 1) < *domcell && *domcell < *rightcell_in_higher_layer);
				if (i == k-2) {
					auto tree_cell = TCell(domcell->up_x, domcell->up_y, domcell);
					tree.insert(rightcell_in_last_skyline, tree_cell);
				}
			}
			higher_layer.splice(rightcell_in_higher_layer, current_layer, std::next(leftcell, 1), rightcell);
		}
		else {
			for (layer_iterator domcell = std::next(leftcell, 1); domcell != rightcell; ++domcell) {
				domcell->add_last_vol(domcell->volume_above + domcell->a * (domcell->z - p[2]));
				contribs_v.insert(std::make_pair(std::make_pair(domcell->up_x, domcell->up_y), ContribPoint<T>(domcell->up_points, domcell->cumulative_vol)));
			}
			current_layer.erase(std::next(leftcell, 1), rightcell);
		}
	}
	assert(leftcell->up_y >= p[1]);
	assert(std::next(leftcell, 1)->up_y < p[1] || (i==0 && std::next(leftcell, 1)->up_y == p[1]));
	assert(rightcell->up_x >= p[0]);
	assert(std::prev(rightcell, 1)->up_x < p[0] || (i==0 && std::prev(rightcell, 1)->up_x == p[0]));
	assert(leftcell_in_higher_layer->up_y >= p[1] && std::next(leftcell_in_higher_layer, 1)->up_y < p[1]);
	assert(rightcell_in_higher_layer->up_x >= p[0] && std::prev(rightcell_in_higher_layer, 1)->up_x < p[0]);

	return std::make_tuple(leftcell, rightcell, leftcell_in_higher_layer, rightcell_in_higher_layer);
}

template<typename Point, typename ContainerOutput>
void hypervolume_contributions_sorted(int k, std::vector<Point> &points, ContainerOutput &contribs_v) {
	typedef typename Point::value_type T;
	typedef typename hssp_contribution::Chain_Cell<T> Cell;
	typedef typename hssp_contribution::Tree_Cell<T> TCell;
	typedef typename std::set<TCell, std::less<void>> skyline_set;
	typedef typename std::list<Cell> skyline_layer;
	typedef typename skyline_layer::iterator layer_const_iterator;
	typedef typename skyline_layer::iterator layer_iterator;
	typedef typename skyline_set::iterator set_iterator;
	assert(k >= 1);
	assert(std::is_sorted(points.begin(), points.end(),[](Point p, Point q){return p[2] > q[2];}));
	const T max_T_value = std::numeric_limits<T>::max();


	// tempsky[i] represents layer i of skyline: cells jointly dominated by i+1 points.
	// Initializes skyline layers with 2 dummy cells.

	std::vector<skyline_layer> tempsky;
	tempsky.reserve(k+2);
	for (int i = 0; i < k+2; ++i) {
		auto cl = Cell(0, max_T_value);
		auto cr = Cell(max_T_value, 0);
		tempsky.push_back({cl, cr});
	} // The layers tempsky[k+1] and tempsky[k] is only used to make sure we have higher_layer arguments to pass to hypervolume_contributions_iteration_splice.
	auto cl = TCell(0, max_T_value, tempsky[k-1].begin());
	auto cr = TCell(max_T_value, 0, std::next(tempsky[k-1].begin()));
	skyline_set tree = {cl, cr};
	int point_index = -1;

	for (auto p : points) { // Invariant: (index_max_active_layer + 1) is the number of points that have already been inserted before p,
		++point_index;
		int index_max_active_layer = std::max(0,std::min(point_index-1, k-1));
		// index_max_active_layer is the index of the maximal skyline layer that is not trivial ({cl, cr}), replaced with 0 when there is no such layer.
		// so, as point index increases, index_max_active_layer takes values : 0, 0, 1, 2, ... k-1, k-1, k-1...
		auto c = TCell(p[0], p[1]);
		layer_const_iterator leftcell_in_higher_layer = tempsky[index_max_active_layer+1].begin();  // Hint used to speedup insertion: records the last cell with up_y >= p[1] in the previous layer.
		layer_const_iterator rightcell_in_higher_layer = std::prev(tempsky[index_max_active_layer+1].end(), 1);  // Hint used to speedup insertion: records the first cell with up_x >= p[0] in the previous layer.
		layer_const_iterator leftcell_in_layer_above_higher_layer = tempsky[index_max_active_layer+2].begin();  // Hint used to speedup insertion: records the last cell with up_y >= p[1] in the previous layer.
		layer_const_iterator rightcell_in_layer_above_higher_layer = std::prev(tempsky[index_max_active_layer+2].end(), 1);  // Hint used to speedup insertion: records the first cell with up_x >= p[0] in the previous layer.
		layer_iterator rightcell;
		layer_iterator leftcell;

		set_iterator leftcell_in_last_skyline;
		set_iterator rightcell_in_last_skyline;

		if (point_index >= k || k == 1) {  // Case when the highest active layer is [k-1]: we perform a binary search for left/rightcell.
			int i = k-1;

			rightcell_in_last_skyline = tree.lower_bound(c);  // First point greater on x. Using set::lower_bound is presumably faster than std::lower_bound.
			leftcell_in_last_skyline = tree.template lower_bound<T>(c.up_y);  // Using the ``trick'' of searching the set on an order (over T values) correlated with the set's underlying order (over cells).
			rightcell = rightcell_in_last_skyline->to_cell;
			leftcell = leftcell_in_last_skyline->to_cell;
			--leftcell;  // leftcell is now the last point greater on y
			tree.erase(leftcell_in_last_skyline, rightcell_in_last_skyline);

			auto res = hypervolume_contributions_iteration_splice<Point, skyline_layer, skyline_set, ContainerOutput>(
				i,
				k,
				point_index,
				p,
				leftcell_in_higher_layer,
				rightcell_in_higher_layer,
				leftcell_in_layer_above_higher_layer,
				rightcell_in_layer_above_higher_layer,
				leftcell,
				rightcell,
				rightcell_in_last_skyline,
				contribs_v,
				tempsky,
				tree
				);
			leftcell_in_higher_layer = std::get<0>(res);
			rightcell_in_higher_layer = std::get<1>(res);
			leftcell_in_layer_above_higher_layer = std::get<2>(res);
			rightcell_in_layer_above_higher_layer = std::get<3>(res);
			assert(leftcell_in_higher_layer != tempsky[i].end() && rightcell_in_higher_layer != tempsky[i].end());
		}
		else {
			leftcell_in_last_skyline = tree.end();  // dummy value.
			rightcell_in_last_skyline = tree.end();  // dummy value.
		}

		if (k > 1) {
			for (int i = std::min(index_max_active_layer, k-2); i >= 0; --i) {  // The skyline layers greater than index_max_active_layer are empty, the smaller ones (under general position assumption) are not.
				// Update left/rightcell using the value of left/rightcell_in_higher_layer and pointers to_right.
				if (leftcell_in_higher_layer == tempsky[i+1].begin()) {
					leftcell = tempsky[i].begin();
					if (std::next(leftcell, 1)->up_y >= p[1]) {
						leftcell = std::next(leftcell, 1);
					}
				}
				else {
					leftcell = leftcell_in_higher_layer->to_right;
				}
				if (rightcell_in_higher_layer->up_x != max_T_value) {
					rightcell = rightcell_in_higher_layer->to_right;
				}
				else {
					rightcell = std::prev(tempsky[i].end(), 1);
				}
				if (std::prev(rightcell, 1)->up_x >= p[0]) {
					rightcell = std::prev(rightcell, 1);
				}

				auto res = hypervolume_contributions_iteration_splice<Point, skyline_layer, skyline_set, ContainerOutput>(
					i,
					k,
					point_index,
					p,
					leftcell_in_higher_layer,
					rightcell_in_higher_layer,
					leftcell_in_layer_above_higher_layer,
					rightcell_in_layer_above_higher_layer,
					leftcell,
					rightcell,
					rightcell_in_last_skyline,
					contribs_v,
					tempsky,
					tree
					);
				leftcell_in_higher_layer = std::get<0>(res);
				rightcell_in_higher_layer = std::get<1>(res);
				leftcell_in_layer_above_higher_layer = std::get<2>(res);
				rightcell_in_layer_above_higher_layer = std::get<3>(res);
			}
		}
	}

	// Adding to result all cells that have not been dominated (except the dummy extremities of each layer).

	for (int i = 0; i < k; ++i) {
		for (auto domcell : tempsky[i]) {
			if (domcell.up_x != max_T_value && domcell.up_y != max_T_value) {
				domcell.add_last_vol(domcell.volume_above + domcell.a * (domcell.z));
				contribs_v.insert(std::make_pair(std::make_pair(domcell.up_x, domcell.up_y), ContribPoint<T>(domcell.up_points, domcell.cumulative_vol)));
			}
		}
	}

}


template<typename Point, typename ContainerOutput>
std::pair<std::vector<size_t>, ContainerOutput> hypervolume_contributions(int k, std::vector<Point> points) {// Passing points by value has a slight overhead but allows to keep input constant.
	ContainerOutput contribs_v;
	std::vector<size_t> index_map(points.size());
	std::iota(index_map.begin(), index_map.end(), 0);
	std::sort(index_map.begin(), index_map.end(), [&points](size_t i1, size_t i2) {
		return points[i1][2]> points[i2][2];
	});
	std::sort(points.begin(), points.end(), [](Point p, Point q){return p[2] > q[2];});
	// index_map[i] will record the position in the input vector of the i^th point by decreasing order on z (i.e., the position of points[i] in the input vector passed as argument to the function).
	hypervolume_contributions_sorted<Point,ContainerOutput>(k, points, contribs_v);
	return std::make_pair(index_map, contribs_v);
}

}  // namespace hssp_contribution
#endif /* HYPVCONTRIBUTIONS_CHAIN_SPLICE6_H_ */
