/*
 * cell.h
 *
 *  Created on: Jul 19, 2016
 *      Author: bgroz
 *
 * Implements cell and result_cell.
 *
 * A result_cell records the hypervolume jointly dominated by  points from up_points.
 *
 * The cell structure is exploited by the hypervolume contribution algorithm to compute the corresponding result_cell.
 * A cell boundary is defined by uppper corner plus lower staircase (which evolves; represents boundary at a given slie (z-level).
 * A cell also includes variables updated throughout the algorithm computing the cell volume.
 *
 * Parameter T: type of point coordinates: can be int, float(decimal)...
 */


#ifndef CELL_CHAIN_H_
#define CELL_CHAIN_H_

#include <list>
#include <unordered_map>
#include <vector>

namespace hssp_contribution {


template<typename T>
class ContribPoint {
public:
	std::vector<int> up_points;  // Points dominating the cell.
	std::vector<T> cumulative_vol; 
	// At the end of the algorithm (after calling add_last_vol): 
	//  Let j=up_points.size()-cumulative_vol.size().
	//  For i>0, cumulative_vol[i] - cumulative_vol[i-1] is the volume of the cell dominated by {up_points[0],...,up_points[i+j]}.
	//  cumulative_vol[0] is the volume of the cell dominated by up_points[0],...,up_point[j]
	// Before we call function add_last_vol():
	//  The same property holds if we pop up_points.back(), for which the corresponding volume has not been recorded yet.	
	ContribPoint () {
	}
	ContribPoint (std::vector<int> upp, std::vector<T> vol) : up_points(upp), cumulative_vol(vol) {
	}
	void add_point(int upp) {
		up_points.push_back(upp);
	}
	void add_contrib(int upp, T vol) {
		assert(vol>0);
		up_points.push_back(upp);
		if (cumulative_vol.size() == 0) {
			cumulative_vol.push_back(vol);
		}
		else {
			cumulative_vol.push_back((cumulative_vol.back()) + vol);
		}
	}
	void add_last_vol(T vol) { 
	// This function will be called only once, at the end of the algorithm.
	// Once it is called, the ContribPoint is 'frozen', i.e., not modified anymore.
		assert(vol>0);
		if (cumulative_vol.size() == 0) {
			cumulative_vol.push_back(vol);
		}
		else {
			cumulative_vol.push_back((cumulative_vol.back()) + vol);
		}
	}
	std::string to_string() const {
		std::string s ="uppoints: ";
		s += utils::to_string<std::vector<int>>(up_points);
		s += "\n volumes: ";
		s+=  utils::to_string<std::vector<T>>(cumulative_vol);
		return s;
	}
};

template<typename T>
struct Chain_Cell {
public:
	typedef typename std::list<Chain_Cell<T>> skyline_layer;
	typedef typename skyline_layer::iterator layer_iterator;

	T up_x;  // Coordinates on x and y of the upper corner of the cell.
	T up_y;

	// Staircase represented as list of points (the down/left extremity of each step) sorted by increasing x, together with starting index (the index below starting index are stale points).
	std::list<std::pair<T,T>> low_stairs;

	T a;
	T z;  // z-value of last modification to cell.
	T volume_above;  // hypervolume contribution of slices above.

//	layer_iterator to_left;  // Last cell c in prev layer (i.e., dominated by 1 more point) with c.up_y == up_y (cells after have c.up_y < up_y) or prev_layer.end() if none.
	layer_iterator to_right;  // Last cell c in next layer (i.e., dominated by 1 less point) with c.up_y >= up_y (cells after have c.up_y < up_y) or current_layer.end() if no next layer.
//	res_iterator to_res;

	std::vector<int> up_points;  // Points dominating the cell.
	std::vector<T> cumulative_vol;

	Chain_Cell(T upx, T upy) :
		up_x(upx), up_y(upy), low_stairs({{0, 0}}), a(upx * upy), z(1), volume_above(0), to_right(), up_points(), cumulative_vol()
	{
	}
	Chain_Cell(T upx, T upy, std::list<std::pair<T,T> > lows, T na, T nz, T hypv, layer_iterator cr, std::vector<int> upp, std::vector<T> vol) :
		up_x(upx), up_y(upy), low_stairs(lows), a(na), z(nz), volume_above(hypv), to_right(cr), up_points(upp), cumulative_vol(vol)
	{
	}
	bool operator <(const Chain_Cell<T> &q) const {  //p<q if p smaller on x (used as order for skyline layers)
		return up_x < q.up_x;
	}
	bool operator <(const T &val_y) const { //p>q if p larger on y (used to identify rightcell)
		return up_y > val_y;
	}
	bool operator >(const Chain_Cell<T> &q) const {
		return up_y > q.up_y;
	}
	void print_cell() const {
		std::cout<<"cell upcorner: "<<up_x<<","<<up_y<<std::endl;
		std::cout<<"z:"<<z<<", hypv_above:"<<volume_above<<", a:"+std::to_string(a)<<std::endl;
	}
	void add_point(int upp) {
		up_points.push_back(upp);
	}
	void add_contrib(int upp, T vol) {
		assert(vol>0);
		up_points.push_back(upp);
		if (cumulative_vol.size() == 0) {
			cumulative_vol.push_back(vol);
		}
		else {
			cumulative_vol.push_back((cumulative_vol.back()) + vol);
		}
	}
	void add_last_vol(T vol) {
	// This function will be called only once, at the end of the algorithm.
	// Once it is called, the ContribPoint is 'frozen', i.e., not modified anymore.
		assert(vol>0);
		if (cumulative_vol.size() == 0) {
			cumulative_vol.push_back(vol);
		}
		else {
			cumulative_vol.push_back((cumulative_vol.back()) + vol);
		}
	}
	std::string to_string() const {
		std::string s ="";
		if (up_x != 0 && up_y != 0) {
			s = " , to_right: "+std::to_string(to_right->up_x)+","+std::to_string(to_right->up_y);
		}
		return "cell upcorner: "+std::to_string(up_x)
		+","+std::to_string(up_y)
		+s
		+"\n, z:"+std::to_string(z)
		+", hypv_above:"+std::to_string(volume_above)
		+", a:"+std::to_string(a);
	}
};


template<typename T>
struct Tree_Cell {
public:
	typedef typename std::list<Chain_Cell<T>>::iterator cellptr;
	T up_x;
	T up_y;
	cellptr to_cell;
	Tree_Cell(T upx, T upy) :
		up_x(upx), up_y(upy), to_cell()
	{
	}
	Tree_Cell(T upx, T upy, cellptr c) :
		up_x(upx), up_y(upy), to_cell(c)
	{
	}
	bool operator <(const Tree_Cell<T> &q) const {  //p<q if p smaller on x (used as order for skyline layers)
		return up_x < q.up_x;
	}
	bool operator <(const T &val_y) const { //p>q if p larger on y (used to identify rightcell)
		return up_y > val_y;
	}
	bool operator >(const Tree_Cell<T> &q) const {
		return up_y > q.up_y;
	}
	std::string to_string() const {
		return "tree cell upcorner: "+std::to_string(up_x)
		+","+std::to_string(up_y) + " pointing toward cell:" + to_cell->to_string();
	}
};

}  // namespace hssp_contribution
#endif /* CELL_CHAIN_H_ */
