/*
 * RangeStructLowerInc.cpp
 *
 *  Created on: Aug 17, 2016
 *      Author: bgroz
 *
 * assumes input points form a 3d skyline
 * assumes general position: not a pb for construction but pb at query time if query point = split value on dim coordinate.
 * assumes Points::value_type is signed as we multiply by -1 when looking for extreme point on hull in given direction.
  * ``Lower'' because we exploit the property of 3d skylines (that p <_1 q && p <_2 q => p >_3 q) to solve the pb with three 2d range trees  instead of one 3d range tree.
 * ``Incremental'' because we add points to the range tree hull iteratively , by increasing order on component dim_insertion
 *      to replace each 2d range tree with a 1d range tree.
 */

#ifndef RANGESTRUCTLOWERINC_HPP
#define RANGESTRUCTLOWERINC_HPP

#include <algorithm>
#include <array>
#include <bitset>
#include <cmath>
#include <functional>
#include <iterator>
#include <iostream>
#include <memory>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "hull/upperhull_updatable.hpp"
#include "hull/upperhull_updatable_double.hpp"
#include "utils/point_representation.hpp"
#include "utils/print_helpers.hpp"

namespace rangetree {

template<typename Point> class RangeStructLowerIncTree;

template <typename Point> struct RangeStructLowerIncNode {
private:
	typedef typename std::unique_ptr<RangeStructLowerIncNode<Point>> Node_unique_ptr;
	typedef typename Point::value_type ValueType;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;
	typedef utils::Hull_Point_With_Slope<ValueType> HullPoint;
	typedef utils::Point2d<ValueType> HullQueryPoint;
	typedef typename hull::UpperHull_Set<ValueType, HullPoint, HullQueryPoint> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;
public:
	RangeStructLowerIncNode() : split_value(), left(nullptr), right(nullptr), hull(nullptr), truncated(false), points() {};
	// Each node represents implicitly a set of input points (its descendants in the range tree).
	// * left and right nodes represent respectively the lower and upper half of descendants, split around the median according to coordinate[dim], dim being given by the tree containing the node.
	//   split point belongs to left.
	ValueType split_value;
	Node_unique_ptr left;
	Node_unique_ptr right;
	// No particular reason for storing pointers to UpperHull_Set instead of directly the hull here.
	Hull_unique_ptr hull;
	bool truncated;
	// Container with all subtree points. Only defined when node is truncated.
	ContainerWithId points;
};



template<typename Point> class RangeStructLowerIncTree {
private:
	typedef typename std::unique_ptr<RangeStructLowerIncNode<Point>> Node_unique_ptr;
	typedef typename Point::value_type ValueType;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;
	typedef typename ContainerWithId::const_iterator Points_const_it;
	typedef utils::Hull_Point_With_Slope<ValueType> HullPoint;
	typedef utils::Point2d<ValueType> HullQueryPoint;
	typedef typename hull::UpperHull_Set<ValueType, HullPoint, HullQueryPoint> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;
	
	Node_unique_ptr root_;
	const ContainerWithId &copy_container_with_id_;
	const int dim_tree_;  // dimension on which we order the tree.
	const int dim_insertion_;  // the points get activated by increasing order on dim_insertion
	const int truncsize_;
public:
	// Complexity: O(n log n), where n is std::distance(begin, end).
	// Space used: O(n).
	RangeStructLowerIncTree(const ContainerWithId &copy_container_with_id, int dim_tree, int dim_insertion, int truncsize): \
		copy_container_with_id_(copy_container_with_id), dim_tree_(dim_tree), dim_insertion_(dim_insertion), truncsize_(truncsize) {
		ContainerWithId tree_points_;
		std::copy(copy_container_with_id.begin(), copy_container_with_id.end(), std::back_inserter(tree_points_));
		std::sort(tree_points_.begin(), tree_points_.end(), [&](PointWithId a, PointWithId b) {
			return a.point[dim_tree] < b.point[dim_tree];
		});
		// Building the tree.
		root_ = nullptr;
		root_ = make_tree(tree_points_.begin(), tree_points_.end(), true);
	}

	// Complexity: O(log^(2) n + output size).
	// Searches which of the input points best matches the tree_query_point and inserts the point into the hull where it belongs.
	// The search is restricted to points that are smaller than query_point on dim_insertion_ and dim_tree_, plus a few other nodes because of truncation.
	//   temp_optimum records the temporary optimum, and will be updated if a better pair is found through hull queries,
	void tree_query(PointWithId query_point, std::pair<size_t, ValueType> &temp_optimum, ValueType vol1) {
		node_query(query_point, root_, temp_optimum, vol1, true);
	}

private:
	// Recursively creates the range tree.
	// Complexity: O(n) where n = std::distance(begin, end).
	Node_unique_ptr make_tree(Points_const_it begin, Points_const_it end, bool is_left) {
		Node_unique_ptr ptr_node(new RangeStructLowerIncNode<Point>());
		// Creating the new tree node, along with its info.
		// Copy the actual point values into the rangetree::node only in the case of truncated nodes
		// Subtrees left/right split range in half so that largest half is left (an arbitrary choice).

		long length = std::distance(begin, end);
		if(length <= truncsize_) {
			ptr_node->split_value = begin->point[dim_tree_];
			ptr_node->left = nullptr;
			ptr_node->right = nullptr;
			ptr_node->points = ContainerWithId();
			std::copy(begin, end, std::back_inserter(ptr_node->points));  // we could strip the id to save space as we don't need it for points (have no hull) but then node_scan will use PointWithoutId in temp_optimum , whereas get_extreme uses Index...
			ptr_node->truncated = true;
		}
		else {
			if(length%2 == 1) {  // Splitting in half for odd splits.
				ptr_node->split_value = (begin+length/2)->point[dim_tree_];
				ptr_node->left = make_tree(begin, begin+length/2+1, true);
				ptr_node->right = make_tree(begin+length/2+1, end, false);
			}
			else {  // Splitting in half for even splits.
				ptr_node->split_value = (begin+length/2-1)->point[dim_tree_];
				ptr_node->left = make_tree(begin, begin+length/2, true);
				ptr_node->right = make_tree(begin+length/2, end, false);
			}
			// Remark: we do not compute hull in nodes that are right children of their parent as they will not be used.
			if (is_left) {
				Hull_unique_ptr ptr_hull(new Hull_class());
				ptr_node->hull = std::move(ptr_hull);
			}
			else {
				ptr_node->hull = nullptr;
			}
		}
		return ptr_node;
	}


	ValueType pair_hypervolume(const Point &p, const Point &q, ValueType vol1) {
		ValueType common_vol = std::min(p[0], q[0]) * std::min(p[1], q[1]) * std::min(p[2], q[2]);
		ValueType vol2 = q[0] * q[1] * q[2];
		return vol1 + vol2 - common_vol;
	}


	void node_points_scan(const Point &query_point, \
			Points_const_it begin, \
			Points_const_it end, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		for (auto q = begin; q != end; ++q) {
			ValueType pair_vol = pair_hypervolume(query_point, q->point, vol1);
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {q->id, pair_vol};
			}
		}
	}

	// Queries the convex hull at node to identify the active point below the node that maximizes volume together with query_point.
	// temp_optimum is updated if the resulting hypervolume is higher.
	// Assumes vol1 is the hypervolume below query_point.
	// Complexity: If node->truncated is false, O(log h), otherwise O(h), where h = hull_size.
	void get_extreme(const Point &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		if (node->truncated) {
			node_points_scan(query_point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else if (node->hull->get_hull_size() > 0) {
			HullQueryPoint p = {-query_point[3 - dim_tree_ - dim_insertion_], 1};
			size_t extreme_point_id = node->hull->Extreme(p).id;
			ValueType pair_vol = pair_hypervolume(query_point, copy_container_with_id_[extreme_point_id].point, vol1);
			// we could avoid this call to pair_hypervolume, and remove the copy_container reference within trees:
			// vol2 is coordinate 1 from  extreme_point, and common_vol can be computed from query_point, and coordinate 0 of extreme_point.
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {extreme_point_id, pair_vol};
			}
		}
	}



	// Returns the best match for query_point among active points below "node".
	// Remark: here we need the id of the point for the insertion into the hull.
	void node_query(const PointWithId &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1, \
			bool is_left) {
		if (node->truncated) {
			node_points_scan(query_point.point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else {
			assert(node->left != nullptr);
			assert(node->right != nullptr);

			if (is_left) {
				HullPoint new_hull_point = {
						query_point.point[this->dim_tree_] * query_point.point[this->dim_insertion_],
						query_point.point[this->dim_tree_] * query_point.point[this->dim_insertion_] * query_point.point[3 - this->dim_tree_ - this->dim_insertion_],
						query_point.id,
						std::numeric_limits<double>::max()
				};
				node->hull->add_point(new_hull_point);
			}
			if (query_point.point[dim_tree_] <= node->split_value) {
				node_query(query_point, node->left, temp_optimum, vol1, true);
			}
			else {
				node_query(query_point, node->right, temp_optimum, vol1, false);
				get_extreme(query_point.point, node->left, temp_optimum, vol1);
			}
		}
	}
};


// Builds a range tree for points in {begin,..., end} where each node contains the hull of its points projected on {coord_dim_tree * coord_dim_insertion, coord0 * coord1 * coord2}.
// Constructor builds the tree structure from the start with all points, but Hulls are initially empty.
// Best_pair EITHER returns the pair (p1,p2) that dominates the maximal hypervolume such that p2 is greater on dim_insertion and dim_tree OR (p1, p2') where p2' is a better point than the best such (p1,p2).
//  => a better point could be found in truncated nodes as we do not check dimension orders there.
//  For this, best_pair considers points by increasing order on dim_insertion, and for each point:
//  - we query the orthant 'points smaller on dim_tree' (and by construction also smaller on dim_insertion, therefore larger on dim because input is a 3D skyline)
//  - we activate the point: insert the point in its respective hull.
template<typename InputPoint, class InputContainer> class RangeStructLowerInc1d {
private:
	typedef typename InputPoint::value_type ValueType;
	typedef InputPoint Point;
	typedef utils::PointWithId<Point> PointWithId;
	typedef std::vector<PointWithId> ContainerWithId;

	std::unique_ptr<RangeStructLowerIncTree<Point>> rtree1_;

	// Records input points, sorted in increasing order on dim_insertion.
	ContainerWithId copy_container_with_id_;
public:

	RangeStructLowerInc1d(typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int dim, int truncsize) {
	//		assert (truncsize >= 1);  // The absence of truncation should be expressed with truncsize = 1.
	//		assert(0<= dim && dim <= 2);
		int dim_tree = 0;
		int dim_insertion = 0;
		switch (dim) {
		case 2 : {
			dim_tree = 1;
			dim_insertion = 0;
			break;
		}
		case 1 : {
			dim_tree = 2;
			dim_insertion = 0;
			break;
		}
		case 0 : {
			dim_tree = 2;
			dim_insertion = 1;
			break;
		}
		}

		// Not very efficient (would probably be faster if we sort the input (begin, end) then do a single transform
		std::transform(begin, end, std::back_inserter(copy_container_with_id_), [] (Point input_point) {return PointWithId(input_point, 0);});
		size_t i = 0;
		std::sort(copy_container_with_id_.begin(), copy_container_with_id_.end(), [dim_insertion](PointWithId a, PointWithId b) {return a.point[dim_insertion] < b.point[dim_insertion]; });
		for (auto pt = copy_container_with_id_.begin(); pt != copy_container_with_id_.end(); ++pt) {
			pt->id = i++;
		}
		this->rtree1_ = std::unique_ptr<RangeStructLowerIncTree<Point>>(new RangeStructLowerIncTree<Point>(
				copy_container_with_id_, dim_tree, dim_insertion, truncsize));
	}

	std::tuple<InputPoint, InputPoint, ValueType> best_pair() {
		std::pair<size_t, ValueType> temp_optimum = {0, std::numeric_limits<ValueType>::min()};
		ValueType prev_opt_value = std::numeric_limits<ValueType>::min();
		Point opt_query_point;
		for (auto query_point : copy_container_with_id_) {
			ValueType vol1 = query_point.point[0] * query_point.point[1] * query_point.point[2];
			rtree1_->tree_query(query_point, temp_optimum, vol1);
			if (temp_optimum.second > prev_opt_value) {
				opt_query_point = query_point.point;
				prev_opt_value = temp_optimum.second;
			}
		}
		InputPoint opt_match_point = copy_container_with_id_[temp_optimum.first].point;
		return std::make_tuple(opt_match_point, opt_query_point, temp_optimum.second);
	}
};


template<typename InputPoint, class InputContainer>
std::tuple<InputPoint, InputPoint, typename InputPoint::value_type> range_struct_lower_inc (typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int truncsize) {
	assert(truncsize >= 1);  // The absence of truncation should be expressed with truncsize = 1.
	assert((*begin)[1] * (*begin)[2] < std::numeric_limits<typename InputPoint::value_type>::max()/(*begin)[0]);  // Otherwise high risk of overflow.
	typedef typename InputPoint::value_type T;
	RangeStructLowerInc1d<InputPoint, InputContainer> rtree1a(begin, end, 0, truncsize);
	std::tuple<InputPoint, InputPoint, T> result = rtree1a.best_pair();
	for (int dim = 1; dim <= 2; ++dim) {
		RangeStructLowerInc1d<InputPoint, InputContainer> rtree1b(begin, end, dim, truncsize);
		std::tuple<InputPoint, InputPoint, T> res = rtree1b.best_pair();
		if (std::get<2>(res) > std::get<2>(result)) {
			result = res;
		}
	}
	return result;
};



}  // namespace rangetree

#endif /* RANGESTRUCTLOWERINC_HPP */
