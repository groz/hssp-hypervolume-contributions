/*
 * RangeStructLower.cpp
 *
 *  Created on: Aug 17, 2016
 *      Author: smaniu, bgroz
 *
 * assumes input points form a 3d skyline
 * assumes general position: not a pb for construction but pb at query time if query point = split value on dim coordinate.
 * assumes Points::value_type is signed as we multiply by -1 when looking for extreme point on hull in given direction.
  * ``Lower'' because we exploit the property of 3d skylines (that p <_1 q && p <_2 q => p >_3 q) to solve the pb with three 2d range trees  instead of one 3d range tree.
 */

#ifndef RANGESTRUCTLOWER_HPP
#define RANGESTRUCTLOWER_HPP



#include <algorithm>
#include <array>
#include <bitset>
#include <cmath>
#include <functional>
#include <iterator>
#include <iostream>
#include <memory>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "hull/cvxhull.hpp"
// #include "hull/cvxhull_double.hpp"
#include "hull/cvxhull_double_without_slopes.hpp"
#include "utils/point_representation.hpp"
#include "utils/print_helpers.hpp"


namespace rangetree {
//std::string display=""; //FOR debuging only

// Optimizations: we try to store only hull that are relevant:
// As we only compare query_points with points that are smaller on the 2 dimensions (current, other), right children have neither subtrees nor hulls


template<typename Point> class RangeStructLowerTree;


template <typename Point> struct RangeStructLowerNode {
private:
	typedef typename Point::value_type ValueType;
	typedef utils::Point2d<ValueType> QueryPoint;
	typedef utils::PointWithId2d<ValueType> HullPoint;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;
	typedef typename hull::ConvexHull<ValueType, HullPoint, QueryPoint, std::vector<HullPoint>> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;
public:
	RangeStructLowerNode(): split_value(), subtree(nullptr), left(nullptr), right(nullptr), hull(nullptr), truncated(false), points() {};
	// Each node represents implicitly a set of input points (its descendants in the range tree).
	// * left and right nodes represent respectively the lower and upper half of descendants, split around the median according to coordinate[dim], dim being given by the tree containing the node.
	//   split point belongs to left.
	// * subtree points to the tree  copying the node's subtree, but in dimension dim-1.
	// * hull is only defined when dimension is 1.
	//   For each i< 2^global_dim, we can view i in binary as a subset of dimensions;
	//     hull[i] then stores the hull obtained when mapping input_points to :
	//     {product of coordinates in dimset, product of all coordinates, index of original point in global root's copy_container}
	ValueType split_value;
	std::unique_ptr<RangeStructLowerTree<Point>> subtree;
	std::unique_ptr<RangeStructLowerNode<Point>> left;
	std::unique_ptr<RangeStructLowerNode<Point>> right;
	// No particular reason for storing pointers to ConvexHull instead of directly the hull here.
	Hull_unique_ptr hull;
	bool truncated;
	// Container with all subtree points. Only defined when node is truncated.
	ContainerWithId points;

	//	ContainerWithId dbpoints;// DEBUG
};



template<typename Point> class RangeStructLowerTree {

private:
	typedef typename std::unique_ptr<RangeStructLowerNode<Point>> Node_unique_ptr;
	typedef typename Point::value_type ValueType;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;
	typedef typename ContainerWithId::const_iterator Points_const_it;
	typedef utils::Point2d<ValueType> QueryPoint;
	typedef utils::PointWithId2d<ValueType> HullPoint;
	typedef typename hull::ConvexHull<ValueType, HullPoint, QueryPoint, std::vector<HullPoint>> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;
	
	std::unique_ptr<RangeStructLowerNode<Point>> root_;
	const ContainerWithId &copy_container_with_id_; 
	const int dim_current_;  // dimension on which we order the current tree.
	const int dim_other_;  // dimension of the root tree if is_subtree_, otherwise dimension on which we order the lower_dimensional subtree.
	const bool is_subtree_;  // true if there is no lower-dimensional subtree
	const int truncsize_;
public:
	// Complexity: O(n log^(3) n), where n is std::distance(begin, end) when !is_subtree, and O(n log^(2) n) when is_subtree.
	// We could gain a log factor if we pass sorted points to the hull and define a corresponding constructor that buils hull from sorted points (but would make code more complex).
	// Builds a tree sorted on dim_current. 
	// If is_subtree, builds hull in each node. coordinate of point p in hull: (p[dim_current_ * dim_other_], p[0] * [[1] * p[2]]).
	// If !is_subtree, builds a subtree sorted on dim_other for each node.
	// Space used: same as complexity.
	RangeStructLowerTree(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end, int dim_current, int dim_other, bool is_subtree, int truncsize): \
	copy_container_with_id_(copy_container_with_id), dim_current_(dim_current), dim_other_(dim_other), is_subtree_(is_subtree), truncsize_(truncsize) {
		// Copy the parameters and the iterator.
		ContainerWithId copy_container;
		std::copy(begin, end, std::back_inserter(copy_container));
		// Sort the points by the coord [dim-1].
		std::sort(copy_container.begin(), copy_container.end(), [&](PointWithId a, PointWithId b) {
			return (a.point)[dim_current] < (b.point)[dim_current];
		});
		// Building the tree.
		root_ = nullptr;
		root_ = make_tree_root(copy_container_with_id, copy_container.begin(), copy_container.end());
	}

	// Complexity: O(log^(3) n).
	// Searches which of the input points best matches the tree_query_point.
	// The search is restricted to points that are smaller than query_point on dim_current_ and dim_other_, plus a few other nodes because of truncation.
	//   temp_optimum records the temporary optimum, and will be updated if a better pair is found through hull queries,
	void tree_query(Point query_point, std::pair<size_t, ValueType> &temp_optimum, ValueType vol1) {
		// Ensure the points are ordered.
		//		display.push_back(' ');
		//		std::cout<<display+"launching tree_query for point"<<to_string<Point>(query_point,",","(",")")<<" dim:"<<dim<<" orthant:"<<orthant<<std::endl;

		node_query(query_point, root_, temp_optimum, vol1);

		//		std::cout<<display+"concluding tree_query for point"<<to_string<Point>(query_point,",","(",")")<<std::endl;
		//		display.pop_back();

	}

private:
	// Recursively creates the range tree.
	// Remark: here we need the reference to copy_container_with_id so that we can pass it to subtrees.
	Node_unique_ptr make_tree_root(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end) {
		Node_unique_ptr ptr_node(new RangeStructLowerNode<Point>());
		// Creating the new tree node, along with its info.
		// Copy the actual point values into the rangetree::node only in the case of truncated nodes
		// (but of course we also copy values into the rangetree:: copycontainer when the dimension decreases).
		// Build the convex hull from the points only in the lowest dimension (dim=1).
		// Subtrees left/right split range in half so that largest half is left (an arbitrary choice).

		//		std::copy(begin, end, std::back_inserter(ptr_node->dbpoints));// DEBUG

		long length = std::distance(begin, end);
		ptr_node->subtree = nullptr;
		if(length <= truncsize_) {
			ptr_node->split_value = begin->point[dim_current_];
			ptr_node->left = nullptr;
			ptr_node->right = nullptr;
			ptr_node->points = ContainerWithId();
			std::copy(begin, end, std::back_inserter(ptr_node->points));  // we could strip the id to save space as we don't need it for points (have no hull) but then node_scan will use PointWithoutId in temp_optimum , whereas get_extreme uses Index...
			ptr_node->truncated = true;
		}
		else {
			if(length%2 == 1) {  // Splitting in half for odd splits.
				ptr_node->split_value = (begin+length/2)->point[dim_current_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2+1, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2+1, end, false);
			}
			else {  // Splitting in half for even splits.
				ptr_node->split_value = (begin+length/2-1)->point[dim_current_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2, end, false);
			}
		}
		return ptr_node;
	}
	
	Node_unique_ptr make_tree(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end, bool is_left) {
		Node_unique_ptr ptr_node(new RangeStructLowerNode<Point>());
		// Creating the new tree node, along with its info.
		// Copy the actual point values into the rangetree::node only in the case of truncated nodes
		// (but of course we also copy values into the rangetree:: copycontainer when the dimension decreases).
		// Build the convex hull from the points only in the lowest dimension (dim=1).
		// Subtrees left/right split range in half so that largest half is left (an arbitrary choice).

		//		std::copy(begin, end, std::back_inserter(ptr_node->dbpoints));// DEBUG

		long length = std::distance(begin, end);
		ptr_node->subtree = nullptr;
		if(length <= truncsize_) {
			ptr_node->split_value = begin->point[dim_current_];
			ptr_node->left = nullptr;
			ptr_node->right = nullptr;
			ptr_node->points = ContainerWithId();
			std::copy(begin, end, std::back_inserter(ptr_node->points));  // we could strip the id to save space as we don't need it for points (have no hull) but then node_scan will use PointWithoutId in temp_optimum , whereas get_extreme uses Index...
			ptr_node->truncated = true;
		}
		else {
			if(length%2 == 1) {  // Splitting in half for odd splits.
				ptr_node->split_value = (begin+length/2)->point[dim_current_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2+1, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2+1, end, false);
			}
			else {  // Splitting in half for even splits.
				ptr_node->split_value = (begin+length/2-1)->point[dim_current_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2, end, false);
			}

			if (is_left) {
				if (!is_subtree_) {  // Create the tree on a lower dimension.
					std::unique_ptr<RangeStructLowerTree<Point>>\
					ptr_subtree(new RangeStructLowerTree<Point>(copy_container_with_id, begin, end, dim_other_, dim_current_, true, truncsize_));
					ptr_node->subtree = std::move(ptr_subtree);
				}
				else {  // Remark: we could spare ourself the computation of hull in nodes that are right children of their parent as they will not be used.
					std::vector<HullPoint> mapped_points = {};
					mapped_points.reserve(length);
					std::transform(begin, end, std::back_inserter(mapped_points),
						[this] (PointWithId input_point) {return make_hullpoint(input_point);});
					// 		[ this] (PointWithId input_point) -> HullPoint {
					// 	const int dim3 = 3-this->dim_current_-this->dim_other_;
					// 	return std::make_tuple(  // Returns 3D Vector: {product of coordinates in dimset, product of all coordinates, index of original point in global root's copy_container}
					// 			input_point.point[this->dim_current_] * input_point.point[this->dim_other_],
					// 			input_point.point[this->dim_current_] * input_point.point[this->dim_other_] * input_point.point[dim3],
					// 			input_point.id
					// 	);
					// });
					Hull_unique_ptr ptr_hull(new Hull_class(mapped_points));
					ptr_node->hull = std::move(ptr_hull);
					//                std::cout<<i<<"dimset:"<<dimset<<" hull size="<<ptr_node->hull.back()->get_hull_size()<<std::endl;
				}
			}
			
			// MIGHT BE slightly faster to compute only once the product of coordinates, as indicated below: could perhaps spare a factor of 2. But it does not seem worth it.
			//			std::vector<ValueType> coord_product;
			//			coord_product.reserve(length);
			//			std::transform(begin, end, std::back_inserter(coord_product),
			//					[ this ](Point input_point) -> ValueType { // Capture global_dim by reference to object.
			//				return {
			//					std::accumulate(input_point.begin(), input_point.begin() + this->global_dim, 1., std::multiplies<ValueType>())
			//				};
			//			});
		}
		return ptr_node;
	}

	HullPoint make_hullpoint(PointWithId input_point) {
		const int dim3 = 3 - this->dim_current_ - this->dim_other_;
		return HullPoint(  // Returns 3D Vector: {product of coordinates in dimset, product of all coordinates, index of original point in global root's copy_container}
				input_point.point[this->dim_current_] * input_point.point[this->dim_other_],
				input_point.point[this->dim_current_] * input_point.point[this->dim_other_] * input_point.point[dim3],
				input_point.id
		);
	}
	
	
	ValueType pair_hypervolume(const Point &p, const Point &q, ValueType vol1) {
		ValueType common_vol = std::min(p[0], q[0]) * std::min(p[1], q[1]) * std::min(p[2], q[2]);
		ValueType vol2 = q[0] * q[1] * q[2];
		return vol1 + vol2 - common_vol;
	}


	void node_points_scan(const Point &query_point, \
			Points_const_it begin, \
			Points_const_it end, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		for (auto q = begin; q != end; ++q) {
			ValueType pair_vol = pair_hypervolume(query_point, q->point, vol1);
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {q->id, pair_vol};
			}
		}
	}

	// Queries the convex hull at node to identify the point below the node that maximizes volume together with query_point.
	// temp_optimum is updated if the resulting hypervolume is higher.
	// Assumes vol1 is the hypervolume below query_point.
	// Complexity: If node->truncated is false, O(log h), otherwise O(h), where h = hull_size.
	void get_extreme(const Point &query_point, \
			Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		if (node->truncated) {
			node_points_scan(query_point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else {
			QueryPoint p(-query_point[3 - dim_current_ - dim_other_], 1);
			size_t extreme_point_id = (node->hull->Extreme(p)).id;
			ValueType pair_vol = pair_hypervolume(query_point, copy_container_with_id_[extreme_point_id].point, vol1);
			// we could avoid this call to pair_hypervolume, and remove the copy_container reference within trees:
			// vol2 can is coordinate 1 from  extreme_point, and common_vol can be computed from query_point, and coordinate 0 of extreme_point.
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {extreme_point_id, pair_vol};
			}
		}
	}



	// Returns the query answers in the subtree with root at "node".
	void node_query(const Point &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		if (node->truncated) {
			node_points_scan(query_point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else if (is_subtree_) {
			assert(node->left != nullptr);
			assert(node->right != nullptr);
			if (query_point[dim_current_] <= node->split_value) {
				node_query(query_point, node->left, temp_optimum, vol1);
				// we do not search for extreme in node->right because we only search points smaller on dim_current_ (and also smaller on dim_other_)
			}
			else {
				node_query(query_point, node->right, temp_optimum, vol1);
				get_extreme(query_point, node->left, temp_optimum, vol1);
			}
		}
		else {
			assert(node->left != nullptr);
			assert(node->right != nullptr);
			if (query_point[dim_current_] <= node->split_value) {
				node_query(query_point, node->left, temp_optimum, vol1);
				// we do not search for extreme in node->right->subtree because we only search points smaller on dim_current_ (and also smaller on dim_other_)
			}
			else {
				node_query(query_point, node->right, temp_optimum, vol1);
				if (node->left->truncated) {
					node_points_scan(query_point, node->left->points.begin(), node->left->points.end(), temp_optimum, vol1);
				}
				else {
					assert(node->left->subtree != nullptr);
					node->left->subtree->tree_query(query_point, temp_optimum, vol1);
				}
			}
		}
	}
};



template<typename InputPoint, class InputContainer> class RangeStructLower1d {
private:
	typedef typename InputPoint::value_type ValueType;
	typedef InputPoint Point;
	typedef utils::PointWithId<Point> PointWithId;
	typedef std::vector<PointWithId> ContainerWithId;

	std::unique_ptr<RangeStructLowerTree<Point>> rtree1_;

public:
	// Records input points, unsorted, in original order.
	ContainerWithId copy_container_with_id_;

	// Builds a 2d range tree ordered by dim_current with a subtree ordered by dim_other.
	// dim == 3 - dim_current - dim_other  
	RangeStructLower1d(typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int dim, int truncsize) {
		assert(truncsize >= 1);  // The absence of truncation should be expressed with truncsize = 1.
		size_t i = 0;
		std::transform(begin, end, std::back_inserter(copy_container_with_id_), [&i] (typename InputContainer::value_type input_point) {return PointWithId(input_point, i++);});
		int dim_current = 0;
		int dim_other = 0;
		switch (dim) {
		case 2 : {
			dim_current = 1;
			dim_other = 0;
			break;
		}
		case 1 : {
			dim_current = 2;
			dim_other = 0;
			break;
		}
		case 0 : {
			dim_current = 2;
			dim_other = 1;
			break;
		}
		}
		this->rtree1_ = std::unique_ptr<RangeStructLowerTree<Point>>(new RangeStructLowerTree<Point>(
				copy_container_with_id_, copy_container_with_id_.begin(), copy_container_with_id_.end(), dim_current, dim_other, false, truncsize));
	}

//		// We might implement some pruning heuristic here:
//		// computing for each point its hypervolume then skip a point if its hypervolume plus the maximal point_hypervolume is less than temp_optimum.second
//		// but this pruning must then also be applied to naive hssp before comparison.
	std::tuple<InputPoint, InputPoint, ValueType> best_pair() {
		std::pair<size_t, ValueType> temp_optimum = {0, std::numeric_limits<ValueType>::min()};
		ValueType prev_opt_value = std::numeric_limits<ValueType>::min();
		Point opt_query_point;
		for (auto query_point : copy_container_with_id_) {
			ValueType vol1 = query_point.point[0] * query_point.point[1] * query_point.point[2];
			rtree1_->tree_query(query_point.point, temp_optimum, vol1);
			if (temp_optimum.second > prev_opt_value) {
				opt_query_point = query_point.point;
				prev_opt_value = temp_optimum.second;
			}
		}

		InputPoint opt_match_point = copy_container_with_id_[temp_optimum.first].point;
		return std::make_tuple(opt_query_point, opt_match_point, temp_optimum.second);
	}
};

template<typename InputPoint, class InputContainer>
std::tuple<InputPoint, InputPoint, typename InputPoint::value_type> range_struct_lower (typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int truncsize) {
	typedef typename InputPoint::value_type T;
	RangeStructLower1d<InputPoint, InputContainer> rtree1a(begin, end, 0, truncsize);
	std::tuple<InputPoint, InputPoint, T> result = rtree1a.best_pair();
	for (int i = 1; i <= 2; ++i) {
		RangeStructLower1d<InputPoint, InputContainer> rtree1b(begin, end, i, truncsize);
		std::tuple<InputPoint, InputPoint, T> res = rtree1b.best_pair();
		if (std::get<2>(res) > std::get<2>(result)) {
			result = res;
		}
	}
	return result;
};

}  // namespace rangetree
#endif /* RANGESTRUCTLOWER_HPP */


