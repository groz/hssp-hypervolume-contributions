/*
 * RangeStruct.cpp
 *
 *  Created on: Aug 17, 2016
 *      Author: smaniu, bgroz
 *
 * assumes number of dimensions less than 32
 * assumes general position: not a pb for construction but pb at query time if query point = split value on dim coordinate.
 * assumes Points::value_type is signed as we multiply by -1 when looking for extreme point on hull in given direction.
 * ``Incremental'' because we add points to the range tree hull iteratively , by increasing order on component global dim_
 *      to replace the d range tree with a d-1 range tree.
 */

#ifndef RANGESTRUCT_INC_HPP
#define RANGESTRUCT_INC_HPP


#include <algorithm>
#include <array>
#include <bitset>
#include <cmath>
#include <functional>
#include <iterator>
#include <iostream>
#include <memory>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "hull/upperhull_updatable.hpp"
#include "hull/upperhull_updatable_double.hpp"
#include "utils/point_representation.hpp"
#include "utils/print_helpers.hpp"


namespace rangetree {


// IDEA: to improve speed, one may consider:
// 1) building lazily the rangeStruct on the fly at query time: not all subtrees/hull needed, allows to work on smaller trees.
//    would require theoretical analysis and check efficiency in ptractice
// 2) [minor] optimizing hull constructions: should be easy for int, long might be harder to do without losing too much accuracy on double.
//    only interesting if the hull prove the bottleneck.

// With orthant[i] = 1 if and only if query point is greater than all points below the node in dimension i, 0 otherwise.
	// std::string space = " ";  // for debug
template<typename Point> class RangeStructIncTree;

template <typename Point> struct RangeStructIncNode {
private:
	typedef typename std::unique_ptr<RangeStructIncNode<Point>> Node_unique_ptr;
	typedef typename Point::value_type ValueType;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;
	typedef typename std::vector<PointWithId>::const_iterator point_iter;
	typedef utils::Hull_Point_With_Slope<ValueType> HullPoint;
	typedef utils::Point2d<ValueType> HullQueryPoint;
	typedef typename hull::UpperHull_Set<ValueType, HullPoint, HullQueryPoint> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;
	typedef typename std::unique_ptr<RangeStructIncTree<Point>> Tree_unique_ptr;
public:
	RangeStructIncNode(): split_value(), subtree(nullptr), left(nullptr), right(nullptr), hull(nullptr), truncated(false), points() {};
	// Each node represents implicitly a set of input points (its descendants in the range tree).
	// * left and right nodes represent respectively the lower and upper half of descendants, split around the median according to coordinate[dim], dim being given by the tree containing the node.
	//   split point belongs to left.
	// * subtree points to the tree  copying the node's subtree, but in dimension dim-1.
	// * hull is only defined when dimension is 1.
	//   For each i< 2^nb_dim, we can view i in binary as a subset of dimensions;
	//     hull[i] then stores the hull obtained when mapping input_points to :
	//     {product of coordinates in dimset, product of all coordinates, index of original point in global root's copy_container}
	ValueType split_value;
	Tree_unique_ptr subtree;
	Node_unique_ptr left;
	Node_unique_ptr right;
	// No particular reason for storing pointers to ConvexHull instead of directly the hull here.
	Hull_unique_ptr hull;
	bool truncated;
	// Iterators to obtain all subtree points. Only defined when node is truncated.
	ContainerWithId points;
	// ContainerWithId pointsdb;  // For debug.
};



template<typename Point> class RangeStructIncTree {
private:
	typedef typename std::unique_ptr<RangeStructIncNode<Point>> Node_unique_ptr;
	typedef typename Point::value_type ValueType;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;
	typedef typename ContainerWithId::const_iterator Points_const_it;
	typedef utils::Hull_Point_With_Slope<ValueType> HullPoint;
	typedef utils::Point2d<ValueType> HullQueryPoint;
	typedef typename hull::UpperHull_Set<ValueType, HullPoint, HullQueryPoint> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;

	Node_unique_ptr root_;
	const ContainerWithId &copy_container_with_id_;
	const int dim_;  // dimension on which we order the current tree.
	const int nb_dim_;  // number of dimension in the structure.
	const int truncsize_;
	const std::bitset<32> orthant_;  // orthant[dim_+1] == 1 if this tree is the subtree of a left child. For j > dim_+1, orthant[j] inherits the value from parent tree. Bits j <= dim_ take default value 0.
public:
	// Complexity: O(n log^(dim+1) n), where n is std::distance(begin, end).
	// Space used: same as complexity.

	RangeStructIncTree(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end, int dim, int nb_dim, int truncsize, std::bitset<32> orthant): \
		copy_container_with_id_(copy_container_with_id), dim_(dim), nb_dim_(nb_dim), truncsize_(truncsize), orthant_(orthant) {
		// Copy the parameters and the iterator.
		ContainerWithId copy_container;
		std::copy(begin, end, std::back_inserter(copy_container));
		// Sort the points by the coord [dim-1].
		std::sort(copy_container.begin(), copy_container.end(), [&](PointWithId a, PointWithId b) {
			return (a.point)[dim] < (b.point)[dim];
		});

		// Building the tree.
		root_ = nullptr;
		// space += "-";
		// std::cout<<space+"building tree"<<std::endl;
		root_ = make_tree_root(copy_container_with_id, copy_container.begin(), copy_container.end());
		// std::cout<<space+"end built tree"<<std::endl;
		// space.pop_back();
	}

	// Complexity: O(log^(dim+1) n + output size).
	// Searches which of the input points best matches the tree_query_point.
	//   temp_optimum records the temporary optimum, and will be updated if a better pair is found through hull queries,
	// We already know from previous comparisons how the query_point compares with the points in the subtree on higher dimensions. This information is recorded in orthant
	void tree_query(PointWithId query_point, std::pair<size_t, ValueType> &temp_optimum, ValueType vol1) {
		// Ensure the points are ordered.
		// std::cout<<space+"querying tree: point: "<<utils::to_string(query_point.point)<<" dim:"<<dim_<<std::endl;
		// space += "-";
		node_query(query_point, root_, temp_optimum, vol1);
		// space.pop_back();
		// std::cout<<space+"end querying tree"<<std::endl;
	}
	
	void tree_hull_insert(PointWithId query_point) {
		// std::cout<<space+"inserting in tree: point: "<<utils::to_string(query_point.point)<<" dim:"<<dim_<<std::endl;
		// space += "-";
		if (root_->left != nullptr && root_->right != nullptr) {
			if (query_point.point[dim_] <= root_->split_value) {
				node_hull_insert(query_point, root_->left, true);
			}
			else {
				node_hull_insert(query_point, root_->right, false);
			}
		}
		else if (root_->left != nullptr) {
			node_hull_insert(query_point, root_->left, true);
		}
		else if (root_->right != nullptr) {
			node_hull_insert(query_point, root_->right, false);			
		}
		// space.pop_back();
		// std::cout<<space+"end inserting tree"<<std::endl;
	}
	
private:
	// Recursively creates the range tree.
	Node_unique_ptr make_tree_root(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end) {
		Node_unique_ptr ptr_node(new RangeStructIncNode<Point>());
		// Creating the new tree node, along with its info.
		// Copy the actual point values into the rangetree::node only in the case of truncated nodes
		// (but of course we also copy values into the rangetree:: copycontainer when the dimension decreases).
		// No subtree not hull in the root.
		// Subtrees left/right split range in half so that largest half is left (an arbitrary choice).
			// std::copy(begin, end, std::back_inserter(ptr_node->pointsdb));  // For debug.
		long length = std::distance(begin, end);
		ptr_node->subtree = nullptr;
		if(length <= truncsize_) {
			// std::cout<<"truncated root"<<" "<<dim_<<" "<<length<<" "<<truncsize_<<std::endl;  // May happen, but only in ''extraordinary'' case where truncsize >= number of input points for the structure.
			ptr_node->split_value = begin->point[dim_];
			ptr_node->left = nullptr;
			ptr_node->right = nullptr;
			ptr_node->points = ContainerWithId();
			std::copy(begin, end, std::back_inserter(ptr_node->points));  // we could strip the id to save space as we don't need it for points (have no hull) but then node_scan will use PointWithoutId in temp_optimum , whereas get_extreme uses Index...
			ptr_node->truncated = true;
		}
		else {
			if(length%2 == 1) {  // Splitting in half for odd splits.
				ptr_node->split_value = (begin+length/2)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2+1, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2+1, end, false);
			}
			else {  // Splitting in half for even splits.
				ptr_node->split_value = (begin+length/2-1)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2, end, false);
			}
		}
		return ptr_node;
	}

	Node_unique_ptr make_tree(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end, bool is_left) {
		Node_unique_ptr ptr_node(new RangeStructIncNode<Point>());
		// Creating the new tree node, along with its info.
		// Copy the actual point values into the rangetree::node only in the case of truncated nodes
		// (but of course we also copy values into the rangetree:: copycontainer when the dimension decreases).
		// Build the convex hull from the points only in the lowest dimension (dim=1).
		// Subtrees left/right split range in half so that largest half is left (an arbitrary choice).
			// std::copy(begin, end, std::back_inserter(ptr_node->pointsdb));  // For debug.
		long length = std::distance(begin, end);
		ptr_node->subtree = nullptr;
		if(length <= truncsize_) {
			ptr_node->split_value = begin->point[dim_];
			ptr_node->left = nullptr;
			ptr_node->right = nullptr;
			ptr_node->points = ContainerWithId();
			std::copy(begin, end, std::back_inserter(ptr_node->points));  // we could strip the id to save space as we don't need it for points (have no hull) but then node_scan will use PointWithoutId in temp_optimum , whereas get_extreme uses Index...
			ptr_node->truncated = true;
		}
		else {
			if(length%2 == 1) {  // Splitting in half for odd splits.
				ptr_node->split_value = (begin+length/2)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2+1, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2+1, end, false);
			}
			else {  // Splitting in half for even splits.
				ptr_node->split_value = (begin+length/2-1)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2, end, false);
			}

			if (dim_ > 0) {  // Create the tree on a lower dimension.
				std::bitset<32> orthant = orthant_;
				assert(orthant[dim_] == 0);
				if (is_left) orthant[dim_] = 1;
				std::unique_ptr<RangeStructIncTree<Point>>\
				ptr_subtree(new RangeStructIncTree<Point>(copy_container_with_id, begin, end, dim_ - 1, nb_dim_, truncsize_, orthant));
				ptr_node->subtree = std::move(ptr_subtree);
			}
			else {  // Initializes hull.
				assert(dim_ == 0);
				Hull_unique_ptr ptr_hull(new Hull_class());
				ptr_node->hull = std::move(ptr_hull);
			}
		}
		return ptr_node;
	}

	HullPoint make_hullpoint(PointWithId input_point, std::bitset<32> dimset) {
		int dim_index = 0;
		return {  // Returns 3D Vector: {product of coordinates in dimset, product of all coordinates, index of original point in global root's copy_container}
				std::accumulate<typename Point::const_iterator, ValueType>(
						input_point.point.begin(),
						input_point.point.begin() + this->nb_dim_,
						1,
						[this, &dimset, &dim_index] (ValueType a, ValueType b) { if (dimset[dim_index++]) {return a*b;} else {return a;} }
				),
				std::accumulate<typename Point::const_iterator, ValueType>(
						input_point.point.begin(),
						input_point.point.begin() + this->nb_dim_,
						1,
						std::multiplies<ValueType>()
				),
				input_point.id,
				std::numeric_limits<double>::max()
		};
	}

	ValueType pair_hypervolume(const Point &p, const Point &q, ValueType vol1) {  // One could precompute the hypervolume of each point in a global array to optimize away the computation of vol2
		// but this would introduce more cache misses so not worth it on small dimensions.
		ValueType common_vol = 1;
		ValueType vol2 = 1;
		for (int i = 0; i < this->nb_dim_; i++) {
			vol2 *= q[i];
			common_vol *= std::min(p[i], q[i]);
		}
		return vol1 + vol2 - common_vol;
	}


	void node_points_scan(const Point &query_point, \
			Points_const_it begin, \
			Points_const_it end, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		for (auto q = begin; q != end; ++q) {
			ValueType pair_vol = pair_hypervolume(query_point, q->point, vol1);
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {q->id, pair_vol};
			}
		}
	}

	// Queries the appropriate convex hull (indicated by orthant) at node to identify the point below the node that maximizes volume together with query_point.
	// temp_optimum is updated if the resulting hypervolume is higher.
	// Assumes orthant[i] = 1 if and only if query point is greater than all points below the node in dimension i, 0 otherwise.
	// Assumes vol1 is the hypervolume below query_point.
	// Complexity: If node->truncated is false, O(log h), otherwise O(h), where h= hull_size.
	void get_extreme(const Point &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1, \
			bool is_left) {
		assert(dim_ == 0);
	// 	if (query_point[0] == 95){
	// 	std::cout<<space+"extrm query"<<std::endl;
	// 	for (auto p: node->pointsdb){
	// 		std::cout<<utils::to_string<Point>(p.point, " ", "{", "}");
	// 		std::cout<<"["<<p.id<<"]";
	// 	}
	// 	std::cout<<"\n***"<<std::endl;
	// }
		if (node->truncated) {
		// 	if (query_point[0] == 95){
		// 	std::cout<<space+"extrm: trunc"<<std::endl;
		// }
			node_points_scan(query_point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else if (node->hull->get_hull_size() > 0) {
			std::bitset<32> orthant = orthant_;
			assert(orthant[dim_] == 0);
			assert(node->hull != nullptr);
			if (is_left) orthant[dim_] = 1;
			int dim_index = 0;
			HullQueryPoint p(
					std::accumulate<typename Point::const_iterator, ValueType>(
							query_point.begin(),
							query_point.begin() + this->nb_dim_,
							-1,
							[this, &orthant, &dim_index] (ValueType a, ValueType b) { if (orthant[dim_index++]) {
								return a;} else {return a*b;} }
					),
					1);
			// std::cout<<"hullsize"<<node->hull.size()<<std::endl;
			size_t extreme_point_id = node->hull->Extreme(p).id;
			ValueType pair_vol = pair_hypervolume(query_point, copy_container_with_id_[extreme_point_id].point, vol1);
			// if (query_point[0] == 95){
			// 	std::cout<<orthant_<<std::endl;
			// 	std::cout<<p.coord0<<" "<<p.coord1<<std::endl;
			// for (auto pp:node->hull->get_hull_points()) {
			// 	std::cout<<pp.coord0<<" "<<pp.coord1<<std::endl;
			// }
			// std::cout<<space+"extrm :"<<extreme_point_id<< " "<< copy_container_with_id_[extreme_point_id].point[0]<<"hullsize:"<<node->hull->get_hull_size()<< std::endl;
			// }
			// we could avoid this call to pair_hypervolume, and remove the copy_container reference within trees:
			// vol2 can is coordinate 1 from  extreme_point, and common_vol can be computed from query_point, orthant, and coordinate 0 of extreme_point.
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {extreme_point_id, pair_vol};
			}
		}
		// else if (query_point[0] == 95){
		// 	std::cout<<space+"extrm: empty hull"<<std::endl;
		// }
		// if (query_point[0] == 95){
		// std::cout<<space+"end extrm query"<<std::endl;
		// }
	}


	void node_hull_insert(const PointWithId &query_point, \
			const Node_unique_ptr &node, \
			bool is_left) {
				if (!node->truncated) {
					if (dim_ == 0) {
						std::bitset<32> orthant = orthant_;
						assert(orthant[dim_] == 0);
						assert(node->hull != nullptr);
						if (is_left) orthant[dim_] = 1;
						// Adding the point to the hull within this node.
						HullPoint new_hull_point = make_hullpoint(query_point, orthant);
						node->hull->add_point(new_hull_point);
					}
					else {
						assert(!node->subtree->root_->truncated);
						node->subtree->tree_hull_insert(query_point);
					}
					if (query_point.point[dim_] <= node->split_value) {
						node_hull_insert(query_point, node->left, true);
					}
					else {
						node_hull_insert(query_point, node->right, false);						
					}
				}
			}
	// Returns the query answers in the subtree with root at "node".
	// orthant[i] = 1 if query point is greater than all points below the node in dimension i, 0 otherwise (0 adopted as default).
	void node_query(const PointWithId &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
	// if (query_point.point[0] == 95){
	// 	std::cout<<space+"node query dim; "<<dim_<<std::endl;
	// 	space += "=";
	// 	for (auto p: node->pointsdb){
	// 		std::cout<<utils::to_string<Point>(p.point, " ", "{", "}");
	// 		std::cout<<"["<<p.id<<"]";
	// 	}
	// 	std::cout<<"\n----"<<std::endl;
	// }
		if (node->truncated) {
			node_points_scan(query_point.point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else if (dim_ == 0) {
			assert(node->left != nullptr);
			assert(node->right != nullptr);
			if (query_point.point[dim_] <= node->split_value) {
			// 	if (query_point.point[0] == 95){
			// 	std::cout<<space+"query left ext right"<<std::endl;
			// }
				node_query(query_point, node->left, temp_optimum, vol1);
				get_extreme(query_point.point, node->right, temp_optimum, vol1, false);
			}
			else {
			// 	if (query_point.point[0] == 95){
			// 	std::cout<<space+"query right ext left"<<std::endl;
			// }
				node_query(query_point, node->right, temp_optimum, vol1);
				get_extreme(query_point.point, node->left, temp_optimum, vol1, true);
			}

		}
		else {  // dim > 0 and not truncated
			assert(node->left != nullptr);
			assert(node->right != nullptr);
			if (query_point.point[dim_] <= node->split_value) {
			// 	if (query_point.point[0] == 95){
			// 	std::cout<<space+"query left, query right subtree"<<std::endl;
			// }
				node_query(query_point, node->left, temp_optimum, vol1);
				if (node->right->truncated) {
					node_points_scan(query_point.point, node->right->points.begin(), node->right->points.end(), temp_optimum, vol1);
				}
				else {
					assert(node->right->subtree != nullptr);
					node->right->subtree->tree_query(query_point, temp_optimum, vol1);
				}
			}
			else {
			// 	if (query_point.point[0] == 95){
			// 	std::cout<<space+"query right, query left subtree"<<std::endl;
			// }
				node_query(query_point, node->right, temp_optimum, vol1);
				if (node->left->truncated) {
					node_points_scan(query_point.point, node->left->points.begin(), node->left->points.end(), temp_optimum, vol1);
				}
				else {
					assert(node->left->subtree != nullptr);
					node->left->subtree->tree_query(query_point, temp_optimum, vol1);
				}
			}
		}
	// if (query_point.point[0] == 95){
	// 	space.pop_back();
	// 	std::cout<<space+"end node query"<<std::endl;
	// }
	}
};



template<typename InputPoint, class InputContainer> class RangeStructIncClass {
private:
	typedef typename InputPoint::value_type ValueType;
	typedef InputPoint Point;
	typedef utils::PointWithId<Point> PointWithId;
	typedef std::vector<PointWithId> ContainerWithId;

	std::unique_ptr<RangeStructIncTree<Point>> rtree_;
	int nb_dim_;
public:
	ContainerWithId copy_container_with_id_;

	RangeStructIncClass(typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int nbdim, int truncsize): nb_dim_(nbdim) {
		assert(nbdim > 1);  // absurd value: no point for the technique when nbdim == 1.
		assert(truncsize >= 1);  // The absence of truncation should be expressed with truncsize = 1.
		assert((*begin)[1] * (*begin)[2] < std::numeric_limits<ValueType>::max()/(*begin)[0]);  // Otherwise risk of overflow.

		std::transform(begin, end, std::back_inserter(copy_container_with_id_), [] (Point input_point) {return PointWithId(input_point, 0);});
		size_t i = 0;
		std::sort(copy_container_with_id_.begin(), copy_container_with_id_.end(), [nbdim](PointWithId a, PointWithId b) {return a.point[nbdim - 1] < b.point[nbdim - 1]; });
		for (auto pt = copy_container_with_id_.begin(); pt != copy_container_with_id_.end(); ++pt) {
			pt->id = i++;
		}
		std::bitset<32> initial_orthant(1<<(nbdim-1));
		this->rtree_ = std::unique_ptr<RangeStructIncTree<Point>>(new RangeStructIncTree<Point>(
				copy_container_with_id_, copy_container_with_id_.begin(), copy_container_with_id_.end(), nbdim - 2, nbdim, truncsize, initial_orthant));
	}

	void rstruct_query(PointWithId query_point, std::pair<size_t, ValueType> &temp_optimum) {
		ValueType vol1 = 1;
		for (int i = 0; i < this->nb_dim_; ++i) {
			vol1 *= query_point.point[i];
		}
		// TODO we might implement some pruning heuristic here:
		// computing for each point its hypervolume then skip a point if its hypervolume plus the maximal point_hypervolume is less than temp_optimum.second
		// but this pruning must then also be applied to naive hssp before comparis
		rtree_->tree_query(query_point, temp_optimum, vol1);
		rtree_->tree_hull_insert(query_point);
	}

	std::tuple<InputPoint, InputPoint, ValueType> best_pair() {
		std::pair<size_t, ValueType> temp_optimum = {0, std::numeric_limits<ValueType>::min()};
		ValueType prev_opt_value = std::numeric_limits<ValueType>::min();
		Point opt_point;
		for (auto p : copy_container_with_id_) {
			// std::cout<<space+"rtreequery with:"<<utils::to_string(p.point)<<std::endl;
			rstruct_query(p, temp_optimum);
			if (temp_optimum.second > prev_opt_value) {
				opt_point = p.point;
				prev_opt_value = temp_optimum.second;
			}
		}
		InputPoint output0 = opt_point;
		Point aux = copy_container_with_id_[temp_optimum.first].point;
		InputPoint output1 = aux;
		return std::make_tuple(output0, output1, temp_optimum.second);
	}
};

template<typename InputPoint, class InputContainer>
std::tuple<InputPoint, InputPoint, typename InputPoint::value_type> range_struct_inc (typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int nbdim, int truncsize) {
	typedef typename InputPoint::value_type T;
	assert(nbdim > 1);
	RangeStructIncClass<InputPoint, InputContainer> rtree(begin, end, nbdim, truncsize);
	std::tuple<InputPoint, InputPoint, T> result = rtree.best_pair();
	return result;
};
}  // namespace rangetree
#endif /* RANGESTRUCT_INC_HPP */


