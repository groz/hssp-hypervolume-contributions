/*
 * RangeStruct.cpp
 *
 *  Created on: Aug 17, 2016
 *      Author: smaniu, bgroz
 *
 * assumes number of dimensions less than 32
 * assumes general position: not a pb for construction but pb at query time if query point = split value on dim coordinate.
 * assumes Points::value_type is signed as we multiply by -1 when looking for extreme point on hull in given direction.
 */

#ifndef RANGESTRUCT_HPP
#define RANGESTRUCT_HPP


#include <algorithm>
#include <array>
#include <bitset>
#include <cmath>
#include <functional>
#include <iterator>
#include <iostream>
#include <memory>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "hull/cvxhull.hpp"
#include "hull/cvxhull_double_without_slopes.hpp"
#include "utils/point_representation.hpp"
#include "utils/print_helpers.hpp"


namespace rangetree {
// With orthant[i] = 1 if and only if query point is greater than all points below the node in dimension i, 0 otherwise.
// Optimizations: we try to store only hull that are relevant:
// 1) we need only compare query_points with points that are larger on global dimension 
//   (we can safely assume this, by symmetry, otherwise we would consider each pair twice): 
//   orthant.to_ulong() restricted to {1, .., 2^{(nbdim)-1}-1}, i.e., orthant[nbdim-1] == 0. This saves a factor 2. 
//   This is the reason why we distinguish node_query_nb_dim
// 2) no hulls nor subtrees at the root of trees.
template<typename Point> class RangeStructTree;

template <typename Point> struct RangeStructNode {
private:
	typedef typename Point::value_type ValueType;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;;
	typedef utils::Point2d<ValueType> QueryPoint;
	typedef utils::PointWithId2d<ValueType> HullPoint;
	typedef typename hull::ConvexHull<ValueType, HullPoint, QueryPoint, std::vector<HullPoint>> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;
public:
	RangeStructNode(): split_value(), subtree(nullptr), left(nullptr), right(nullptr), hull(nullptr), truncated(false), points() {};
	// Each node represents implicitly a set of input points (its descendants in the range tree).
	// * left and right nodes represent respectively the lower and upper half of descendants, split around the median according to coordinate[dim], dim being given by the tree containing the node.
	//   split point belongs to left.
	// * subtree points to the tree  copying the node's subtree, but in dimension dim-1.
	// * hull is only defined when dimension is 1.
	//   For each i< 2^nb_dim, we can view i in binary as a subset of dimensions;
	//     hull[i] then stores the hull obtained when mapping input_points to :
	//     {product of coordinates in dimset, product of all coordinates, index of original point in global root's copy_container}
	ValueType split_value;
	std::unique_ptr<RangeStructTree<Point>> subtree;
	std::unique_ptr<RangeStructNode<Point>> left;
	std::unique_ptr<RangeStructNode<Point>> right;
	// No particular reason for storing pointers to ConvexHull instead of directly the hull here.
	Hull_unique_ptr hull;
	bool truncated;
	// Iterators to obtain all subtree points. Only defined when node is truncated.
	ContainerWithId points;
};



template<typename Point> class RangeStructTree {
private:
	typedef typename std::unique_ptr<RangeStructNode<Point>> Node_unique_ptr;
	typedef typename Point::value_type ValueType;
	typedef utils::PointWithId<Point> PointWithId;
	typedef typename std::vector<PointWithId> ContainerWithId;
	typedef typename ContainerWithId::const_iterator Points_const_it;
	typedef utils::Point2d<ValueType> QueryPoint;
	typedef utils::PointWithId2d<ValueType> HullPoint;
	typedef typename hull::ConvexHull<ValueType, HullPoint, QueryPoint, std::vector<HullPoint>> Hull_class;
	typedef typename std::unique_ptr<Hull_class> Hull_unique_ptr;

	Node_unique_ptr root_;  // The root node has no hull nor subtree.
	const ContainerWithId &copy_container_with_id_;
	const int dim_;  // dimension on which we order the current tree.
	const int nb_dim_;  // number of dimension in the structure.
	const int truncsize_;
	const std::bitset<32> orthant_;  // orthant[dim_+1] == 1 if this tree is the subtree of a left child. For j > dim_+1, orthant[j] inherits the value from parent tree. Bits j <= dim_ take default value 0.
public:
	// Complexity: O(n log^(dim+1) n), where n is std::distance(begin, end).
	// Space used: same as complexity.

	RangeStructTree(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end, int dim, int nb_dim, int truncsize, std::bitset<32> orthant): \
		copy_container_with_id_(copy_container_with_id), dim_(dim), nb_dim_(nb_dim), truncsize_(truncsize), orthant_(orthant) {
		// Copy the parameters and the iterator.
		ContainerWithId copy_container;
		std::copy(begin, end, std::back_inserter(copy_container));
		// Sort the points by the coord [dim].
		std::sort(copy_container.begin(), copy_container.end(), [&](PointWithId a, PointWithId b) {
			return (a.point)[dim] < (b.point)[dim];
		});

		// Building the tree.
		root_ = nullptr;
		root_ = make_tree_root(copy_container_with_id, copy_container.begin(), copy_container.end());
	}

	// Complexity: O(log^(dim+1) n + output size).
	// Searches which of the input points best matches the tree_query_point.
	//   temp_optimum records the temporary optimum, and will be updated if a better pair is found through hull queries,
	// We already know from previous comparisons how the query_point compares with the points in the subtree on higher dimensions. This information is recorded in orthant
	void tree_query(Point query_point, std::pair<size_t, ValueType> &temp_optimum, ValueType vol1) {
		// Ensure the points are ordered.
		if (dim_ == nb_dim_ - 1) {
			node_query_nb_dim(query_point, root_, temp_optimum, vol1); // optimized version for the case nb_dim
		}
		else {
			node_query(query_point, root_, temp_optimum, vol1);
		}
	}

private:
	// Recursively creates the range tree.
	Node_unique_ptr make_tree_root(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end) {
		Node_unique_ptr ptr_node(new RangeStructNode<Point>());
		// Creating the new tree node, along with its info.
		// Copy the actual point values into the rangetree::node only in the case of truncated nodes
		// (but of course we also copy values into the rangetree:: copycontainer when the dimension decreases).
		// No subtree not hull in the root.
		// Subtrees left/right split range in half so that largest half is left (an arbitrary choice).

		long length = std::distance(begin, end);
		ptr_node->subtree = nullptr;
		if(length <= truncsize_) {
			ptr_node->split_value = begin->point[dim_];
			ptr_node->left = nullptr;
			ptr_node->right = nullptr;
			ptr_node->points = ContainerWithId();
			std::copy(begin, end, std::back_inserter(ptr_node->points));  // we could strip the id to save space as we don't need it for points (have no hull) but then node_scan will use PointWithoutId in temp_optimum , whereas get_extreme uses Index...
			ptr_node->truncated = true;
		}
		else {
			if(length%2 == 1) {  // Splitting in half for odd splits.
				ptr_node->split_value = (begin+length/2)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2+1, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2+1, end, false);
			}
			else {  // Splitting in half for even splits.
				ptr_node->split_value = (begin+length/2-1)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2, end, false);
			}
		}
		return ptr_node;
	}
	
	Node_unique_ptr make_tree(const ContainerWithId &copy_container_with_id, Points_const_it begin, Points_const_it end, bool is_left) {
		Node_unique_ptr ptr_node(new RangeStructNode<Point>());
		// Creating the new tree node, along with its info.
		// Copy the actual point values into the rangetree::node only in the case of truncated nodes
		// (but of course we also copy values into the rangetree:: copycontainer when the dimension decreases).
		// Build the convex hull from the points only in the lowest dimension (dim==0).
		// Subtrees left/right split range in half so that largest half is left (an arbitrary choice).

		long length = std::distance(begin, end);
		ptr_node->subtree = nullptr;
		if(length <= truncsize_) {
			ptr_node->split_value = begin->point[dim_];
			ptr_node->left = nullptr;
			ptr_node->right = nullptr;
			ptr_node->points = ContainerWithId();
			std::copy(begin, end, std::back_inserter(ptr_node->points));  // we could strip the id to save space as we don't need it for points (have no hull) but then node_scan will use PointWithoutId in temp_optimum , whereas get_extreme uses Index...
			ptr_node->truncated = true;
		}
		else {
			if(length%2 == 1) {  // Splitting in half for odd splits.
				ptr_node->split_value = (begin+length/2)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2+1, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2+1, end, false);
			}
			else {  // Splitting in half for even splits.
				ptr_node->split_value = (begin+length/2-1)->point[dim_];
				ptr_node->left = make_tree(copy_container_with_id, begin, begin+length/2, true);
				ptr_node->right = make_tree(copy_container_with_id, begin+length/2, end, false);
			}

			if (dim_ > 0) {  // Create the tree on a lower dimension, except on left children when dim == nb_dim_
				if (dim_ < nb_dim_ - 1 || !is_left) {
					std::bitset<32> orthant = orthant_;
					assert(orthant[dim_] == 0);
					if (is_left) orthant[dim_] = 1;
					std::unique_ptr<RangeStructTree<Point>>\
					ptr_subtree(new RangeStructTree<Point>(copy_container_with_id, begin, end, dim_ - 1, nb_dim_, truncsize_, orthant));
					ptr_node->subtree = std::move(ptr_subtree);
				}
			}
			else {  // Builds the hull.
				assert(dim_ == 0);
				std::bitset<32> orthant = orthant_;
				assert(orthant[dim_] == 0);
				if (is_left) orthant[dim_] = 1;
				std::vector<HullPoint> mapped_points = {};
				mapped_points.reserve(length);
				std::transform(begin, end, std::back_inserter(mapped_points),
						[ this, orthant ] (PointWithId input_point) {return make_hullpoint(input_point, orthant);});  // Need to capture this (it is captured by ref) for method make_hullpoint
				Hull_unique_ptr ptr_hull(new Hull_class(mapped_points));
				ptr_node->hull = std::move(ptr_hull);
			}
		}
		return ptr_node;
	}

	HullPoint make_hullpoint(PointWithId input_point, std::bitset<32> dimset) {
		int dim_index = 0;
		return HullPoint(  // Returns 3D Vector: {product of coordinates in dimset, product of all coordinates, index of original point in global root's copy_container}
				std::accumulate<typename Point::const_iterator, ValueType>(
						input_point.point.begin(),
						input_point.point.begin() + this->nb_dim_,
						1,
						[this, &dimset, &dim_index] (ValueType a, ValueType b) { if (dimset[dim_index++]) {return a*b;} else {return a;} }
				),
				std::accumulate<typename Point::const_iterator, ValueType>(
						input_point.point.begin(),
						input_point.point.begin() + this->nb_dim_,
						1,
						std::multiplies<ValueType>()
				),
				input_point.id
		);
	}
	
	ValueType pair_hypervolume(const Point &p, const Point &q, ValueType vol1) {  // One could precompute the hypervolume of each point in a global array to optimize away the computation of vol2
		// but this would introduce more cache misses so not worth it on small dimensions.
		ValueType common_vol = 1;
		ValueType vol2 = 1;
		for (int i = 0; i < this->nb_dim_; i++) {
			vol2 *= q[i];
			common_vol *= std::min(p[i], q[i]);
		}
		return vol1 + vol2 - common_vol;
	}


	void node_points_scan(const Point &query_point, \
			Points_const_it begin, \
			Points_const_it end, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		for (auto q = begin; q != end; ++q) {
			ValueType pair_vol = pair_hypervolume(query_point, q->point, vol1);
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {q->id, pair_vol};
			}
		}
	}

	// Queries the appropriate convex hull (indicated by orthant) at node to identify the point below the node that maximizes volume together with query_point.
	// temp_optimum is updated if the resulting hypervolume is higher.
	// Assumes orthant[i] = 1 if and only if query point is greater than all points below the node in dimension i, 0 otherwise.
	// Assumes vol1 is the hypervolume below query_point.
	// Complexity: If node->truncated is false, O(log h), otherwise O(h), where h= hull_size.
	void get_extreme(const Point &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1, \
			bool is_left) {
		assert(dim_ == 0);
		if (node->truncated) {
			node_points_scan(query_point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else {
			std::bitset<32> orthant = orthant_;
			assert(orthant[dim_] == 0);
			assert(node->hull != nullptr);
			if (is_left) orthant[dim_] = 1;
			int dim_index = 0;
			QueryPoint p(
					std::accumulate<typename Point::const_iterator, ValueType>(
							query_point.begin(),
							query_point.begin() + this->nb_dim_,
							-1,
							[this, &orthant, &dim_index] (ValueType a, ValueType b) { if (orthant[dim_index++]) {
								return a;} else {return a*b;} }
					),
					1);

			size_t extreme_point_id = (node->hull->Extreme(p)).id;
			ValueType pair_vol = pair_hypervolume(query_point, copy_container_with_id_[extreme_point_id].point, vol1);
			// we could avoid this call to pair_hypervolume, and remove the copy_container reference within trees:
			// vol2 can is coordinate 1 from  extreme_point, and common_vol can be computed from query_point, orthant, and coordinate 0 of extreme_point.
			if (pair_vol > temp_optimum.second) {
				temp_optimum = {extreme_point_id, pair_vol};
			}
		}
	}




	// Returns the query answers in the subtree with root at "node".
	// orthant[i] = 1 if query point is greater than all points below the node in dimension i, 0 otherwise (0 adopted as default).
	void node_query(const Point &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		if (node->truncated) {
			node_points_scan(query_point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else if (dim_ == 0) {
			assert(node->left != nullptr);
			assert(node->right != nullptr);
			if (query_point[dim_] <= node->split_value) {
				node_query(query_point, node->left, temp_optimum, vol1);
				get_extreme(query_point, node->right, temp_optimum, vol1, false);
			}
			else {
				node_query(query_point, node->right, temp_optimum, vol1);
				get_extreme(query_point, node->left, temp_optimum, vol1, true);
			}
		}
		else {
			assert(node->left != nullptr);
			assert(node->right != nullptr);
			if (query_point[dim_] <= node->split_value) {
				node_query(query_point, node->left, temp_optimum, vol1);
				if (node->right->truncated) {
					node_points_scan(query_point, node->right->points.begin(), node->right->points.end(), temp_optimum, vol1);
				}
				else {
					assert(node->right->subtree != nullptr);
					node->right->subtree->tree_query(query_point, temp_optimum, vol1);
				}
			}
			else {
				node_query(query_point, node->right, temp_optimum, vol1);
				if (node->left->truncated) {
					node_points_scan(query_point, node->left->points.begin(), node->left->points.end(), temp_optimum, vol1);
				}
				else {
					assert(node->left->subtree != nullptr);
					node->left->subtree->tree_query(query_point, temp_optimum, vol1);
				}
			}
		}
	}


	// Returns the query answers in the subtree with root at "node".
	// orthant[i] = 1 if query point is greater than all points below the node in dimension i, 0 otherwise (0 adopted as default).
	void node_query_nb_dim(const Point &query_point, \
			const Node_unique_ptr &node, \
			std::pair<size_t, ValueType> &temp_optimum, \
			ValueType vol1) {
		assert(dim_ == nb_dim_ - 1);  // a fortiori dim > 1
		if (node->truncated) {
			node_points_scan(query_point, node->points.begin(), node->points.end(), temp_optimum, vol1);
		}
		else {
			assert(node->left != nullptr);
			assert(node->right != nullptr);
			if (query_point[dim_] <= node->split_value) {
				node_query_nb_dim(query_point, node->left, temp_optimum, vol1);
				if (node->right->truncated) {
					node_points_scan(query_point, node->right->points.begin(), node->right->points.end(), temp_optimum, vol1);
				}
				else {
					assert(node->right->subtree != nullptr);
					node->right->subtree->tree_query(query_point, temp_optimum, vol1);
				}
			}
			else {
				node_query_nb_dim(query_point, node->right, temp_optimum, vol1);
				// => skipped search on left->subtree because points are not greater on global_dim
			}
		}
	}
};



template<typename InputPoint, class InputContainer> class RangeStructClass {
private:
	typedef typename InputPoint::value_type ValueType;
	typedef InputPoint Point;
	typedef utils::PointWithId<Point> PointWithId;
	typedef std::vector<PointWithId> ContainerWithId;

	std::unique_ptr<RangeStructTree<Point>> rtree_;
	int nb_dim_;
public:
	ContainerWithId copy_container_with_id;

	RangeStructClass(typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int nbdim, int truncsize): nb_dim_(nbdim) {
		assert(nbdim > 0);  // absurd value
		assert(truncsize >= 1);  // The absence of truncation should be expressed with truncsize = 1.
		assert((*begin)[1] * (*begin)[2] < std::numeric_limits<ValueType>::max()/(*begin)[0]);  // Otherwise risk of overflow.

		size_t i = 0;
		std::transform(begin, end, std::back_inserter(copy_container_with_id), [&i] (typename InputContainer::value_type input_point) {return PointWithId(input_point, i++);});
		this->rtree_ = std::unique_ptr<RangeStructTree<Point>>(new RangeStructTree<Point>(
				copy_container_with_id, copy_container_with_id.begin(), copy_container_with_id.end(), nbdim - 1, nbdim, truncsize, std::bitset<32>(0)));
	}

	void rstruct_query(Point query_point, std::pair<size_t, ValueType> &temp_optimum) {
		ValueType vol1 = 1;
		for (int i = 0; i < this->nb_dim_; ++i) {
			vol1 *= query_point[i];
		}
		// TODO we might implement some pruning heuristic here:
		// computing for each point its hypervolume then skip a point if its hypervolume plus the maximal point_hypervolume is less than temp_optimum.second
		// but this pruning must then also be applied to naive hssp before comparison.
		rtree_->tree_query(query_point, temp_optimum, vol1);
	}

	std::tuple<InputPoint, InputPoint, ValueType> best_pair() {
		std::pair<size_t, ValueType> temp_optimum = {0, std::numeric_limits<ValueType>::min()};
		ValueType prev_opt_value = std::numeric_limits<ValueType>::min();
		Point opt_point;
		for (auto p : copy_container_with_id) {
			rstruct_query(p.point, temp_optimum);
			if (temp_optimum.second > prev_opt_value) {
				opt_point = p.point;
				prev_opt_value = temp_optimum.second;
			}
		}
		InputPoint output0 = opt_point;
		Point aux = copy_container_with_id[temp_optimum.first].point;
		InputPoint output1 = aux;
		return std::make_tuple(output0, output1, temp_optimum.second);
	}
};

template<typename InputPoint, class InputContainer>
std::tuple<InputPoint, InputPoint, typename InputPoint::value_type> range_struct (typename InputContainer::const_iterator begin, typename InputContainer::const_iterator end, int nbdim, int truncsize) {
	typedef typename InputPoint::value_type T;
	assert(nbdim > 2); // the structure is useless for lower number of dimensions.
	RangeStructClass<InputPoint, InputContainer> rtree(begin, end, nbdim, truncsize);
	std::tuple<InputPoint, InputPoint, T> result = rtree.best_pair();
	return result;
};
}  // namespace rangetree
#endif /* RANGESTRUCT_HPP */


