/*
 * cvxhull.hpp
 *
 *  Created on: Aug 17, 2016
 *      Author: bgroz, smaniu
 */


#ifndef CVXHULLDOUBLE_H
#define CVXHULLDOUBLE_H

#include <algorithm>
#include <string>
#include <vector>


#include "hull/cvxhull.hpp"
#include "utils/floating_point_arithmetics.hpp"

namespace hull {
// Computes the 2D convex hull of input points, using coordinates coord. More exactly, only the upper left part of the hull.
// Assumes that HullPoint and QueryPoint have public members .coord0 and .coord1 returning the x/y coordinates. And have default constructors.
// Also allows to query extreme point in the direction of any given 2D vector.
// Implements Andrew's monotone chain convex hull algorithm.
// See wikipedia's code: we inverted the crossproduct so that upper hull is computed first.
// Complexity: O(n log n) for construction, O(log n) for extreme point query.
// Constructor assumes number of input points > 0.
template<typename HullPoint, typename QueryPoint, class InputContainer> class ConvexHull<double, HullPoint, QueryPoint, InputContainer>{
public:
	ConvexHull(): convhull_(), upper_hull_size_(0) { std::cerr<<"PB:default constructor for cvxhull(double, w/o slopes). Should not have been used"<<std::endl;};
	ConvexHull(InputContainer points);
	size_t get_hull_size() const;  // For tests and debugging.
	std::vector<HullPoint> get_hull_points() const;  // For tests and debugging.
	HullPoint Extreme(const QueryPoint &point) const;  // Assumes container is not empty, otherwise extreme will have undefined behavious (accessing index -1).

private:
	std::vector<HullPoint> convhull_;  // points of upper hull first (sorted by increasing first coordinate) then lower hull (decreasing order)
	size_t upper_hull_size_;
	bool Cross(const HullPoint &o, const HullPoint &x, const HullPoint &y) const; // true iff clockwise turn (from ox to oy) ie 0<=(ox,oy)<pi, under the assumption that o,x,y have increasing x-coordinates
	double Dotp_compare(const HullPoint &x, const HullPoint &y, const QueryPoint &d) const; // compares dot products <x,d> and <y,d>
	HullPoint Findextreme(size_t low, size_t high, const QueryPoint &direction) const;
};

// --------------------------------------------
// Implementation of the methods:

template<typename HullPoint, typename QueryPoint, class InputContainer>
ConvexHull<double, HullPoint, QueryPoint, InputContainer>::ConvexHull(InputContainer points) {
	size_t k = 0;
	size_t n = std::distance(points.begin(), points.end());
	convhull_.resize(n);  // Allocate at most the number of points;

	// Sort the points
	std::sort(points.begin(), points.end(), [](HullPoint a, HullPoint b){ return a.coord0 < b.coord0 || (a.coord0 == b.coord0 && a.coord1 < b.coord1); });

	// Compute the upper hull
	convhull_[k++] = points[0];
	for(size_t i = 1; i < n; ++i) {
		if (convhull_[k-1].coord1 < points[i].coord1) {
			while ((k >= 2) && Cross(convhull_[k-2], convhull_[k-1], points[i])) {
				k--;
			}
			convhull_[k++] = points[i];
		}
	}
	this->upper_hull_size_ = k;

	//	// Compute the lower hull
	//	for(size_t i = n-2, t = k+1; i >= 0; i--){
	//		while (k >= t && cross(convhull[k-2], convhull[k-1], points[i]) <= 0)  // When multiple points are aligned, the middle ones are not included in the hull as we use <= and not <.
	//			k--;
	//		convhull[k++] = points[i];
	//	}

	//resize the output
	convhull_.resize(k);
}


template<typename HullPoint, typename QueryPoint, class InputContainer>
std::vector<HullPoint> ConvexHull<double, HullPoint, QueryPoint, InputContainer>::get_hull_points() const { return convhull_; }

template<typename HullPoint, typename QueryPoint, class InputContainer>
size_t ConvexHull<double, HullPoint, QueryPoint, InputContainer>::get_hull_size() const { return convhull_.size(); }

template<typename HullPoint, typename QueryPoint, class InputContainer>
HullPoint ConvexHull<double, HullPoint, QueryPoint, InputContainer>::Extreme(const QueryPoint &point) const{
	return Findextreme(0, upper_hull_size_ - 1, point); }


template<typename HullPoint, typename QueryPoint, class InputContainer>
bool ConvexHull<double, HullPoint, QueryPoint, InputContainer>::Cross(const HullPoint &o, const HullPoint &x, const HullPoint &y) const{
	//	assert(o.coord0 < x.coord0 < y.coord0);
	//	return (x.coord0-o.coord0) * (y.coord1-o.coord1) >= (x.coord1-o.coord1) * (y.coord0-o.coord0);
	//	Pb with above: risks of overflow has it is basically elevates input range to the square.
	//  Better solution:
	return (((y.coord1-o.coord1) / (y.coord0-o.coord0)) >= ((x.coord1-o.coord1) / (x.coord0-o.coord0) - PREPROCESSOR_EPSILON));
}


template<typename HullPoint, typename QueryPoint, class InputContainer>
double ConvexHull<double, HullPoint, QueryPoint, InputContainer>::Dotp_compare(const HullPoint &x, const HullPoint &y, const QueryPoint &d) const {
	return (x.coord0-y.coord0) * d.coord0 + (x.coord1-y.coord1) * d.coord1;
}

template<typename HullPoint, typename QueryPoint, class InputContainer>
HullPoint ConvexHull<double, HullPoint, QueryPoint, InputContainer>::Findextreme(size_t low, size_t high, const QueryPoint &direction) const {
	size_t mid = low + (high - low)/2;
	if (high - low < 2) {
		if (high == low) {
			return convhull_[low];
		}
		else {  // high == low+1
			if (Dotp_compare(convhull_[low], convhull_[high], direction) > 0) {
				return convhull_[low];
			}
			else {
				return convhull_[high];
			}
		}
	}
	else { // (high-low > 2) so mid >low and mid<high
		if (Dotp_compare(convhull_[mid-1], convhull_[mid], direction) < 0
				&& Dotp_compare(convhull_[mid+1], convhull_[mid], direction) < 0) {
			return convhull_[mid];
		}
		else if (Dotp_compare(convhull_[mid-1], convhull_[mid], direction) < 0
				&& Dotp_compare(convhull_[mid+1], convhull_[mid], direction) == 0) {
			return Findextreme(mid+1, high, direction);
		}
		else if (Dotp_compare(convhull_[mid+1], convhull_[mid], direction) < 0
				&& Dotp_compare(convhull_[mid-1], convhull_[mid], direction) == 0) {
			return Findextreme(low, mid-1, direction);
		}
		else if (Dotp_compare(convhull_[mid-1], convhull_[mid], direction) < 0) { // && necessarily dotp_compare(convhull[mid+1], convhull[mid], direction) > 0.
			return Findextreme(mid+1, high, direction);
		}
		else  { // &&  In our setting the dotproduct on upper hull of a (negative, positive) increases until maxima then decreases.
			// Therefore necessarily dotp_compare(convhull[mid+1], convhull[mid], direction) < 0
			// and consequently also dotp_compare(convhull[mid-1], convhull[mid], direction) > 0.
			return Findextreme(low, mid-1, direction);
		}
	}
}

}  // namespace hull
#endif /* CVXHULLDOUBLE_H */
