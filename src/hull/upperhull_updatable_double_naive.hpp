/*
 * UpperHull_Updatable.hpp
 *
 *  Created on: Aug 17, 2016
 *      Author: bgroz
 */


#ifndef UPPERHULL_UPDATABLE_DOUBLE2_H
#define UPPERHULL_UPDATABLE_DOUBLE2_H

#include <algorithm>
#include <cmath>
#include <iostream>
#include <set>
#include <string>
#include <type_traits>
#include <vector>


#include "hull/cvxhull_double_without_slopes.hpp"

#include "utils/floating_point_arithmetics.hpp"

namespace hull {

// THIS FILE IS EXACTLY THE SAME AS UPPERHULL_UPDATABLE_H EXCEPT THAT COMPARISONS ARE RELAXED FOR FLOATING POINT ARITHMETIC.
// Computes the 2D convex hull (actually only the upper_left hull quadrant of the convex hull).
// Assumes that HullPoint and QueryPoint have public members .coord0 and .coord0 returning the x/y coordinates, and .left_slope returning the slope to the left of the point in the hull (+infty if none). (And have default constructors?)
// Assumes that HullPoint and QueryPoint have negative coord0, positive coord0.
// Supports insertion of a new point on the hull.
// Also allows to query extreme point in the direction of any given 2D vector (performed as a search on slope).
// Complexity: O(log n) for adding point and for extreme point query.
// Constructor assumes number of input points > 0.
// Does not assume general position: points may have equal first coordinates or equal second coordinates.

// NB: the slopes within the points are not maintained here because the ConvexHull stores the slopes apart.
// This version is intended for tests; ex: testing the correctness of upperhull_updatable.

// Here InputContainer must be a container of HullPointWithSlope. The slope may be arbitrary.
template<typename ValueType, typename HullPointWithSlope, typename QueryPoint> class UpperHull_Set2{
public:
	UpperHull_Set2() { convhull_ = {}; };
	
	// This constructor is presumably faster in practice than creating an empty hull and adding points one at a time
	//   because we first sort points, which facilitates the insertions: insertion costs sum up to O(h) after the sorting step.
	// Complexity: O(h log h) where h=points.size().
	// UpperHull_Set2(InputContainer &points);
	
	size_t get_hull_size() const;

	// Inserts the point at the proper position in hull.
	// The point left_slope should be std::numeric_limits<double>::max().
	// Complexity: O(log h) where h is the Hull size.
	void add_point(HullPointWithSlope point);

	// Assumes Hull is not empty, otherwise extreme will have undefined behaviour (accessing index -1).
	// Does not use point.id nor point.left_slope.
	// Complexity: O(log h) where h is the Hull size (exploits the trick of searching set on order correlated with key).
	HullPointWithSlope Extreme(const QueryPoint &point) const;


	std::vector<HullPointWithSlope> get_hull_points() const;  // For tests and debugging.
//	std::string get_hstring() const;

private:
	hull::ConvexHull<double, HullPointWithSlope, QueryPoint, std::vector<HullPointWithSlope>> convhull_;  // points of upper left hull quadrant
	bool Cross(const HullPointWithSlope &o, const HullPointWithSlope &x, const HullPointWithSlope &y) const; // true iff clockwise turn (from ox to oy) ie 0<=(ox,oy)<pi, under the assumption that o,x,y have increasing x-coordinates
};



// --------------------------------------------
// Implementation of the methods:


template<typename ValueType, typename HullPointWithSlope, typename QueryPoint>
size_t UpperHull_Set2<ValueType, HullPointWithSlope, QueryPoint>::get_hull_size() const {
	return convhull_.get_hull_size();
}

template<typename ValueType, typename HullPointWithSlope, typename QueryPoint>
std::vector<HullPointWithSlope> UpperHull_Set2<ValueType, HullPointWithSlope, QueryPoint>::get_hull_points() const {
	return convhull_.get_hull_points();
}

template<typename ValueType, typename HullPointWithSlope, typename QueryPoint>
void UpperHull_Set2<ValueType, HullPointWithSlope, QueryPoint>::add_point(HullPointWithSlope point) {
	std::vector<HullPointWithSlope> v = convhull_.get_hull_points();
	v.push_back(point);
	convhull_ = hull::ConvexHull<double, HullPointWithSlope, QueryPoint, std::vector<HullPointWithSlope>>(v);
}

//
//

template<typename ValueType, typename HullPointWithSlope, typename QueryPoint>
HullPointWithSlope UpperHull_Set2<ValueType, HullPointWithSlope, QueryPoint>::Extreme(const QueryPoint &point) const {
	return convhull_.Extreme(point);
}

}  // namespace hull
#endif /* UPPERHULL_UPDATABLE_DOUBLE2_H */
