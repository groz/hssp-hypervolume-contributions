/*
 * cvxhull.hpp
 *
 *  Created on: Aug 17, 2016
 *      Author: bgroz, smaniu
 */


#ifndef CVXHULLDOUBLE_H
#define CVXHULLDOUBLE_H

#include <algorithm>
#include <string>
#include <vector>


#include "hull/cvxhull.hpp"
#include "utils/floating_point_arithmetics.hpp"

namespace hull {
// Computes the 2D convex hull of input points, using coordinates coord. More exactly, only the upper left part of the hull.
// Assumes that HullPoint and QueryPoint have public members .coord0 and .coord1 returning the x/y coordinates. And have default constructors.
// Assumes that QueryPoint has negative coord0, positive coord1.
// Also allows to query extreme point in the direction of any given 2D vector.
// Implements Andrew's monotone chain convex hull algorithm.
// See wikipedia's code: we inverted the crossproduct so that upper hull is computed first.
// Complexity: O(n log n) for construction, O(log n) for extreme point query.
// Constructor assumes number of input points > 0.
	
// This version uses internally a vector with the slope: performance in running time is mostly the same (a bit faster at query time, a little bit slower at construction) but it simplifies the code.
template<typename HullPoint, typename QueryPoint, class InputContainer> class ConvexHull<double, HullPoint, QueryPoint, InputContainer>{
public:
	ConvexHull(): convhull_(), slopes_(), upper_hull_size_(0) { std::cerr<<"PB:default constructor for cvxhull(double). Should not have been used, except in tests."<<std::endl;};
	ConvexHull(InputContainer points);
	size_t get_hull_size() const;  // For tests and debugging.
	std::vector<HullPoint> get_hull_points() const;  // For tests and debugging.
	HullPoint Extreme(const QueryPoint &point) const;  // Assumes container is not empty, otherwise extreme will have undefined behavious (accessing index -1).

private:
	std::vector<HullPoint> convhull_;  // points of upper left hull only
	std::vector<double> slopes_;  // left slope of points on upper left hull (decreasing order)
	size_t upper_hull_size_;
	bool Cross(const HullPoint &o, const HullPoint &x, const HullPoint &y) const; // true iff clockwise turn (from ox to oy) ie 0<=(ox,oy)<pi, under the assumption that o,x,y have increasing x-coordinates
};

// --------------------------------------------
// Implementation of the methods:

template<typename HullPoint, typename QueryPoint, class InputContainer>
ConvexHull<double, HullPoint, QueryPoint, InputContainer>::ConvexHull(InputContainer points) {
	size_t k = 0;
	size_t n = std::distance(points.begin(), points.end());
	convhull_.resize(n);  // Allocate at most the number of points;

	if (n > 0) {
		// Sort the points
		std::sort(points.begin(), points.end(), [](HullPoint a, HullPoint b){ return a.coord0 < b.coord0 || (a.coord0 == b.coord0 && a.coord1 < b.coord1); });

		// Compute the upper hull
		convhull_[k++] = points[0];
		for(size_t i = 1; i < n; ++i) {
			if (convhull_[k-1].coord1 < points[i].coord1) {
				while ((k >= 2) && Cross(convhull_[k-2], convhull_[k-1], points[i])) {
					k--;
				}
				convhull_[k++] = points[i];
			}
		}
		this->upper_hull_size_ = k;

		// Resize the output
		convhull_.resize(k);
		slopes_.resize(k);
		if (k > 0) {
			slopes_[0] = std::numeric_limits<double>::max();
			for (size_t i = 1; i < k; ++i) {
				slopes_[i] = (convhull_[i].coord1 - convhull_[i-1].coord1)/(convhull_[i].coord0 - convhull_[i-1].coord0);
				assert(slopes_[i]>=0);
			}
		}
	}
}


template<typename HullPoint, typename QueryPoint, class InputContainer>
std::vector<HullPoint> ConvexHull<double, HullPoint, QueryPoint, InputContainer>::get_hull_points() const { return convhull_; }

template<typename HullPoint, typename QueryPoint, class InputContainer>
size_t ConvexHull<double, HullPoint, QueryPoint, InputContainer>::get_hull_size() const { return convhull_.size(); }


template<typename HullPoint, typename QueryPoint, class InputContainer>
HullPoint ConvexHull<double, HullPoint, QueryPoint, InputContainer>::Extreme(const QueryPoint &point) const {
	assert(-point.coord0 > 0);
	auto it = std::lower_bound(slopes_.begin(), slopes_.end(), -point.coord0/point.coord1, std::greater<double>());
	return *(std::next(convhull_.begin(), std::distance(slopes_.begin(), it) - 1));
}


template<typename HullPoint, typename QueryPoint, class InputContainer>
bool ConvexHull<double, HullPoint, QueryPoint, InputContainer>::Cross(const HullPoint &o, const HullPoint &x, const HullPoint &y) const {
	//	assert(o.coord0 < x.coord0 < y.coord0);
	//	return (x.coord0-o.coord0) * (y.coord1-o.coord1) >= (x.coord1-o.coord1) * (y.coord0-o.coord0);
	//	Pb with above: risks of overflow has it is basically elevates input range to the square.
	//  Better solution:
	return (((y.coord1-o.coord1) / (y.coord0-o.coord0)) >= ((x.coord1-o.coord1) / (x.coord0-o.coord0) - PREPROCESSOR_EPSILON));
}




}  // namespace hull
#endif /* CVXHULLDOUBLE_H */
