/*
 * upperhull_incremental.hpp
 *
 *  Created on: Aug 17, 2016
 *      Author: bgroz
 */



#ifndef UPPERHULL_H
#define UPPERHULL_H

#include <algorithm>
#include <tuple>
#include <vector>

//#define NDEBUG
#include <cassert>


namespace hull {
	
// Computes the 2D convex hull of input points, using coordinates coord. More exactly, only the upper left part of the hull.
// Assumes that HullPoint and QueryPoint have public members .coord0 and .coord1 returning the x/y coordinates. And HullPoint has default constructors (because we use std::vector::resize).
// The hull is constructed by adding points in increasing order of first coordinates, and also stores the points in this order.
// At any time during construction, one can perform extreme point aueries in the direction of any given 2D vector,
//    but extreme points returned must have increasing first coordinate
//    (this is necessary to have amortized O(1) complexity for extreme queries, using index_last_extreme).
// Complexity: amortized O(1) for construction, amortized O(1) for extreme point query.
template<typename T, typename HullPoint, typename QueryPoint> class UpperHull{
public:
	UpperHull(): upleft_hull_({}), index_last_extreme_(0) {}
	// Insert the point if needed, updates the upper hull.
	// Assumes the new point has larger first coordinate than any hull point.
	// Complexity: O(number of items removed from hull).
	void add_point(const HullPoint &point);
	// Returns extreme point among {upleft_hull[index_last_extreme],...upleft_hull.back()} in the direction of any given 2D vector.
	// And updates index_last_extreme to this point.
	// Complexity: O(distance between index_last_extreme and new extreme).
	HullPoint Extreme(const QueryPoint &point);
	// Returns the points inside the hull. This function is not used in hssp2dExtreme.
	std::vector<HullPoint> Points();
private:
	std::vector<HullPoint> upleft_hull_; // points of upper left hull (sorted by increasing first coordinate)
	size_t index_last_extreme_;
	// Auxiliary methods.
	T Cross(const HullPoint &o, const HullPoint &x, const HullPoint &y); // true iff clockwise turn (from ox to oy) ie 0<=(ox,oy)<pi, under the assumption that o,x,y have increasing x-coordinates. Complexity O(1).
	T Dotp_compare(const HullPoint &x, const HullPoint &y, const QueryPoint &d); // Compares dot products <x,d> and <y,d>. Complexity O(1).
	HullPoint Findextreme_dichotomy(size_t low, size_t high, const QueryPoint &direction); // This function is not used in hssp2dExtreme.
};



// --------------------------------------------
// Implementation of the methods:

template<typename T, typename HullPoint, typename QueryPoint>
void UpperHull<T, HullPoint, QueryPoint>::add_point(const HullPoint &point) {
	assert(upleft_hull_.size() == 0 || point.coord0 >= (upleft_hull_.back()).coord0);
	if (upleft_hull_.size() == 0) {
		upleft_hull_.push_back(point);
		index_last_extreme_ = 0;
	}
	else if ((upleft_hull_.back()).coord1 < point.coord1) {
		size_t k = upleft_hull_.size();
		while (k>1 && Cross(upleft_hull_[k-2], upleft_hull_[k-1], point) > 0) {
			--k;
		}
		upleft_hull_.resize(k);
		upleft_hull_.push_back(point);
		index_last_extreme_ = std::min(index_last_extreme_, k-1);
	}
	// No operation is performed  if (upleft_hull.back()[1] >= point[1]).
}



template<typename T, typename HullPoint, typename QueryPoint>
std::vector<HullPoint> UpperHull<T, HullPoint, QueryPoint>::Points() { return upleft_hull_; }



template<typename T, typename HullPoint, typename QueryPoint>
T UpperHull<T, HullPoint, QueryPoint>::Cross(const HullPoint &o, const HullPoint &x, const HullPoint &y) {  // true iff clockwise turn (from ox to oy) ie 0<=(ox,oy)<pi, under the assumption that o,x,y have increasing x-coordinates
	//	assert(o.coord0 < x.coord0 < y.coord0);
	return (x.coord0-o.coord0) * (y.coord1-o.coord1) >= (x.coord1-o.coord1) * (y.coord0-o.coord0);
	//	Pb with above: risks of overflow has it is basically elevates input range to the square.
	//	return (x.coord0-o.coord0) * (y.coord1-o.coord1) >= (x.coord1-o.coord1) * (y.coord0-o.coord0);
	//  Better solution for integer types - but might be slower:
	//		return ((y.coord1-o.coord1) / (y.coord0-o.coord0)) > ((x.coord1-o.coord1) / (x.coord0-o.coord0))
	//				|| (
	//						((y.coord1-o.coord1) / (y.coord0-o.coord0)) == ((x.coord1-o.coord1) / (x.coord0-o.coord0))
	//						&& ((y.coord1-o.coord1) % (y.coord0-o.coord0)) >= ((x.coord1-o.coord1) % (x.coord0-o.coord0))
	//						);
}

template<typename T, typename HullPoint, typename QueryPoint>
T UpperHull<T, HullPoint, QueryPoint>::Dotp_compare(const HullPoint &x, const HullPoint &y, const QueryPoint &d) {  // Compares dot products: returns <x,d> - <y,d>.
	return (x.coord0-y.coord0) * d.coord0 + (x.coord1-y.coord1) * d.coord1;
}

template<typename T, typename HullPoint, typename QueryPoint>
HullPoint UpperHull<T, HullPoint, QueryPoint>::Extreme(const QueryPoint &point) {
	size_t k = index_last_extreme_;
	const size_t n = upleft_hull_.size();
	while (k < n-1 && Dotp_compare(upleft_hull_[k], upleft_hull_[k+1], point) < 0) {
		++k;
	}
	index_last_extreme_ = k;
	return upleft_hull_[k];
}


template<typename T, typename HullPoint, typename QueryPoint>
HullPoint UpperHull<T, HullPoint, QueryPoint>::Findextreme_dichotomy(size_t low, size_t high, const QueryPoint &direction) {
	size_t mid = low + (high - low)/2;
	if (high - low < 2) {
		if (high == low) {
			return upleft_hull_[low];
		}
		else {  // high == low+1
			if (Dotp_compare(upleft_hull_[low], upleft_hull_[high], direction) > 0) {
				return upleft_hull_[low];
			}
			else {
				return upleft_hull_[high];
			}
		}
	}
	else { // (high-low > 2) so mid >low and mid<high
		if (Dotp_compare(upleft_hull_[mid-1], upleft_hull_[mid], direction) < 0
				&& Dotp_compare(upleft_hull_[mid+1], upleft_hull_[mid], direction) < 0) {
			return upleft_hull_[mid];
		}
		else if (Dotp_compare(upleft_hull_[mid-1], upleft_hull_[mid], direction) < 0
				&& Dotp_compare(upleft_hull_[mid+1], upleft_hull_[mid], direction) == 0) {
			return findextreme(mid+1, high, direction);
		}
		else if (Dotp_compare(upleft_hull_[mid+1], upleft_hull_[mid], direction) < 0
				&& Dotp_compare(upleft_hull_[mid-1], upleft_hull_[mid], direction) == 0) {
			return findextreme(low, mid-1, direction);
		}
		else if (Dotp_compare(upleft_hull_[mid-1], upleft_hull_[mid], direction) < 0) { // && necessarily dotp_compare(convhull[mid+1], convhull[mid], direction) > 0.
			return findextreme(mid+1, high, direction);
		}
		else  { // &&  In our setting the dotproduct on upper hull of a (negative, positive) increases until maxima then decreases.
			// Therefore necessarily dotp_compare(convhull[mid+1], convhull[mid], direction) < 0
			// and consequently also dotp_compare(convhull[mid-1], convhull[mid], direction) > 0.
			return findextreme(low, mid-1, direction);
		}
	}
}

}  // namespace hull

#endif /* UPPERHULL_H */
