/*
 * UpperHull_Updatable.hpp
 *
 *  Created on: Aug 17, 2016
 *      Author: bgroz
 */


#ifndef UPPERHULL_UPDATABLE_DOUBLE_H
#define UPPERHULL_UPDATABLE_DOUBLE_H

#include <algorithm>
#include <cmath>
#include <iostream>
#include <set>
#include <string>
#include <type_traits>
#include <vector>


#include "utils/point_representation.hpp"
#include "utils/floating_point_arithmetics.hpp"

#include "hull/upperhull_updatable.hpp"

namespace hull {

// THIS FILE IS EXACTLY THE SAME AS UPPERHULL_UPDATABLE_H EXCEPT THAT COMPARISONS ARE RELAXED FOR FLOATING POINT ARITHMETIC.
// Computes the 2D convex hull (actually only the upper_left hull quadrant of the convex hull).
// Assumes that HullPoint and QueryPoint have public members .coord0 and .coord0 returning the x/y coordinates, and .left_slope returning the slope to the left of the point in the hull (+infty if none). (And have default constructors?)
// Assumes that HullPoint and QueryPoint have negative coord0, positive coord0.
// Supports insertion of a new point on the hull.
// Also allows to query extreme point in the direction of any given 2D vector (performed as a search on slope).
// Complexity: O(log n) for adding point and for extreme point query.
// Constructor assumes number of input points > 0.
// Does not assume general position: points may have equal first coordinates or equal second coordinates.




// Here InputContainer must be a container of HullPointWithSlope. The slope may be arbitrary.
template<typename HullPointWithSlope, typename QueryPoint> class UpperHull_Set<double, HullPointWithSlope, QueryPoint>{
public:
	UpperHull_Set() { convhull_ = {}; };
	
	// This constructor is presumably faster in practice than creating an empty hull and adding points one at a time
	//   because we first sort points, which facilitates the insertions: insertion costs sum up to O(h) after the sorting step.
	// Complexity: O(h log h) where h=points.size().
	// UpperHull_Set(InputContainer &points);
	
	size_t get_hull_size() const;

	// Inserts the point at the proper position in hull.
	// The point left_slope should be std::numeric_limits<double>::max().
	// Complexity: O(log h) where h is the Hull size.
	void add_point(HullPointWithSlope point);

	// Assumes Hull is not empty, otherwise extreme will have undefined behaviour (accessing index -1).
	// Does not use point.id nor point.left_slope.
	// Complexity: O(log h) where h is the Hull size (exploits the trick of searching set on order correlated with key).
	HullPointWithSlope Extreme(const QueryPoint &point) const;


	std::vector<HullPointWithSlope> get_hull_points() const;  // For tests and debugging.
//	std::string get_hstring() const;

private:
	std::set<HullPointWithSlope, std::less<void>> convhull_;  // points of upper left hull quadrant
	bool Cross(const HullPointWithSlope &o, const HullPointWithSlope &x, const HullPointWithSlope &y) const; // true iff clockwise turn (from ox to oy) ie 0<=(ox,oy)<pi, under the assumption that o,x,y have increasing x-coordinates
};



// --------------------------------------------
// Implementation of the methods:


// template<typename HullPointWithSlope,  typename QueryPoint>
// UpperHull_Set<double, HullPointWithSlope, QueryPoint>::UpperHull_Set(InputContainer &points) {
// 	assert(points.size()>0);
// 	size_t k = 0;
// 	size_t n = std::distance(points.begin(), points.end());
// 	std::vector<HullPointWithSlope> aux_convhull_;
// 	aux_convhull_.resize(n);  // Allocate at most the number of points;
//
// 	// Sort the points
// 	std::sort(points.begin(), points.end(), [](HullPointWithSlope a, HullPointWithSlope b){ return a.coord0 < b.coord0 || (std::abs(a.coord0 - b.coord0) < PREPROCESSOR_EPSILON && a.coord1 <= b.coord1); });
//
// 	// Compute the upper hull
// 	aux_convhull_[k++] = points[0];
// 	assert(points[0].left_slope == std::numeric_limits<double>::max());
// 	for(size_t i = 1; i < n; ++i) {
// 		if (aux_convhull_[k-1].coord1 < points[i].coord1) {
// 			while ((k >= 2) && ((std::abs(points[i].coord0 - aux_convhull_[k-1].coord0) < PREPROCESSOR_EPSILON && Cross(aux_convhull_[k-2], aux_convhull_[k-1], points[i])))) {
// 				k--;
// 			}
// 			points[i].left_slope = (points[i].coord1 - aux_convhull_[k-1].coord1) / (points[i].coord0 - aux_convhull_[k-1].coord0);
// 			aux_convhull_[k++] = points[i];
// 		}
// 	}
// 	//	aux_convhull_.resize(k);
// 	convhull_ = std::set<HullPointWithSlope, std::less<void>>(aux_convhull_.begin(), aux_convhull_.begin()+k);
// }

template<typename HullPointWithSlope, typename QueryPoint>
size_t UpperHull_Set<double, HullPointWithSlope, QueryPoint>::get_hull_size() const {
	return convhull_.size();
}

template<typename HullPointWithSlope, typename QueryPoint>
std::vector<HullPointWithSlope> UpperHull_Set<double, HullPointWithSlope, QueryPoint>::get_hull_points() const {
	return std::vector<HullPointWithSlope>(convhull_.begin(), convhull_.end());
}

template<typename HullPointWithSlope, typename QueryPoint>
void UpperHull_Set<double, HullPointWithSlope, QueryPoint>::add_point(HullPointWithSlope point) {
	typedef typename std::set<HullPointWithSlope, std::less<void>>::iterator hull_iter;
	if (convhull_.size() == 0) {
		assert(point.left_slope == std::numeric_limits<double>::max());
		convhull_.insert(point);
	}
	else {
		hull_iter next_hull_point = convhull_.lower_bound(point);
		if (next_hull_point == convhull_.begin()) {
			assert(point.left_slope == std::numeric_limits<double>::max());
			if (point.coord1 >= std::prev(convhull_.end(), 1)->coord1) {
				convhull_.clear();
				convhull_.insert(point);
			}
			else {
				if (std::abs(point.coord0 - next_hull_point->coord0) < PREPROCESSOR_EPSILON) {
					if (point.coord1 > next_hull_point->coord1) {
						hull_iter next_it = std::next(next_hull_point,1);
						while (next_it->coord1 < point.coord1) { // this cannot exceed convhull_.end() due to conditions above
							++next_it;
						}
						hull_iter high_it = std::next(next_it, 1);
						while(high_it != convhull_.end() && Cross(point, *next_it, *high_it)) {
							++high_it;
							++next_it;
						}
						// Here we know that next_it remains in the hull (in particular, next_it->coord1 > point.coord1)
						// next_it is not really deleted: we only delete it in order to update next_it->left_slope through reinsertion
						// (std::set don't allow updates even on attributes independent from the set order).
						HullPointWithSlope updated_next_it = {next_it->coord0, next_it->coord1, next_it->id, double(next_it->coord1 - point.coord1) / double(next_it->coord0 - point.coord0)};
						convhull_.erase(next_hull_point, high_it);
						convhull_.insert(high_it, point);
						convhull_.insert(high_it, updated_next_it);

					}
				}
				else {
					convhull_.insert(next_hull_point, point);
					hull_iter next_it = next_hull_point;  // In this case, next_it initialized to convhull_.begin() and != convhull_.end()
					hull_iter high_it = std::next(next_it, 1);
					while(high_it != convhull_.end() && Cross(point, *next_it, *high_it)) {
						++high_it;
						++next_it;
					}
					// Here we know that next_it remains in the hull (in particular, next_it->coord1 > point.coord1)
					// next_it is not really deleted: we only delete it in order to update next_it->left_slope through reinsertion
					// (std::set don't allow updates even on attributes independent from the set order).
					HullPointWithSlope updated_next_it = {next_it->coord0, next_it->coord1, next_it->id, double(next_it->coord1 - point.coord1) / double(next_it->coord0 - point.coord0)};
					convhull_.erase(next_hull_point, high_it);
					convhull_.insert(high_it, updated_next_it);
				}
			}
		}
		else if (point.coord1 >= std::prev(convhull_.end(), 1)->coord1) {
			// then we do not have to worry about point.coord0==next_hull_point.coord0 as the latter is erased anyway.
			hull_iter prev_it = std::prev(next_hull_point, 1);
			if (prev_it != convhull_.begin()) {
				hull_iter low_it = std::prev(prev_it,1);
				while(low_it != convhull_.begin() && Cross(*low_it, *prev_it, point)) {
					--low_it;
					--prev_it;
				}
				if (low_it == convhull_.begin() && Cross(*low_it, *prev_it, point)) {
					--prev_it;
				}
			}
			point.left_slope = double(point.coord1 - prev_it->coord1) / double(point.coord0 - prev_it->coord0);
			convhull_.erase(std::next(prev_it,1), convhull_.end());
			convhull_.insert(convhull_.end(), point);
		}
		else if ((next_hull_point != convhull_.end()) && \
				((std::abs(point.coord0 - next_hull_point->coord0) < PREPROCESSOR_EPSILON) || !Cross(*(std::prev(next_hull_point, 1)), point, *next_hull_point))) {
			if (std::abs(point.coord0 - next_hull_point->coord0) < PREPROCESSOR_EPSILON && (point.coord1 < next_hull_point->coord1)) {
				// Nothing to do.
			}
			else {
				hull_iter next_it = next_hull_point;
				hull_iter high_it = std::next(next_hull_point, 1);
				if (std::abs(point.coord0 - next_hull_point->coord0) < PREPROCESSOR_EPSILON && (point.coord1 > next_hull_point->coord1)) {
					++high_it;
					++next_it;
				}
				hull_iter prev_it = std::prev(next_hull_point, 1);
				hull_iter low_it = prev_it;
				if (prev_it != convhull_.begin()) {
					low_it = std::prev(prev_it,1);
					while(low_it != convhull_.begin() && Cross(*low_it, *prev_it, point)) {
						--low_it;
						--prev_it;
					}
					if (low_it == convhull_.begin() && Cross(*low_it, *prev_it, point)) {
						--prev_it;
					}
				}
				while(high_it != convhull_.end() && Cross(point, *next_it, *high_it)) {
					++high_it;
					++next_it;
				}
				// Erasing between prev_it and next_it (both excluded), updating slope of next_it and inserted point
				HullPointWithSlope updated_next_it = {next_it->coord0, next_it->coord1, next_it->id, double(next_it->coord1 - point.coord1) / double(next_it->coord0 - point.coord0)};
				point.left_slope = double(point.coord1 - prev_it->coord1) / double(point.coord0 - prev_it->coord0);
				convhull_.erase(std::next(prev_it,1), high_it);
				convhull_.insert(high_it, point);
				convhull_.insert(high_it, updated_next_it);
			}
		}
		// In the remaining case: cross(*(std::prev(next_hull_point, 1)), point, *next_hull_point) == true, point is not inserted so the hull needs no update.
	}
}

template<typename HullPointWithSlope, typename QueryPoint>
bool UpperHull_Set<double, HullPointWithSlope, QueryPoint>::Cross(const HullPointWithSlope &o, const HullPointWithSlope &x, const HullPointWithSlope &y) const{
	//	assert(o.coord0 < x.coord0 < y.coord0);
	//	return (x.coord0-o.coord0) * (y.coord1-o.coord1) >= (x.coord1-o.coord1) * (y.coord0-o.coord0);
	//	Pb with above: risks of overflow has it is basically elevates input range to the square.
	//  Better solution for integer types:
	return ((y.coord1-o.coord1) / (y.coord0-o.coord0)) > ((x.coord1-o.coord1) / (x.coord0-o.coord0)) - PREPROCESSOR_EPSILON;
}
//
//

template<typename HullPointWithSlope, typename QueryPoint>
HullPointWithSlope UpperHull_Set<double, HullPointWithSlope, QueryPoint>::Extreme(const QueryPoint &point) const {
	assert(convhull_.size() > 0);
	utils::Wrap_double s(-(double)point.coord0/(double)point.coord1);
	auto current_hull_point = convhull_.template lower_bound<utils::Wrap_double>(s);  // in general, it should be -point.coord0 / point.coord1 but we always have point.coord1 = 1 so we could simplify.
	return *std::prev(current_hull_point,1);
}

}  // namespace hull
#endif /* UPPERHULL_UPDATABLE_DOUBLE_H */
