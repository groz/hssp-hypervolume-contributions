/*
 * other_utils.hpp
 *
 *  Created on: Jul 5, 2016
 *      Author: bgroz
 *
 *
 *  Implements:
 *  
 */


#ifndef OTHER_UTILS_H_
#define OTHER_UTILS_H_

#include <iostream>
#include <cmath>  // for abs.
#include <string>
#include <vector>

#include "floating_point_arithmetics.hpp"

namespace utils {

template<class Point>
bool check_general_position(std::vector<Point> v);

// Takes as input a set of points for our pb: where (exclusive) contribution of point q is {points p' | 0 < p'< q, and such that for any input point p'': p'<p'' => p''< q}.
// and transforms those points into equivalent dataset for our pb, where (exclusive) contribution is {points p' | q < p'< refpoint, and such that for any input point p''< p' : q< p''}.
// To perform the conversion: point coordinates are multiplied by -1, and offset by the maximal value on each coordinate
// (the minimal value on each coordinate after the transformation becomes 0).
// Complexity: O(dim*n)
template<class Point>
void convert_maximization_to_positive_minimization_with_reference(const std::vector<Point> &points, std::vector<Point> &vec, Point &refpoint, int dim);

// Same function but for Point = std::Array<T, 2>
template<class Point>
void convert_maximization_to_positive_minimization_with_reference_array2d(const std::vector<Point> &points, std::vector<Point> &vec, Point &refpoint);

// Same function but for Point = std::Array<T, 3>
template<class Point>
void convert_maximization_to_positive_minimization_with_reference_array3d(const std::vector<Point> &points, std::vector<Point> &vec, Point &refpoint);

// // Takes as input a set of points meant for a pb where contribution of q is the volume of {points p' | q < p'< refpoint, and such that for any input point p''< p' : q< p''}
// // and transforms those points into equivalent dataset for our pb, where contribution is {points p' | 0 < p'< newq, and such that for any input point p'': p'<p'' => p''< q}.
// template<class T, class Point>
// void convert_minimization_with_reference_to_maximization(std::string infilename, Point refpoint, std::vector<Point> &vec);
//




// --------------------------------------------
// Implementation:
// --------------------------------------------

template<class Point>
bool check_general_position(std::vector<Point> v) {  // Call by value so that we can sort without affecting input
	bool found = false;
	for (size_t i = 0; i < v[0].size() && !found; ++i) {
		sort(v.begin(), v.end(), [=](Point w, Point wb) {return w[i] < wb[i];} );
		auto duplicate_index = std::adjacent_find(v.begin(), v.end(), [=](Point w,Point wb) {return std::abs(w[i]-wb[i]) < PREPROCESSOR_EPSILON;} );
		if (duplicate_index != v.end()) {
			found = true;
			std::cout << "duplicate found: value " << (*duplicate_index)[i] << " on coordinate " << i << std::endl;
		}
	}
	return !found;
}

template<class Point>
void convert_maximization_to_positive_minimization_with_reference(const std::vector<Point> &points, std::vector<Point> &vec, Point &refpoint, int dim) {
	typedef typename Point::value_type T;
	std::vector<T> max_coord(dim, 0);
	for (auto p : points) {
		for (int i = 0; i < dim; ++i) {
			max_coord[i] = std::max(max_coord[i], p[i]);
		}
	}
	std::transform(points.begin(), points.end(), std::back_inserter(vec),
			[max_coord](Point p) {int j = 0; std::transform(p.begin(), p.end(), p.begin(), [=,&j](T val) {return max_coord[j++]-val;});  // p has been multiplied by -1
			return p;});
	refpoint = Point(max_coord.begin(), max_coord.end());
}

template<class Point>
std::vector<typename Point::value_type> convert_maximization_to_positive_minimization_with_reference_array(int dim, const std::vector<Point> &points, std::vector<Point> &vec) {
	typedef typename Point::value_type T;
	std::vector<T> max_coord(dim, 0);
	for (auto p : points) {
		for (int i = 0; i < dim; ++i) {
			max_coord[i] = std::max(max_coord[i], p[i]);
		}
	}
	std::transform(points.begin(), points.end(), std::back_inserter(vec),
			[max_coord](Point p) {int j = 0; std::transform(p.begin(), p.end(), p.begin(), [=,&j](T val) {return max_coord[j++] - val;});  // p has been multiplied by -1
			return p;});
	return max_coord;
}

template<class Point>
void convert_maximization_to_positive_minimization_with_reference_array2d(const std::vector<Point> &points, std::vector<Point> &vec, Point &refpoint) {
	int dim = 2;
	auto max_coord = convert_maximization_to_positive_minimization_with_reference_array(dim, points,  vec);
	refpoint = {{max_coord[0], max_coord[1]}};
}

template<class Point>
void convert_maximization_to_positive_minimization_with_reference_array3d(const std::vector<Point> &points, std::vector<Point> &vec, Point &refpoint) {
	int dim = 3;
	auto max_coord = convert_maximization_to_positive_minimization_with_reference_array(dim, points,  vec);
	refpoint = {{max_coord[0], max_coord[1], max_coord[2]}};
}


}  // namespace utils

#endif /* OTHER_UTILS_H_ */
