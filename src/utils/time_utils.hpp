/*
 * time_utils.hpp
 *
 *  Created on: Jul 5, 2016
 *      Author: bgroz
 *
 *
 *  Implements:
 *  functions that return the current timestamp, and the delay between two timestamps.
 */


#ifndef TIME_UTILS_H_
#define TIME_UTILS_H_

#include <chrono>
#include <ctime>
#include <iostream>
#include <cmath>  // for abs.
#include <string>
#include <vector>

#include "floating_point_arithmetics.hpp"

namespace utils {


std::chrono::time_point<std::chrono::high_resolution_clock> current_clock();

std::string current_time();

std::string delay_between(std::chrono::time_point<std::chrono::high_resolution_clock> t1, std::chrono::time_point<std::chrono::high_resolution_clock> t2);

// --------------------------------------------
// Implementation::
// --------------------------------------------

std::chrono::time_point<std::chrono::high_resolution_clock> current_clock() {
	return std::chrono::high_resolution_clock::now();
}


std::string current_time() {
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	auto duration = now.time_since_epoch(); 
	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
	std::time_t time_now = std::chrono::system_clock::to_time_t(now);
        long rem_ms = milliseconds-(long)time_now*1000;	
	std::ostringstream oss;
	oss << std::put_time(std::localtime(&time_now), "%Y-%m-%d %H:%M:%S:");
	oss << std::setfill('0') << std::setw(3) << rem_ms; 
	return oss.str();
}

std::string delay_between(std::chrono::time_point<std::chrono::high_resolution_clock> t1, std::chrono::time_point<std::chrono::high_resolution_clock> t2) {
	return std::to_string(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count())/1000000.0);
}

}  // namespace utils

#endif /* TIME_UTILS_H_ */
