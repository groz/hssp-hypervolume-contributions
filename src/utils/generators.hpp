/*
 * generators.hpp
 *
 *  Created on: Jul 5, 2016
 *      Author: bgroz
 *
 *
 *  Implements:
 *  functions generating points according to some distribution.
 */


#ifndef GENERATORS_H_
#define GENERATORS_H_

#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <random>  // for random point generator.
#include <string>
#include <unordered_set>  // for random point generator.
#include <vector>
#include <math.h>



namespace utils {

// Generates a random set of points uniformly distributed.
// If type is int or long: domain is (0,100*nbpoints)^dimension.
// If type is double or float: same (integer) domain but divided by 10.
template<class T>
void points_generator_gener_pos(std::string infilename, int nbpoints=100, int dimension=2, unsigned int seed=5, int factor_upper_bound=100);

// Generates a random 2D skyline.
// If type is int or long: domain is (0,100*nbpoints)^dimension.
// If type is double or float: same (integer) domain but divided by 10.
// Generation technique: first creating arbitrary lists of x coordinates then sorting on x, and then similarly on y.
template<class T>
void skyline_points_generator_gener_pos(std::string infilename, int nbpoints, unsigned int seed=5, int factor_upper_bound=10);

// Generates a random skyline with points in general position along the hyperplane 'sum of all coordinates = (dimension-1)*max_domain, where max_domain = nbpoints * factor_upper_bound.
// Distribution: first coordinates are generated uniformly independently in 1..max_domain, and the last coordinate is complement w.r.t. (dimension-1)*max_domain. 
// This means last coordinate can be much larger than the others.
void hyperplane_points_generator_gener_pos_dim_d(std::string infilename, int nbpoints, int dimension=2, unsigned int seed=5, int factor_upper_bound=10);

// Generates a 3d skyline having $\Omega(k^2n)$ j-joint contributions, j<k.
void points_generator_gener_distr_1(std::string infilename, int nbpoints);


// Generates a 3d skyline of points following a uniform distribution on the d-dimensional convex and concave spheres
void convex_sphere_points_generator_dim_d(std::string const infilename, int nbpoints,
    int dimension=2, unsigned int seed=5, int factor_upper_bound=10);

void concave_sphere_points_generator_dim_d(std::string const infilename, int nbpoints,
    int dimension=2, unsigned int seed=5, int factor_upper_bound=10);

// Similar to concave except we simulate a shifted origin by requesting that all dimensions except the last are small
void concave_sphere_points_generator_dim_d_shifted_origin(std::string const infilename, int nbpoints,
    int dimension=2, unsigned int seed=5, int factor_upper_bound=10);
void convex_sphere_points_generator_dim_d_shifted_origin(std::string const infilename, int nbpoints,
    int dimension=2, unsigned int seed=5, int factor_upper_bound=10);

// --------------------------------------------
// Implementation:
// --------------------------------------------

// Pseudo_random generator; sequence is deterministic, depends only on seed, low and high bounds.
class RandomIntGenerator {
private:
	std::default_random_engine re_;
	const int low_bound_;
	const int up_bound_;
	std::uniform_int_distribution<int> distribution_{low_bound_,up_bound_};
public:
	RandomIntGenerator(int low, int high, unsigned int seed): re_(std::default_random_engine{seed}), low_bound_(low), up_bound_(high) {
	}
	int rand_int() {  // Returns the 'next' int according to the generator.
		return distribution_(re_);
	}
	std::string to_string() {
		return "generator:\nlow_bound:"+std::to_string(low_bound_)+"\nup_bound:"+std::to_string(up_bound_);
	}
};


// Pseudo_random generator; sequence is deterministic, depends only on seed, low and high bounds.
class NormalDoubleGenerator {
private:
	std::default_random_engine re_;
	const double mean_;
	const double stdev_;
	std::normal_distribution<double> distribution_{mean_,stdev_};
public:
	NormalDoubleGenerator(double mean, double stdev, unsigned int seed): re_(std::default_random_engine{seed}), mean_(mean), stdev_(stdev) {
	}
	double rand_double() {  // Returns the 'next' int according to the generator.
		return distribution_(re_);
	}
	std::string to_string() {
		return "generator:\nmean:"+std::to_string(mean_)+"\nstdev:"+std::to_string(stdev_);
	}
};

template<class T>
void points_generator_gener_pos(std::string infilename, int nbpoints, int dimension, unsigned int seed, int factor_upper_bound) {
	//	std::cout<<"Entering generator"<<std::endl;
	int lower_bound = 1;
	int upper_bound = factor_upper_bound*nbpoints;

	if (remove(infilename.c_str()) != 0)
		std::cerr << "Error deleting file" << std::endl;

	// Might be simple to define pairs (index of coordinate, value of coordinate) and hash those pairs into an unordered set directly.
	std::vector<std::unordered_set<int>> coordinates;
	coordinates.reserve(dimension);
	for (auto i = 0; i < dimension; ++i) {
		std::unordered_set<int> a = {};
		coordinates.push_back(a);
	}

	int nbp_built=0;
	int index_coord = 0;
	RandomIntGenerator rand_generator(lower_bound,upper_bound, seed);

	if (std::is_same<T,int>::value || std::is_same<T,long>::value || std::is_same<T,long long>::value) {
		std::ofstream out(infilename, std::ios::app);
		while (nbp_built<nbpoints) {
			int new_coord_int = rand_generator.rand_int();
			std::unordered_set<int>::const_iterator got = coordinates[index_coord].find(new_coord_int);
			if (got == coordinates[index_coord].end())  // Random values do not correspond to existing coordinates.
			{
				coordinates[index_coord].insert(new_coord_int);
				if (index_coord < dimension-1) {
					index_coord++;
					out << new_coord_int << "\t";
				}
				else {
					index_coord = 0;
					out << new_coord_int << std::endl;
					nbp_built++;
				}
			}
		}
	}
	else if (std::is_same<T,double>::value || std::is_same<T,float>::value) {
		std::ofstream out(infilename, std::ios::app);
		out.setf( std::ios::fixed, std:: ios::floatfield );
		out.precision(1);  // To prevent truncation: we only need one digit as we only divide by 10.
		while (nbp_built<nbpoints) {
			int new_coord_int = rand_generator.rand_int();
			double new_coord = new_coord_int/10.0;
			std::unordered_set<int>::const_iterator got = coordinates[index_coord].find(new_coord_int);
			if(got == coordinates[index_coord].end())  // Random values do not correspond to existing coordinates.
			{
				coordinates[index_coord].insert(new_coord_int);
				if (index_coord < dimension-1) {
					index_coord++;
					out << new_coord << "\t";
				}
				else {
					index_coord = 0;
					out << new_coord << std::endl;
					nbp_built++;
				}
			}
		}
	}
}




template<class T>
void skyline_points_generator_gener_pos(std::string infilename, int nbpoints, unsigned int seed, int factor_upper_bound) {
	//	std::cout<<"Entering generator"<<std::endl;
	int lower_bound = 1;
	int dimension = 2;
	int upper_bound = factor_upper_bound*nbpoints;

	if (remove(infilename.c_str()) != 0)
		std::cerr << "Error deleting file" << std::endl;

	std::vector<std::unordered_set<int>> coordinates;
	coordinates.reserve(dimension);
	for (auto i = 0; i < dimension; ++i) {
		coordinates.push_back({});
	}

	int nbp_built = 0;
	int index_coord = 0;
	RandomIntGenerator rand_generator(lower_bound,upper_bound, seed);

	while (nbp_built < nbpoints) {
		int new_coord_int = rand_generator.rand_int();
		std::unordered_set<int>::const_iterator got = coordinates[index_coord].find(new_coord_int);
		if (got == coordinates[index_coord].end())  // Random values do not correspond to existing coordinates.
		{
			coordinates[index_coord].insert(new_coord_int);
			//			out << new_coord_int;
			if (index_coord < dimension-1) {
				index_coord++;
				//				out << "\t";
			}
			else {
				index_coord = 0;
				//				out << std::endl;
				nbp_built++;
			}
		}
	}


	std::vector<std::vector<int>> sorted_coordinates;
	sorted_coordinates.reserve(dimension);

	std::vector<int> a(coordinates[0].begin(), coordinates[0].end());
	std::sort(a.begin(), a.end());
	sorted_coordinates.push_back(a);
	std::vector<int> b(coordinates[1].begin(), coordinates[1].end());
	std::sort(b.begin(), b.end(), std::greater<int>());
	sorted_coordinates.push_back(b);

	if (std::is_same<T,int>::value || std::is_same<T,long>::value || std::is_same<T,long long>::value) {
		std::ofstream out(infilename, std::ios::app);
		for (int nbp_b = 0; nbp_b < nbpoints; ++nbp_b) {
			for (auto i = 0; i < dimension-1; ++i) {
				out << sorted_coordinates[i][nbp_b] << "\t";
			}
			out << sorted_coordinates[dimension-1][nbp_b] << std::endl;
		}
	}
	else if (std::is_same<T,double>::value || std::is_same<T,float>::value) {
		std::ofstream out(infilename, std::ios::app);
		out.setf( std::ios::fixed, std:: ios::floatfield );
		out.precision(1);  // To prevent truncation: we only need one digit as we only divide by 10.

		for (int nbp_b = 0; nbp_b < nbpoints; ++nbp_b) {
			for (auto i = 0; i < dimension-1; ++i) {
				out << sorted_coordinates[i][nbp_b]/10.0 << "\t";
			}
			out << sorted_coordinates[dimension-1][nbp_b]/10.0 << std::endl;
		}
	}
}


void hyperplane_points_generator_gener_pos_dim_d(std::string infilename, int nbpoints, int dimension, unsigned int seed, int factor_upper_bound) {
	int lower_bound = 1;
	int upper_bound = nbpoints * factor_upper_bound;

	if (remove(infilename.c_str()) != 0 || dimension < 2)
		std::cerr << "Error deleting file" <<std::endl;

	std::vector<std::unordered_set<int>> coord_set(dimension);

	int nbp_built = 0;
	
	RandomIntGenerator rand_generator(lower_bound,upper_bound, seed);

	std::ofstream out(infilename, std::ios::app);
	
	while (nbp_built < nbpoints) {
		int good = 0;
		std::vector<int> coords_final;
		while (!good) {
			coords_final.clear();
			std::vector<int> coords;
			int d = 0;
			while (d < dimension-1) {
				int coord = rand_generator.rand_int();
				coords.push_back(coord); d++;
			}
			// Compute the last coordinate.
			int norm1 = 0;
			for (auto c : coords) norm1 += c;
			int last_coord = (dimension - 1) * upper_bound - norm1 + lower_bound; // Remark: We do not want the coordinate to be 0.
			coords.push_back(last_coord);
		
			good = 1;
			d = 0;
			for (auto c : coords) {
				if (coord_set[d].find(c) == coord_set[d].end()) {
					coords_final.push_back(c);
					d++;
				}
				else{
					good = 0;
					break;
				}
			}
		}

		// Print final coordinates.
		int d = 0;
		for (auto c : coords_final) {
			out << c;
			coord_set[d].insert(c);
			if(d < dimension - 1) out << "\t";
			else out << std::endl;
			d++;
		}
		nbp_built++;
	}
}


void points_generator_gener_distr_1(std::string infilename, int nbpoints) {
	if (remove(infilename.c_str()) != 0)
		std::cerr << "Error deleting file" <<std::endl;

	std::ofstream out(infilename, std::ios::app);
	for (int i = 1; i <= nbpoints/2; ++i) {
		out << nbpoints - i + 1 << "\t";
		out << i << "\t";
		out << nbpoints/ + 1 - i << std::endl;
	}
	for (int i = nbpoints/2 + 1; i <= nbpoints; ++i) {
		out << nbpoints - i + 1<< "\t";
		out << i << "\t";
		out << i << std::endl;
	}
}


void convex_sphere_points_generator_dim_d(std::string const infilename, int nbpoints,
	int dimension, unsigned int seed, int factor_upper_bound) {
	if (remove(infilename.c_str()) != 0 || dimension < 2)
		std::cerr << "Error deleting file" <<std::endl;

	int nbp_built = 0;
	int scale = nbpoints * factor_upper_bound;

	NormalDoubleGenerator rand_generator(0.0, 1.0, seed);

	std::ofstream out(infilename, std::ios::app);

	std::vector<std::unordered_set<int>> coord_set(dimension);

	while (nbp_built < nbpoints) {
		int good = 0;
		std::vector<int> coords_final;
		while (!good) {
			coords_final.clear();
			// Draw a random d-dimensional vector with coordinates in Normal(0,1).
			std::vector<double> coords;
			int d = 0;
			while (d < dimension) {
				double coord = std::abs(rand_generator.rand_double());
				coords.push_back(coord); d++;
			}
			// Compute the norm.
			double norm = 0;
			for (auto c : coords) norm += c*c;
			norm = sqrt(norm);
			if (norm > 0) { // Eliminates the unlucky case where norm == 0;
				// Transform the coordinates.
				good = 1;
				d = 0;
				for (auto c:coords) {
					int coord_transf = (scale * c)/norm;
					// Check whether coordinate is in general position
					if ((coord_set[d]).find(coord_transf) == (coord_set[d]).end() && coord_transf > 0) {
						coords_final.push_back(coord_transf);
						d++;
					}
					else {
						good = 0;
						break;
					}
				}	  		
		  	}
		}

		// Print final coordinates.
		int d = 0;
		for (auto c : coords_final) {
			out << c;
			coord_set[d].insert(c);
			if (d < dimension - 1) out << "\t";
			else out << std::endl;
			d++;
		}
		nbp_built++;
	}
	out.close();
}

void concave_sphere_points_generator_dim_d(std::string const infilename, int nbpoints,
	int dimension, unsigned int seed, int factor_upper_bound) {
	if (remove(infilename.c_str()) != 0 || dimension < 2)
		std::cerr << "Error deleting file" <<std::endl;

	int nbp_built = 0;
	int scale = nbpoints * factor_upper_bound;

	NormalDoubleGenerator rand_generator(0.0, 1.0, seed);

	std::ofstream out(infilename, std::ios::app);

	std::vector<std::unordered_set<int>> coord_set(dimension);

	while (nbp_built < nbpoints) {
		int good = 0;
		std::vector<int> coords_final;
		while (!good) {
			coords_final.clear();
			// Draw a random d-dimensional vector with coordinates in Normal(0,1).
			std::vector<double> coords;
			int d = 0;
			while (d < dimension) {
				double coord = std::abs(rand_generator.rand_double());
				coords.push_back(coord); d++;
			}
			// Compute the norm.
			double norm = 0;
			for (auto c : coords) norm += c*c;
			norm = sqrt(norm);
			if (norm > 0) { // Eliminates the unlucky case where norm == 0;
				// Transform the coordinates.
				good = 1;
				d = 0;
				for (auto c : coords){
					int coord_transf = scale - (scale * c)/norm;
					// Check whether coordinate is in general position
					if (coord_set[d].find(coord_transf) == coord_set[d].end() && coord_transf > 0) {
						coords_final.push_back(coord_transf);
						d++;
					}
					else{
						good = 0;
						break;
					}
				}
			}
		}

		// Print final coordinates.
		int d = 0;
		for (auto c : coords_final) {
			out << c;
			coord_set[d].insert(c);
			if(d < dimension - 1) out << "\t";
			else out << std::endl;
			d++;
		}
		nbp_built++;
	}
	out.close();
}


void convex_sphere_points_generator_dim_d_shifted_origin(std::string const infilename, int nbpoints,
	int dimension, unsigned int seed, int factor_upper_bound) {
	if (remove(infilename.c_str()) != 0 || dimension < 2)
		std::cerr << "Error deleting file" <<std::endl;

	int nbp_built = 0;
	int scale = nbpoints * factor_upper_bound;

	NormalDoubleGenerator rand_generator(0.0, 1.0, seed);

	std::ofstream out(infilename, std::ios::app);

	std::vector<std::unordered_set<int>> coord_set(dimension);

	while (nbp_built < nbpoints) {
		int good = 0;
		std::vector<int> coords_final;
		while (!good) {
			coords_final.clear();
			// Draw a random d-dimensional vector with coordinates in Normal(0,1).
			std::vector<double> coords;
			int d = 0;
			while (d < dimension) {
				double coord = std::abs(rand_generator.rand_double());
				while ((coord < .6 && d < dimension - 1) || (coord >.4 && d == dimension - 1)) {
					coord = std::abs(rand_generator.rand_double());
				}
				coords.push_back(coord); d++;
			}
			// Compute the norm.
			double norm = 0;
			for (auto c : coords) norm += c*c;
			norm = sqrt(norm);
			if (norm > 0) { // Eliminates the unlucky case where norm == 0;
				// Transform the coordinates.
				good = 1;
				d = 0;
				for (auto c:coords) {
					int coord_transf = (scale * c)/norm;
					// Check whether coordinate is in general position
					if ((coord_set[d]).find(coord_transf) == (coord_set[d]).end() && coord_transf > 0) {
						coords_final.push_back(coord_transf);
						d++;
					}
					else {
						good = 0;
						break;
					}
				}	  		
		  	}
		}

		// Print final coordinates.
		int d = 0;
		for (auto c : coords_final) {
			out << c;
			coord_set[d].insert(c);
			if (d < dimension - 1) out << "\t";
			else out << std::endl;
			d++;
		}
		nbp_built++;
	}
	out.close();
}


void concave_sphere_points_generator_dim_d_shifted_origin(std::string const infilename, int nbpoints,
	int dimension, unsigned int seed, int factor_upper_bound) {
	if (remove(infilename.c_str()) != 0 || dimension < 2)
		std::cerr << "Error deleting file" <<std::endl;

	int nbp_built = 0;
	int scale = nbpoints * factor_upper_bound;

	NormalDoubleGenerator rand_generator(0.0, 1.0, seed);

	std::ofstream out(infilename, std::ios::app);

	std::vector<std::unordered_set<int>> coord_set(dimension);

	while (nbp_built < nbpoints) {
		int good = 0;
		std::vector<int> coords_final;
		while (!good) {
			coords_final.clear();
			// Draw a random d-dimensional vector with coordinates in Normal(0,1).
			std::vector<double> coords;
			int d = 0;
			while (d < dimension) {
				double coord = std::abs(rand_generator.rand_double());
				while ((coord < .6 && d < dimension - 1) || (coord >.4 && d == dimension - 1)) {
					coord = std::abs(rand_generator.rand_double());
				}
				coords.push_back(coord); d++;
			}
			// Compute the norm.
			double norm = 0;
			for (auto c : coords) norm += c*c;
			norm = sqrt(norm);
			if (norm > 0) { // Eliminates the unlucky case where norm == 0;
				// Transform the coordinates.
				good = 1;
				d = 0;
				for (auto c : coords){
					int coord_transf = scale - (scale * c)/norm;
					// Check whether coordinate is in general position
					if (coord_set[d].find(coord_transf) == coord_set[d].end() && coord_transf > 0) {
						coords_final.push_back(coord_transf);
						d++;
					}
					else{
						good = 0;
						break;
					}
				}
			}
		}

		// Print final coordinates.
		int d = 0;
		for (auto c : coords_final) {
			out << c;
			coord_set[d].insert(c);
			if(d < dimension - 1) out << "\t";
			else out << std::endl;
			d++;
		}
		nbp_built++;
	}
	out.close();
}

}  // namespace utils

#endif /* GENERATORS_H_ */
