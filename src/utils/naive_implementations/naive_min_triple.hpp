/*
 * naive_mintriple.h
 *
 *  Created on: Aug 8, 2016
 *      Author: bgroz
 */

#ifndef NAIVE_MIN_TRIPLE_H_
#define NAIVE_MIN_TRIPLE_H_



#include <algorithm>
#include <iterator>
#include <limits>
#include <numeric>
#include <tuple>
#include <utility>
#include <vector>

#include "weighted_hypergraph/weighted_hypergraph.h"

namespace utils {
	
template<typename VolumeType, typename IndexType>
std::pair<VolumeType, std::vector<IndexType>> compute_min_pair_naive(weighted_hypergraph::WGraph<VolumeType, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>>& g) {

	VolumeType cost = std::numeric_limits<VolumeType>::max();
	IndexType node1 = 0;
	IndexType node2 = 0;

	std::vector<IndexType> nodeset = g.get_nodes();  // Copying g_nodeset so that it is sorted once and for all by node weight.
	std::sort(nodeset.begin(), nodeset.end());
	for (auto n1 = nodeset.begin(); n1 != nodeset.end(); ++n1) {
		for (auto n2 = std::next(n1, 1); n2 != nodeset.end(); ++n2) {
			VolumeType tempcost = g.get_n_weight(*n1) + g.get_n_weight(*n2);
			if (g.has_edge_sorted({*n1, *n2})) {
				tempcost += g.get_e_weight_sorted({*n1, *n2});
			}
			if (tempcost < cost) {
				cost = tempcost;
				node1 = *n1;
				node2 = *n2;
			}
		}
	}
	return {cost, {node1, node2}};
}


template<typename VolumeType, typename IndexType>
std::pair<VolumeType, std::vector<IndexType>> compute_min_triple_naive(weighted_hypergraph::WGraph<VolumeType, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>>& g) {
	typedef typename std::tuple<IndexType, IndexType, IndexType> HEdge;

	VolumeType cost = std::numeric_limits<VolumeType>::max();
	IndexType node1 = 0;
	IndexType node2 = 0;
	IndexType node3 = 0;

	std::vector<IndexType> nodeset = g.get_nodes();  // Copying g_nodeset so that it is sorted once and for all by node id

	for (auto n1 = nodeset.begin(); n1 != nodeset.end(); ++n1) {
		for (auto n2 = std::next(n1, 1); n2 != nodeset.end(); ++n2) {
			for (auto n3 = std::next(n2, 1); n3 != nodeset.end(); ++n3) {
				VolumeType tempcost = g.get_n_weight(*n1) + g.get_n_weight(*n2) + g.get_n_weight(*n3)
				+ g.get_e_weight_if_exists(*n1, *n2)
				+ g.get_e_weight_if_exists(*n1, *n3)
				+ g.get_e_weight_if_exists(*n2, *n3)
				;
				HEdge candid(*n1, *n2, *n3);
				if (g.has_hyperedge_sorted(candid)) {
					tempcost += g.get_h_weight_sorted(candid);
				}
				if (tempcost < cost) {
					cost = tempcost;
					node1 = *n1;
					node2 = *n2;
					node3 = *n3;
				}
			}
		}
	}
	return {cost, {node1, node2, node3}};
}


}  // namespace utils

#endif /* NAIVE_MIN_TRIPLE_H_ */
