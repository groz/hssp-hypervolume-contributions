/*
* naive_hssp.h
*
*  Created on: Aug 8, 2016
*      Author: bgroz
*/
#ifndef NAIVE_HSSP_H_
#define NAIVE_HSSP_H_

#include <algorithm>
#include <iostream>
#include <iterator>
#include <limits>
#include <numeric>
#include <utility>
#include <vector>


namespace utils{

// Assumes input vector consists of 3 points in dimension 3.
// Complexity O(n^3).
template<class Point>
typename Point::value_type hypervolume3points(std::vector<Point> v) {
	typename Point::value_type vol = 0;

	for (auto p1 = v.begin(); p1 != v.end(); ++p1) {
		vol += (*p1)[0] * (*p1)[1] * (*p1)[2];
		for (auto p2 = std::next(p1,1); p2 != v.end(); ++p2) {
			vol -= std::min((*p1)[0],(*p2)[0]) * std::min((*p1)[1],(*p2)[1]) * std::min((*p1)[2],(*p2)[2]);
			for (auto p3 = std::next(p2,1); p3 != v.end(); ++p3) {
				vol += std::min({(*p1)[0],(*p2)[0],(*p3)[0]}) * std::min({(*p1)[1],(*p2)[1],(*p3)[1]}) * std::min({(*p1)[2],(*p2)[2],(*p3)[2]});
			}
		}
	}
	return vol;
}

template<class Point>
typename Point::value_type pair_hypervolume(const Point &p, const Point &q, int dim) {  // One could precompute the hypervolume of each point in a global array,
	// but this would introduce more cache misses so not worth it on small dimensions.
	// However, we could easily optimize away computation of vol1.
	typename Point::value_type common_vol = 1;
	typename Point::value_type vol1 = 1;
	typename Point::value_type vol2 = 1;
	for (int i = 0; i< dim; ++i) {
		common_vol *= std::min(p[i], q[i]);
		vol1 *= p[i];
		vol2 *= q[i];
	}
	return vol1 + vol2 - common_vol;
}

template<class Point>
std::pair<typename Point::value_type, std::vector<Point>> naive_hssp_k2(std::vector<Point> &inputvec, int dim) {

	typename Point::value_type hypv = std::numeric_limits<typename Point::value_type>::min();
	Point node1;
	Point node2;

	for (auto p1 = inputvec.begin(); p1 != inputvec.end(); ++p1) {
		for (auto p2 = std::next(p1,1); p2 != inputvec.end(); ++p2) {
			typename Point::value_type pair_vol = pair_hypervolume(*p1,*p2,dim);
			if (pair_vol > hypv) {
				hypv = pair_vol;
				node1 = (*p1);
				node2 = (*p2);
			}
		}
	}
	return {hypv, {node1,node2}};
}

template<class Point>
std::pair<typename Point::value_type, std::vector<Point>> naive_hssp_d3(std::vector<Point> &inputvec, int dim, int k) {
	int n = inputvec.size();
	assert(inputvec[0][1] * inputvec[0][2] < std::numeric_limits<typename Point::value_type>::max()/inputvec[0][0]);  // Otherwise risk of overflow.
	if (dim == 3 && k == 3) {
		typename Point::value_type hypv = std::numeric_limits<typename Point::value_type>::min();
		Point node1;
		Point node2;
		Point node3;

		for (auto p1 = inputvec.begin(); p1 != inputvec.end(); ++p1) {
			for (auto p2 = std::next(p1,1); p2 != inputvec.end(); ++p2) {
				for (auto p3 = std::next(p2,1); p3 != inputvec.end(); ++p3) {
					auto vol = hypervolume3points<Point>({(*p1),(*p2),(*p3)});
					if (vol > hypv) {
						hypv = vol;
						node1 = (*p1);
						node2 = (*p2);
						node3 = (*p3);
					}
				}
			}
		}
		return {hypv, {node1,node2,node3}};
	}
	else if (dim == 3 && k == n-3) {
		std::cout<<"not implemented"<<std::endl;
		return {0,{}};
	}
	else {
		std::cout<<"not implemented"<<std::endl;
		return {0,{}};
	}

}

}  // namespace utils

#endif /* NAIVE_HSSP_H_*/
