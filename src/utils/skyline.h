/*
 * skyline.h
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 *
 *      functions named ..._gener_pos assume their input points to have general position (two points cannot have same value on
 *      same coordinate)
 */


#ifndef SKYLINE_H_
#define SKYLINE_H_


// #include <iostream>
// #include <fstream>
// #include <string>
// #include <algorithm>
// #include <iterator>
// #include <set>
// #include<cmath>
#include<vector>


namespace utils {
	
// Decides if point p dominates point q.
// Returns 0 if equal, 1 if q dominates p, 2 if p dominates q, 3 if incomparable.
// Dimension is expected to be small so we do not care to stop on first witness coordinate.
template<class Point>
int d_dimensional_domination(const Point &p,const Point &q);


// Naive algorithm in $O(dn^2)$ to return the skyline of $n$ points in dimension $d$
template<class Point>
void naive_d_dimensional_skyline(std::vector<Point> &points, std::vector<Point> &skyline);


// Naive algorithm in $O(dnklog n)$ to return the skyline of $n$ points in dimension $d$.
// Corresponds more or less to algorithm 'BEST' in Godfrey et al's VLDB'06.
// Could use unordered set to get rid of log in theory but would need to define hash on vectors of dimension d (and hash only guarantees average complexity).
// Using unordered set under general position assumption a bit simpler as there is a map from coordinate 1 to point.
template<class Point>
void naive_os_d_dimensional_skyline(std::vector<Point> &points, std::vector<Point> &skyline);


// If provided with 2d points, returns skyline (maximal vectors) in $O(n\log n)$.
// As side effect will sort in place lexicographically the input vector of points by decreasing order.
template<class Point>
void two_dimensional_skyline_gener_pos(std::vector<Point> &points, std::vector<Point> &skyline);


// Appends to nondom the list of points from low that are not dominated by any point from up after projection on the d first coordinates.
// Complexity O(|low|*|up|)
template<class Point>
void naive_aux_dom_d_gp(std::vector<Point> &low, std::vector<Point> &up, std::vector<Point> &nondom, int d);

// Appends to nondom the list of points from low that are not dominated by any point from up after projection on the d first coordinates.
template<class Point>
void aux_dom_d_gp(std::vector<Point> &low, std::vector<Point> &up, std::vector<Point> &nondom, int d);


// Assumes that d is smaller than points[0].size() otherwise behaviour is undefined.
// Considers the projection of points onto the d first coordinates,
// and append the points that are maximal (i.e. whose projection belongs to skyline)
// to the skyline vector.
template<class Point>
void aux_sky_d_gp(std::vector<Point> &points, std::vector<Point> &skyline, int d);


// $O(n log^{d-2}n)$ algorithm based on Kung et al.
// As side effect will reorder the input vector of points.
template<class Point>
void d_dimensional_skyline_gener_pos(std::vector<Point> &points, std::vector<Point> &skyline);


// For 2d points in general position only.
// Appends the skyline of points to skyline.
// Optimal -- in theory.
// Divergence from theory: median algo might only guarantee average complexity (depending on library).
// As side effect will reorder the input vector.
template<class Point>
void io_two_d_gener_pos(std::vector<Point> &points, std::vector<Point> &skyline);


}  // namespace utils

// ---------------
// Implementation:
#include "skyline.hpp"

#endif /* SKYLINE_H_ */


