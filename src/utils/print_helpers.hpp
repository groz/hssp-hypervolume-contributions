/*
 * print_helpers.hpp
 *
 *  Created on: Jul 5, 2016
 *      Author: bgroz
 *
 *
 *  Implements:
 *  printing functions
 */


#ifndef PRINT_HELPERS_H_
#define PRINT_HELPERS_H_

#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace utils {




// Beware: in case of floats, the fractional part of the number may not be printed.
template<class Point>
void print_point(Point vec, std::string sep = " ");

template<class Point>
void print_pointlist(std::vector<Point> vec);

template<class Point>
std::string to_string(Point vec, std::string sep = " ", std::string init = "", std::string end = "");

template<class Point>
std::string to_string(std::vector<Point> vec);

template<class Point>
void print_list_of_vectors_to_file(std::string const infilename, const std::vector<Point> &vec, bool headerfooter, std::string sep);

template<class T>
std::string number_to_string(T number) {
	std::stringstream ss;
	ss << std::fixed << std::setprecision(2) << number;
	return ss.str();
}




// --------------------------------------------
// Implementation::
// --------------------------------------------

template<class Point>
std::string to_string(Point vec, std::string sep, std::string init, std::string end) {
	std::string res = init;
	for (const auto& s : vec){
		res += std::to_string(s)+sep;
	}
	for (size_t i = 0; i < sep.size(); ++i) {
		res.pop_back();
	}
	res += end;
	return res;
}

template<class Point>
std::string to_string(std::vector<Point> vec) {
	std::string res = "";
	for (const auto& v : vec) {
		res += "{";
		res += to_string<Point>(v,",", "(", ")");
		res += "}\n";
	}
	res += "\n";
	return res;
}

template<class Point>
void print_point(Point vec, std::string sep) {
	for (const auto& s : vec){
		std::cout << s << sep;
	}
	std::cout << std::endl;
}

template<class Point>
void print_pointlist(std::vector<Point> vec) {
	for (const auto& v_ : vec) {
		for (const auto& s : v_)
			std::cout << s << " ";
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

template<class Point>
void print_list_of_vectors_to_file(std::string const infilename, const std::vector<Point> &vec, bool headerfooter, std::string sep) {
	if( remove(infilename.c_str()) != 0 )
		std::cerr << "Error deleting file" <<std::endl;
	std::ofstream out(infilename, std::ios::app);
	if (headerfooter)
		out << "#" << std::endl;
	for (auto p : vec) {
		out << to_string<Point>(p, sep,"","") << std::endl;
	}
	if (headerfooter)
		out << "#" << std::endl;
}

// void pnl(std::string s, bool cond=true) {
// 	if (cond) {
// 		std::cout << s << std::endl;
// 	}
// }
// 
// template<typename T>
// void print_vect(std::vector<T> pg_values, std::string idstr="") {
// 	std::string s = idstr+": {";
// 	std::string sep= "";
// 	for (auto c : pg_values) {
// 		s += sep;
// 		s += std::to_string(c);
// 		sep = ",";
// 	}
// 	s += "}";
// 	std::cout << s << std::endl;
// }
//
// template<typename T, typename U>
// void print_vectpair(std::vector<std::pair<T,U>> cm, std::string idstr="") {
// 	std::string s = idstr+": {";
// 	std::string sep= "";
// 	for (auto c : cm) {
// 		s += sep;
// 		s += "(";
// 		s += std::to_string(c.first);
// 		s += ",";
// 		s += std::to_string(c.second);
// 		s += ")";
// 		sep = ",";
// 	}
// 	s += "}";
// 	std::cout << s << std::endl;
// }

}  // namespace utils

#endif /* PRINT_HELPERS_H_ */
