/*
 * Name        : skyline.cpp
 * Author      : B.Groz
 * Version     : 06/2015
 * Copyright   : none
 * Description : skyline algorithms

 * Requires C++ 11
 */

// Remark: in the complexity analyses below, n is the number of points, k the skyline cardinality, d the dimension of the points.

#include <algorithm>  // for : std::copy, std::nth, max, upper_bound.
#include <fstream>
#include <iostream>
#include <iterator>  // for io_2d_skyline: back_inserter.
#include <set>  // for d dimensional.
#include <string>
#include <vector>
//#include "ioutils.hpp"

#include<cmath>  // for dominance product.


namespace utils {
	
template<class Point>
int d_dimensional_domination(const Point &p, const Point &q) {
	int pgt = 0;;  // p is greater on at least one coord.
	int qgt = 0;  // q is greater.
	auto ps = p.size();
	if (ps == q.size()) {
		for(size_t i = 0; i < ps; i++) {
			if (p[i] < q[i]) {
				qgt = 1;
			}
			else if (p[i] > q[i]) {
				pgt = 2;
			}
		}
	}
	else {
		std::cerr << "error: domination test should not be applied to vectors of distinct size" << std::endl;
	}
	return qgt + pgt;
}


// Returns false iff p is (strictly) greater than q on at least one of d first coordinates.
// Complexity O(d).
template<class Point>
bool is_dominated_d(const Point &p, const Point &q, int d) {
	bool is_dominated = true;
	for(int i = 0 ; is_dominated && i < d ; ++i) {
		if (p[i] > q[i]) {
			is_dominated = false;
		}
	}
	return is_dominated;
}


template<class Point>
void naive_d_dimensional_skyline(std::vector<Point> &points, std::vector<Point> &skyline) {
	for (auto p : points) {
		int dominated = 0;
		for (auto q = points.begin(); q != points.end() && dominated != 1; ++q) {
			dominated = d_dimensional_domination<Point>(p, (*q));
		}
		if (dominated != 1) {
			skyline.push_back(p);
		}
	}
}

template<class Point>
void naive_os_d_dimensional_skyline(std::vector<Point> &points, std::vector<Point> &skyline) {
	std::set<Point> tempsky;
	for (auto p : points) {
		int dominated = 0;
		for (auto q = tempsky.begin(); q != tempsky.end() && dominated != 1;) {
			dominated = d_dimensional_domination<Point>(p, (*q));
			if (dominated == 2) {
				q = tempsky.erase(q);
			}
			else {
				++q;
			}
		}
		if (dominated != 1) {
			tempsky.insert(p);
		}
	}
	std::copy(tempsky.begin(), tempsky.end(), back_inserter(skyline));
}


template<class Point>
void two_dimensional_skyline_gener_pos(std::vector<Point> &points, std::vector<Point> &skyline) {
	sort(points.begin(), points.end(), std::greater<Point>());
	auto currentmax = points[0][1];
	skyline.push_back(points[0]);
	for(auto p : points) {
		if (p[1] > currentmax) {
			skyline.push_back(p);
			currentmax = p[1];
		}
	}
}


template<class Point>
struct comp_second_coord {
	bool operator() (Point p, Point q) const {
		return p[1] < q[1] ;
	}
};

template<class Point>
bool compd(const Point p, const Point q, const int d) {
	return p[d-1] > q[d-1];
}


template<class Point>
void naive_aux_dom_d_gp(std::vector<Point> &low, std::vector<Point> &up, std::vector<Point> &nondom, int d) {
	sort(low.begin(), low.end(), std::less<Point>());  // incr order.
	for (auto p : low) {
		bool is_dominated = false;
		for (auto q = up.begin(); !is_dominated && q != up.end(); ++q) {
			is_dominated = is_dominated_d<Point>(p, *q, d);
		}
		if (!is_dominated) {
			nondom.push_back(p);
		}
	}
}


template<class Point>
void aux_dom_d_gp(std::vector<Point> &low, std::vector<Point> &up, std::vector<Point> &nondom, int d) {
	if (low.size() < 2 or up.size() < 2) {
		//    if (low.size() < 10 or up.size() < 10) {
		// We have to check that there are at least 2 points: case checking points.size()<2 must be covered by naive algo,
		// but the algorithm is still correct as long as the constant above is at least 2.
		// For better performance revert to simpler algorithms whenever points.size() is not very large:
		// for instance replace "if(points.size()<2..." with  "...<10...".
		naive_aux_dom_d_gp<Point>(low, up, nondom, d);
	}
	else {
		if (d == 3) {
			sort(up.begin(), up.end(), std::greater<Point>());  // decr order.
			sort(low.begin(), low.end(), std::greater<Point>());  // decr order.
			// In the original algorithm by Kung they instead use binary insertion of lowpoints into uppoints
			// to 'partially sort' lowpoints w.r.t. uppoints.
			// Our complete sort is simpler and probably faster:
			// worst case performance remains the same. But we benefit less from small skylines.
			if (up.size() == 0) {
				nondom.insert(nondom.end(), low.begin(), low.end());
			}
			else {
				std::set<Point, comp_second_coord<Point> > tempsky;  // Maintains a 2d skyline after projecting points on dimensions 2 and 3.
				up.push_back(Point {-std::numeric_limits<typename Point::value_type>::max(), -std::numeric_limits<typename Point::value_type>::max(), -std::numeric_limits<typename Point::value_type>::max()});
				auto u = up.begin();
				auto v = low.begin();
				while (v != low.end()) {
					if(u[0] < v[0]) {  // Find out if v is dominated using tempsky.
						auto it = tempsky.upper_bound(*v);
						if (it == tempsky.end() or (*it)[2] < (*v)[2]) {  // v is not dominated in up.
							nondom.push_back(*v);
						}
						++v;
					}
					else {  // Update tempsky with u.
						auto it = tempsky.upper_bound(*u);
						if (it == tempsky.end() or (*it)[2] < (*u)[2]) {  // u is not dominated in tempsky.
							tempsky.insert(it, *u);
							--it;  // Now points to u.
							if (it != tempsky.begin()) {  // Removing points dominated by u in tempsky.
								auto itlow = it;
								--itlow;
								while(itlow != tempsky.begin() && (*itlow)[2] < (*u)[2]) {
									--itlow;
								}
								if ((*itlow)[2] > (*u)[2]) {
									itlow++;
								}
								tempsky.erase(itlow, it);
							}
						}
						++u;
					}
				}
				up.erase(up.end()-1);
			}
		}
		else {
			// Original version of Kung et al.
			sort(up.begin(), up.end(), [d](Point p, Point q){return p[d-1] > q[d-1];});  // decr order.
			sort(low.begin(), low.end(), [d](Point p, Point q){return p[d-1] > q[d-1];});  // decr order.
			// itk records First (i.e., largest) value in up that is smaller than the median of low.
			auto itk = std::upper_bound(up.begin(), up.end(), *(low.begin() + low.size() / 2), [d](Point p, Point q) {return p[d-1] > q[d-1];});
			std::vector<Point> up_up;
			std::vector<Point> up_low;
			std::vector<Point> low_up;
			std::vector<Point> low_low;
			std::copy(up.begin(), itk, back_inserter(up_up));
			std::copy(itk, up.end(), back_inserter(up_low));
			std::copy(low.begin(), low.begin() + low.size() / 2, back_inserter(low_up));
			std::copy(low.begin() + low.size() / 2, low.end(), back_inserter(low_low));
			// When ordering by coordinate d-1, low_up contains upper half of low, low_low the lower half.
			// But up_up and up_low may be unbalanced (one of them can even be empty).

			aux_dom_d_gp<Point>(low_up, up_up, nondom, d);
			std::vector<Point> res1;
			std::vector<Point> res2;
			aux_dom_d_gp<Point>(low_low, up_low, res1, d);
			aux_dom_d_gp<Point>(low_low, up_up, res2, d-1);
			// We next compute intersection of res1, res2 using sort.
			// Another possibility to avoid sorting res1, res2 would be to make sure that we sort output in all cases (d<3, ...size()<2), or compute intersection through hashing.
			sort(res1.begin(), res1.end());
			sort(res2.begin(), res2.end());
			std::set_intersection(res1.begin(), res1.end(), res2.begin(), res2.end(), back_inserter(nondom));
		}
	}
}


template<class Point>
void aux_sky_d_gp(std::vector<Point> &points, std::vector<Point> &skyline, int d) {
	if (points.size() == 1) {
		// We have to check that there are at least 2 points: case checking points.size() <2 must be covered by naive algo,
		// but the algorithm is still correct as long as the constant above is at least 2.
		// This in particular allows us not to test for border cases (empty arrays) everywhere.
		// For better performance revert to simpler algorithms whenever points.size() is not very large:
		// for instance replace "if(points.size() <2 and point.size()>0..." with  "...<10...and >0".
		skyline.push_back(points[0]);
	}
	//	else if (points.size()<50 and points.size() >1) {
	//		naive_d_dimensional_skyline(points, skyline);
	//	}
	else {
		if (points.size() > 1) {
			if (d == 2) {
				two_dimensional_skyline_gener_pos<Point>(points, skyline);
			}
			else if (d == 3){
				sort(points.begin(), points.end(), std::greater<Point>());  // decr order.
				std::set<Point, comp_second_coord<Point>> tempsky;  // Will maintain a 2d skyline by increasing order on second coordinate after projecting points on dimensions 2 and 3.
				for(auto u : points) {
					auto it = tempsky.upper_bound(u);
					if (it == tempsky.end() or (*it)[2] < u[2]) {  // u is not dominated.
						tempsky.insert(it, u);
						skyline.push_back(u);

						--it;  // now points to u.
						if (it != tempsky.begin()) {  // Removing points dominated by u in tempsky.
							auto itlow = it;
							--itlow;
							while(itlow != tempsky.begin() and (*itlow)[2] < u[2]) {
								--itlow;
							}
							if ((*itlow)[2] > u[2]) {
								itlow++;
							}
							tempsky.erase(itlow, it);
						}
					}
				}
			}
			else if (d > 3) {
				std::nth_element(points.begin(), points.begin() + points.size() / 2, points.end(), [d](Point p, Point q) {return p[d-1] < q[d-1];});
				std::vector<Point> lowpoints;
				std::vector<Point> uppoints;
				std::vector<Point> lowskyline;
				std::vector<Point> upskyline;
				std::copy(points.begin(), points.begin() + points.size()/2, back_inserter(lowpoints));
				std::copy(points.begin() + points.size()/2, points.end(), back_inserter(uppoints));
				aux_sky_d_gp<Point>(lowpoints, lowskyline, d);
				aux_sky_d_gp<Point>(uppoints, upskyline, d);
				std::copy(upskyline.begin(), upskyline.end(), back_inserter(skyline));
				aux_dom_d_gp<Point>(lowskyline, upskyline, skyline, d-1);
			}
		}
	}
}

template<class Point>
void d_dimensional_skyline_gener_pos(std::vector<Point> &points, std::vector<Point> &skyline) {
	if (points.size() > 0) {
		aux_sky_d_gp<Point>(points, skyline, points[0].size());
	}
}

template<class Point>
bool comp_points_first (Point p, Point q) {
	return p[0] < q[0] ;
}

template<class Point>
bool comp_points_second (Point p, Point q) {
	return p[1] < q[1] ;
}


template<class Point>
void io_two_d_gener_pos(std::vector<Point> &points, std::vector<Point> &skyline){
	if (points.size() == 1) {
		skyline.push_back(points[0]);
	}
	else if (points.size() > 1) {
		std::nth_element(points.begin(), points.begin() + points.size() / 2, points.end(), comp_points_first<Point>);  // Selecting median point on first coordinate.
		Point maxright = *max_element(points.begin() + points.size()/2, points.end(), comp_points_second<Point>);
		std::vector<Point> leftv;
		std::vector<Point> rightv;
		std::copy_if(points.begin(), points.begin() + points.size()/2, back_inserter(leftv), [&maxright](Point i) {return comp_points_second<Point>(maxright, i);});
		std::copy_if(points.begin() + points.size()/2, points.end(), back_inserter(rightv), [&maxright](Point i) {return comp_points_first<Point>(maxright, i);});
		io_two_d_gener_pos<Point>(leftv, skyline);
		skyline.push_back(maxright);
		io_two_d_gener_pos<Point>(rightv, skyline);
	}
}


}  // namespace utils
