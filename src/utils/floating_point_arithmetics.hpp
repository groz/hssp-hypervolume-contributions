/*
 * floating_point_arithm.h
 *
 *  Created on: Aug 11, 2016
 *      Author: bgroz
 */

#ifndef FLOATING_POINT_ARITHM_H_
#define FLOATING_POINT_ARITHM_H_


// minimal value for float calculations.
#define PREPROCESSOR_EPSILON 1e-10


#endif /* FLOATING_POINT_ARITHM_H_ */
