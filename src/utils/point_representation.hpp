#ifndef POINT_REPRESENTATION_H
#define POINT_REPRESENTATION_H


#include "utils/floating_point_arithmetics.hpp"

namespace utils{

template<typename T>
struct Point2d {
public:
	T coord0;
	T coord1;
	Point2d (): coord0(), coord1() {}
	Point2d (T c0,  T c1): coord0(c0), coord1(c1) {}
};
template<typename T>
struct PointWithId2d {
public:
	T coord0;
	T coord1;
	size_t id;
	PointWithId2d (): coord0(), coord1(), id() {}
	PointWithId2d (T c0,  T c1, size_t idx): coord0(c0), coord1(c1), id(idx) {}
};


template<typename Point>
struct PointWithId {
public:
	Point point;
	size_t id;
	PointWithId (): point(), id() {}
	PointWithId (Point p,  size_t idx): point(p), id(idx) {}
};



// Class for hull points. When we compare a point with another point we order by coord0, when we compare with a numeric value (double) we compare the value with left_slope.
// This allows to perform binary search on the hull according to either coord0 or slope.

template<typename ValueType>
struct Hull_Point_With_Slope {
public:
	ValueType coord0;
	ValueType coord1;
	size_t id;
	double left_slope;
};


struct Wrap_double {
public:
	double slope;
	Wrap_double(double s): slope(s) {}
};

template<typename ValueType>
bool operator < (const Hull_Point_With_Slope<ValueType> &lhs, const Hull_Point_With_Slope<ValueType> &rhs) {
	return lhs.coord0 < rhs.coord0;
}

template<typename ValueType>
bool operator < (const Wrap_double &lhs, const Hull_Point_With_Slope<ValueType> &rhs) {
	return lhs.slope > rhs.left_slope;
}

template<typename ValueType>
bool operator < (const Hull_Point_With_Slope<ValueType> &lhs, const Wrap_double &rhs) {
	return lhs.left_slope > rhs.slope;
}

// Only used for debugging.
template<typename ValueType>
bool operator == (const Hull_Point_With_Slope<ValueType> &lhs, const Hull_Point_With_Slope<ValueType> &rhs) {
	return (
			lhs.coord0 == rhs.coord0 && lhs.coord1 == rhs.coord1 && lhs.id == rhs.id && lhs.left_slope <= rhs.left_slope + PREPROCESSOR_EPSILON && lhs.left_slope >= rhs.left_slope - PREPROCESSOR_EPSILON
	);
}


template<typename ValueType>
std::string point_to_string (Hull_Point_With_Slope<ValueType> p) {
	return "{" + std::to_string(p.coord0) + "," + std::to_string(p.coord1) + "," + std::to_string(p.id) + "," + std::to_string(p.left_slope) + "}";
}

template<typename ValueType>
std::string to_string (std::vector<Hull_Point_With_Slope<ValueType>> v) {
	std::string s = "{\n";
	for (auto p: v) {
		s += " ";
		s += point_to_string<ValueType>(p);
		s += "\n";
	}
	s += "}";
	return s;
}



}  // namespace utils
#endif /* POINT_REPRESENTATION_H */