/*
 * hash_functions.hpp
 *  Created on: May 16, 2017
 *      Author: bgroz, smaniu
 */





#ifndef HASH_FUNCTIONS_H_
#define HASH_FUNCTIONS_H_


#include <tuple>  // for tuple
#include <utility> // for pair
#include <vector>
// #include <algorithm>
// #include <iostream>
// #include <iterator>
// #include <numeric>
// #include <set>
// #include <string>
// #include <unordered_map>
// #include <unordered_set>
#include <boost/functional/hash.hpp>



namespace std {
// extends default hash function to vector<IndexType>. IndexType may not be arbitrary, but can be long, int...
template <typename IndexType>
struct hash<std::pair<IndexType,IndexType>>
{
std::size_t operator()(const std::pair<IndexType,IndexType> &k) const
{
  return boost::hash<std::pair<IndexType,IndexType>>()(k);
}
};

template <typename IndexType>
struct hash<std::tuple<IndexType,IndexType,IndexType>>
{
std::size_t operator()(const std::tuple<IndexType,IndexType,IndexType> &k) const
{
  return boost::hash<std::tuple<IndexType,IndexType,IndexType>>()(k);
}
};

// extends default hash function to vector<IndexType>. IndexType may not be arbitrary, but can be long, int...
template <typename IndexType>
struct hash<std::vector<IndexType>>
{
  std::size_t operator()(const std::vector<IndexType> &k) const
  {
	return boost::hash<std::vector<IndexType>>()(k);
  }
};

}  // namespace std


#endif /* HASH_FUNCTIONS_H_ */

