/*
 * parsers.hpp
 *
 *  Created on: Jul 5, 2016
 *      Author: bgroz
 *
 *
 *  Implements:
 *  parsers functions
 */


#ifndef PARSERS_H_
#define PARSERS_H_



#include <array>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <boost/tokenizer.hpp>

namespace utils {


template<class Point>
void parse_file_to_list_of_vectors(std::string const infilename, std::vector<Point> &vec);

template<class ArrayPoint>
void parse_file_to_list_of_arrays_size4(std::string const infilename, std::vector<ArrayPoint> &vec);

template<class ArrayPoint>
void parse_file_to_list_of_arrays_size3(std::string const infilename, std::vector<ArrayPoint> &vec);

template<class ArrayPoint>
void parse_file_to_list_of_arrays_size2(std::string const infilename, std::vector<ArrayPoint> &vec);



// --------------------------------------------
// Implementation:
// --------------------------------------------


template<class Point>
void parse_file_to_list_of_vectors(std::string const infilename, std::vector<Point> &vec) {
	// Points should be vectors
	std::ifstream in(infilename.c_str());
	if (in) {
		std::string line;

		typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
		boost::char_separator<char> sep("\t");

		if (std::is_same<double, typename Point::value_type>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stod(t));
				}
				vec.push_back(aux);
			}
		}
		else if (std::is_same<float, typename Point::value_type>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				vec.push_back(aux);
			}
		}
		else if (std::is_same<long, typename Point::value_type>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stol(t));
				}
				vec.push_back(aux);
			}
		}
		else if (std::is_same<long long, typename Point::value_type>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoull(t));
				}
				vec.push_back(aux);
			}
		}
		else if (std::is_same<int, typename Point::value_type>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				vec.push_back(aux);
			}
		}
		else {
			std::cerr << "Input type not supported for parsing file."<< std::endl;
		}
	}
	else {
		std::cerr << "incorrect input file:" << infilename.c_str() << std::endl;
	}
}

template<class ArrayPoint>
void parse_file_to_list_of_arrays_size4(std::string const infilename, std::vector<ArrayPoint> &vec) {
	typedef typename ArrayPoint::value_type T;
	typedef std::vector<T> Point;
	// Points should be arrays of size 4
	// T should be value_type of Point.
	std::ifstream in(infilename.c_str());
	if (in) {
		std::string line;

		typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
		boost::char_separator<char> sep("\t");

		if (std::is_same<double, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stod(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2], aux[3]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<float, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2], aux[3]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<long, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stol(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2], aux[3]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<long long, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoull(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2], aux[3]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<int, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2], aux[3]}};
				vec.push_back(auxpoint);
			}
		}
		else {
			std::cerr << "Input type not supported for parsing file."<< std::endl;
		}
	}
	else {
		std::cerr << "incorrect input file:" << infilename.c_str() << std::endl;
	}
}

template<class ArrayPoint>
void parse_file_to_list_of_arrays_size3(std::string const infilename, std::vector<ArrayPoint> &vec) {
	typedef typename ArrayPoint::value_type T;
	typedef std::vector<T> Point;
	// Points should be arrays of size 3
	// T should be value_type of Point.
	std::ifstream in(infilename.c_str());
	if (in) {
		std::string line;

		typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
		boost::char_separator<char> sep("\t");

		if (std::is_same<double, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stod(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<float, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<long, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stol(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<long long, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoull(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<int, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1], aux[2]}};
				vec.push_back(auxpoint);
			}
		}
		else {
			std::cerr << "Input type not supported for parsing file."<< std::endl;
		}
	}
	else {
		std::cerr << "incorrect input file:" << infilename.c_str() << std::endl;
	}
}


template<class ArrayPoint>
void parse_file_to_list_of_arrays_size2(std::string const infilename, std::vector<ArrayPoint> &vec) {
	typedef typename ArrayPoint::value_type T;
	typedef std::vector<T> Point;
	// Points should be arrays of size 2
	// T should be value_type of Point.
	std::ifstream in(infilename.c_str());
	if (in) {
		std::string line;

		typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
		boost::char_separator<char> sep("\t");

		if (std::is_same<double, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stod(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<float, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<long, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stol(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<long long, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoull(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1]}};
				vec.push_back(auxpoint);
			}
		}
		else if (std::is_same<int, T>::value) {
			while (getline(in,line)) {
				Tokenizer tok(line,sep);
				Point aux;
				for (const auto& t : tok) {
					aux.push_back(std::stoi(t));
				}
				ArrayPoint auxpoint = {{aux[0], aux[1]}};
				vec.push_back(auxpoint);
			}
		}
		else {
			std::cerr << "Input type not supported for parsing file."<< std::endl;
		}
	}
	else {
		std::cerr << "incorrect input file:" << infilename.c_str() << std::endl;
	}
}

}  // namespace utils

#endif /* PARSERS_H_ */
