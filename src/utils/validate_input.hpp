/*
 * validate_input.hpp
 *
 *  Created on: Mar 3, 2017
 *      Author: bgroz
 */

#ifndef VALIDATE_INPUT_H_
#define VALIDATE_INPUT_H_

#include <algorithm>  // for max, is_sorted...
#include <limits>
#include <string>
#include <utility>  // for pair
#include <vector>

namespace utils {
	
// Returns true if input is valid. A valid input may still cause hssp2dExtreme or hssp2dSmawk to run out of memory, of course.
template<typename Point, typename Index_type>
std::pair<bool, std::string> hssp2d_validate_input(const std::vector<Point> &points, const std::vector<Index_type> &result_set_indexes, const Index_type k) {
	typedef typename Point::value_type T;

	bool is_valid = true;
	std::string error_message = "";
	Index_type n = points.size();
	if (n == 0) { error_message += "-- input should contain at least one point "; is_valid = false; } // Getting rid of an absurd case.
	if (n < k) { error_message += "-- input should contain more points than objective "; is_valid = false; } // Getting rid of an absurd case.
	if (k <= 0) { error_message += "-- objective should contain at least one point "; is_valid = false; } // Getting rid of an absurd case.
	if (is_valid) {  // skipping further test in case invalidity has been established above. Otherwise we would need to deal with divisions by 0.
		std::vector<std::tuple<T, T, Index_type>> vec;
		T max_coord0 = 1;
		T max_coord1 = 1;
		for (auto p : points) {
			max_coord0 = std::max(max_coord0, p[0]);
			max_coord1 = std::max(max_coord1, p[1]);
		}

		if (max_coord0 >= std::numeric_limits<T>::max()/max_coord1) { error_message += "-- coordinates too large for data type "; is_valid = false; }  // Otherwise risk of overflow.
		if (k >= std::numeric_limits<Index_type>::max()/(n-k+1)) { error_message += "-- too many points for Index_type "; is_valid = false; }  // Otherwise risk of overflow.
		if (k >= static_cast<Index_type>(result_set_indexes.max_size()) - static_cast<Index_type>(result_set_indexes.size())) { error_message += "-- output size too large "; is_valid = false; }  // Otherwise risk of overflow.
		if (k >= static_cast<Index_type>(vec.max_size())/(n-k+1)) { error_message += "-- output size too large "; is_valid = false; }  // Otherwise risk of overflow.
		if (!std::is_sorted(points.begin(), points.end(), [](Point p, Point q){ return p[0] <= q[0]; })) { error_message += "-- input should be sorted (on x) "; is_valid = false; }
		if (!std::is_sorted(points.begin(), points.end(), [](Point p, Point q){ return p[1] >= q[1]; })) { error_message += "-- input should be sorted (on y) "; is_valid = false; }
	}
	return std::make_pair(is_valid, error_message);
}

template<typename Point>
std::pair<bool, std::string> hssp3dk2_validate_input(const std::vector<Point> &points) {
	typedef typename Point::value_type T;

	bool is_valid = true;
	std::string error_message = "";
	size_t n = points.size();
	if (n <= 1) { error_message += "-- input should contain at least two point "; is_valid = false; } // Getting rid of an absurd case.
	if (is_valid) {  // skipping further test in case invalidity has been established above. Otherwise we would need to deal with divisions by 0.
		T max_coord0 = 1;
		T max_coord1 = 1;
		T max_coord2 = 1;
		for (auto p : points) {
			max_coord0 = std::max(max_coord0, p[0]);
			max_coord1 = std::max(max_coord1, p[1]);
			max_coord2 = std::max(max_coord1, p[2]);
		}

		if (max_coord0 >= (std::numeric_limits<T>::max()/max_coord1)/max_coord2) { error_message += "-- coordinates too large for data type "; is_valid = false; }  // Otherwise risk of overflow.
	}
	return std::make_pair(is_valid, error_message);
}


template<typename Point, typename Index_type>
std::pair<bool, std::string> hssp3dk2_validate_input(const std::vector<Point> &points) {
	typedef typename Point::value_type T;

	bool is_valid = true;
	std::string error_message = "";
	Index_type n = static_cast<Index_type>(points.size());
	if (n <= 1) { error_message += "-- input should contain at least two point "; is_valid = false; } // Getting rid of an absurd case.
	if (is_valid) {  // skipping further test in case invalidity has been established above. Otherwise we would need to deal with divisions by 0.
		std::vector<std::tuple<T, T, Index_type>> vec;
		T max_coord0 = 1;
		T max_coord1 = 1;
		T max_coord2 = 1;
		for (auto p : points) {
			max_coord0 = std::max(max_coord0, p[0]);
			max_coord1 = std::max(max_coord1, p[1]);
			max_coord2 = std::max(max_coord1, p[2]);
		}

		if (max_coord0 >= (std::numeric_limits<T>::max()/max_coord1)/max_coord2) { error_message += "-- coordinates too large for data type "; is_valid = false; }  // Otherwise risk of overflow.
		if (points.size() >= std::numeric_limits<Index_type>::max()) { error_message += "-- too many points for Index_type "; is_valid = false; }  // Otherwise risk of overflow.
	}
	return std::make_pair(is_valid, error_message);
}




template<typename Point, typename Index_type>
std::pair<bool, std::string> hssp4dk2_validate_input(const std::vector<Point> &points) {
	typedef typename Point::value_type T;

	bool is_valid = true;
	std::string error_message = "";
	Index_type n = points.size();
	if (n <= 1) { error_message += "-- input should contain at least two point "; is_valid = false; } // Getting rid of an absurd case.
	if (is_valid) {  // skipping further test in case invalidity has been established above. Otherwise we would need to deal with divisions by 0.
		std::vector<std::tuple<T, T, Index_type>> vec;
		T max_coord0 = 1;
		T max_coord1 = 1;
		T max_coord2 = 1;
		T max_coord3 = 1;
		for (auto p : points) {
			max_coord0 = std::max(max_coord0, p[0]);
			max_coord1 = std::max(max_coord1, p[1]);
			max_coord2 = std::max(max_coord1, p[2]);
			max_coord3 = std::max(max_coord1, p[3]);
		}

		if (max_coord0 >= ((std::numeric_limits<T>::max()/max_coord1)/max_coord2)/max_coord3) { error_message += "-- coordinates too large for data type "; is_valid = false; }  // Otherwise risk of overflow.
		if (points.size() >= std::numeric_limits<Index_type>::max()) { error_message += "-- too many points for Index_type "; is_valid = false; }  // Otherwise risk of overflow.
	}
	return std::make_pair(is_valid, error_message);
}

}  // namespace utils
#endif /* VALIDATE_INPUT_H_ */
