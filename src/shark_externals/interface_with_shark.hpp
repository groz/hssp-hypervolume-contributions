/*
 * interface_with_shark.hpp
 *
 *  Created on: Mar 4, 2017
 *      Author: bgroz
 *
 */

#ifndef INTERFACE_SHARK_H_
#define INTERFACE_SHARK_H_

#include <array>
#include <iterator>
#include <string>
#include <vector>

namespace shark_interface {
	
class VectorType2d {
private:
    double x_;
    double y_;
public:
    VectorType2d(double x, double y):
    x_(x), y_(y) { }
    int size() const {return  2;}
    double operator() (int i) const {
        if (i == 0) {
            return x_;
        }
        else if (i == 1) {
            return y_;
        }
        else {
            std::cerr<<"invalid access"<<std::endl;
            return -1;
        }
    }
};

class VectorType3d {
private:
    double x_;
    double y_;
    double z_;
public:
    VectorType3d(double x, double y, double z):
    x_(x), y_(y), z_(z) { }
    int size() const {return  3;}
	bool operator<(VectorType3d const& rhs) const{//sort by x coordinate
		return x_ < rhs.x_;
	}
    double operator() (int i) const {
        if (i == 0) {
            return x_;
        }
        else if (i == 1) {
            return y_;
        }
        else if (i == 2) {
            return z_;
        }
        else {
            std::cerr<<"invalid access"<<std::endl;
            return -1;
        }
    }
};

template<typename Point>
std::vector<VectorType2d> convert_vec_to_VectorType2d(std::vector<Point> points) {
	std::vector<VectorType2d> res;
	std::transform(points.begin(), points.end(), std::back_inserter(res), [] (Point p) { VectorType2d v(p[0], p[1]); return v;});
	return res;
}

template<typename Point>
std::vector<VectorType3d> convert_vec_to_VectorType3d(std::vector<Point> points) {
	std::vector<VectorType3d> res;
	std::transform(points.begin(), points.end(), std::back_inserter(res), [] (Point p) { VectorType3d v(p[0], p[1], p[2]); return v;});
	return res;
}

std::string to_string(VectorType2d v) {
  return "("+std::to_string(v(0))+","+std::to_string(v(1))+")";
}

std::string to_string(std::vector<VectorType2d> vec) {
  std::string s;
  for (auto v : vec) {
    s += "{" + to_string(v) + "}\n";
  }
  return s;
}

std::string to_string(VectorType3d v) {
  return "("+std::to_string(v(0))+","+std::to_string(v(1))+","+std::to_string(v(2))+")";
}

std::string to_string(std::vector<VectorType3d> vec) {
  std::string s;
  for (auto v : vec) {
    s += "{" + to_string(v) + "}\n";
  }
  return s;
}

}  // namespace shark_interface
#endif /* INTERFACE_SHARK_H_ */
