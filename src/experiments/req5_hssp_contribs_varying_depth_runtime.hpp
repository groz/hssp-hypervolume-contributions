/*
 * main.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 */

#ifndef REQ5_VAR_DEPTH_H_
#define REQ5_VAR_DEPTH_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
// #include <set>
#include <sstream>
#include <string>
#include <utility>

#include "hssp_contribution/hypvcontributions.h"
#include "hssp2d/contrib_matrix.hpp"
#include "utils/generators.hpp"
#include "utils/parsers.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"


//
// Output is a string with :
// - nbpoint,
// - seed,
// - repet,
// - contribdepth,
// - nane_of_algo,
// - datetime,
// - duration
namespace experiment {


	
template<typename Point, typename IndexType>
void print_running_time_contribs_2(std::vector<Point> skyvec, int seed, int nbpoint, int repet, 
int contribdepth, std::string outfilename, std::string headersuffix) {
	typedef typename Point::value_type T;
	typedef typename hssp_contribution::ContribPoint<T> RCell;
	typedef typename std::unordered_map<std::pair<T,T>,RCell> ContainerOutput;

	auto valid = utils::hssp3dk2_validate_input<Point, IndexType>(skyvec);
	if (valid.first) {
		std::string output;
		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(contribdepth) + "," + std::to_string(seed) + "," 
			+ std::to_string(repet) + ",hypvcontributions," + headersuffix;
		output = headerstring + "," + utils::current_time();

		auto t1 = utils::current_clock();
		auto res = hssp_contribution::hypervolume_contributions<Point, ContainerOutput>(contribdepth, skyvec);
		auto t2 = utils::current_clock();
		output += "," + utils::current_time();
		output += "," + utils::delay_between(t1, t2);

		// ContainerOutput contribs_v = res.second;
		// std::cout<<contribs_v.size()<<std::endl;
		// std::cout<<contribs_v.bucket_count()<<std::endl;
		// std::cout<<contribs_v.max_bucket_count()<<std::endl;

		std::ofstream out(outfilename, std::ios::app);
		if (out) {
			out<< output<<std::endl;
			out.close();
		}
		else {
			std::cout<<"Pb: writing in file impossible";
			std::cout<<output;
		}
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<",seed:"<<seed<<",nbpoint:"<<nbpoint<<std::endl;
	}
}

void running_time_hsspd3_contrib_varying_depth() {
	typedef double T;
	typedef int IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints_contribs_vard.txt");
	std::string const outfilename("results_contribs_vard.csv");
	int dim = 3;
	
	int repet_max = 1;
	int seed_max = 3;
	
	for (int nbpoint : {100, 200, 500, 1000, 10000}) {
		std::vector<int> list_of_depth;
		int max_depth = nbpoint;
		if (nbpoint > 1e3) max_depth = 100;
		for (int depth = 1; depth < 10; depth +=1) {
			list_of_depth.push_back(depth);
		}
		for (int depth = 10; depth <= max_depth; depth +=10) {
			list_of_depth.push_back(depth);
		}
		
		for (int contribdepth : list_of_depth) {
			for (int seed = 1; seed <= seed_max; ++seed) {
				for (std::string distrib : {"distr_hyperplane"}) {
				// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
					Container skyvec;
					if (distrib == "distr_hyperplane") {
						utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
					}
					if (distrib == "distr_convex") {
						utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					if (distrib == "distr_concave") {
						utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					utils::parse_file_to_list_of_arrays_size3(infilename, skyvec);

					for (int repet = 1; repet <= repet_max; ++ repet) {
						print_running_time_contribs_2<Point, IndexType>(skyvec, seed, nbpoint, repet, contribdepth, outfilename, distrib);
					}
				}
			}
		}
	}
}

}  // namespace experiment

#endif /* REQ5_VAR_DEPTH__H_*/