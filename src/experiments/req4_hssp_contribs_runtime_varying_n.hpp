/*
 * main.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 */

#ifndef REQ4_VAR_NBPOINT_H_
#define REQ4_VAR_NBPOINT_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
// #include <set>
#include <sstream>
#include <string>
#include <utility>

#include "hssp_contribution/hypvcontributions.h"
#include "hssp2d/contrib_matrix.hpp"
#include "shark_externals/interface_with_shark.hpp"
#include "shark_externals/sharklibrary_contributions.hpp"
#include "utils/generators.hpp"
#include "utils/parsers.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"



namespace experiment {

template<typename Point, typename IndexType>
void print_running_time_contribs_shark(int seed, int nbpoint, int repet, std::string outfilename, std::string headersuffix, 
std::vector<shark_interface::VectorType3d> vect_shark, shark_interface::VectorType3d refpoint_shark) {

	std::string output;
	std::string headerstring = std::to_string(nbpoint) + ",1," + std::to_string(seed) + "," 
		+ std::to_string(repet) + ",hypvcontributions_shark," + headersuffix;
	output = headerstring + "," + utils::current_time();

	auto t1 = utils::current_clock();
	shark::HypervolumeContribution3D c;
	std::vector<shark::KeyValuePair<double,std::size_t> > res = c.smallest(vect_shark, nbpoint, refpoint_shark);
	auto t2 = utils::current_clock();
	output += "," + utils::current_time();
	output += "," + utils::delay_between(t1, t2);

	// ContainerOutput contribs_v = res.second;
	// std::cout<<contribs_v.size()<<std::endl;
	// std::cout<<contribs_v.bucket_count()<<std::endl;
	// std::cout<<contribs_v.max_bucket_count()<<std::endl;

	std::ofstream out(outfilename, std::ios::app);
	if (out) {
		out<< output<<std::endl;
		out.close();
	}
	else {
		std::cout<<"Pb: writing in file impossible";
		std::cout<<output;
	}
}
	
template<typename Point, typename IndexType>
void print_running_time_contribs(std::vector<Point> skyvec, int seed, int nbpoint, int repet, 
int contribdepth, std::string outfilename, std::string headersuffix) {
	typedef typename Point::value_type T;
	typedef typename hssp_contribution::ContribPoint<T> RCell;
	typedef typename std::unordered_map<std::pair<T,T>,RCell> ContainerOutput;

	auto valid = utils::hssp3dk2_validate_input<Point, IndexType>(skyvec);
	if (valid.first) {
		std::string output;
		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(contribdepth) + "," + std::to_string(seed) + "," 
			+ std::to_string(repet) + ",hypvcontributions," + headersuffix;
		output = headerstring + "," + utils::current_time();

		auto t1 = utils::current_clock();
		auto res = hssp_contribution::hypervolume_contributions<Point, ContainerOutput>(contribdepth, skyvec);
		auto t2 = utils::current_clock();
		output += "," + utils::current_time();
		output += "," + utils::delay_between(t1, t2);

		std::ofstream out(outfilename, std::ios::app);
		if (out) {
			out<< output<<std::endl;
			out.close();
		}
		else {
			std::cout<<"Pb: writing in file impossible";
			std::cout<<output;
		}
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<",seed:"<<seed<<",nbpoint:"<<nbpoint<<std::endl;
	}
}

void running_time_hsspd3_contrib_varying_n_d() {
	typedef double T;
	typedef int IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints_contribs_varn.txt");
	std::string const outfilename("results_contribs_varn.csv");
	int dim = 3;
	
	int nbpoint_max = 1e7;
	int repet_max = 1;
	int seed_max = 3;
	
	for (int nbpoint = 100; nbpoint <= nbpoint_max; nbpoint *= 2) {
		std::vector<int> list_of_depth;
		if (nbpoint <= 1e6) {
			list_of_depth = {1,2,10};
		}
		else {
			list_of_depth = {1,2};
		}
		for (int contribdepth : list_of_depth) {
			for (int seed = 1; seed <= seed_max; ++seed) {
				for (std::string distrib : {"distr_hyperplane"}) {
				// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
					Container skyvec;
					if (distrib == "distr_hyperplane") {
						utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
					}
					if (distrib == "distr_convex") {
						utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					if (distrib == "distr_concave") {
						utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					utils::parse_file_to_list_of_arrays_size3(infilename, skyvec);
					Point refpoint;
					Container skyvec_minimization;
					utils::convert_maximization_to_positive_minimization_with_reference_array3d(skyvec, skyvec_minimization, refpoint);
					std::vector<shark_interface::VectorType3d> vect_shark = shark_interface::convert_vec_to_VectorType3d<Point>(skyvec_minimization);
					shark_interface::VectorType3d refpoint_shark(refpoint[0], refpoint[1], refpoint[2]);

					for (int repet = 1; repet <= repet_max; ++ repet) {
						print_running_time_contribs<Point, IndexType>(skyvec, seed, nbpoint, repet, contribdepth, outfilename, distrib);
						if (contribdepth == 1) print_running_time_contribs_shark<Point, IndexType>(seed, nbpoint, repet, outfilename, distrib, vect_shark, refpoint_shark);
					}
				}
			}
		}
	}
}

}  // namespace experiment

#endif /* REQ4_VAR_NBPOINT_H_*/