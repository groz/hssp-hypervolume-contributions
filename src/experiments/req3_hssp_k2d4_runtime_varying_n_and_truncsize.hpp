/*
* hsspk2d4
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef REQ3_VAR_NBPOINT_H_
#define REQ3_VAR_NBPOINT_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>


#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <set>
#include <sstream>
#include <string>

#include "hssp2d/contrib_matrix.hpp"

#include "hull/upperhull_updatable.hpp"
#include "hull/upperhull_updatable_double.hpp"

#include "utils/generators.hpp"
#include "utils/naive_implementations/naive_hssp.h"
#include "utils/parsers.hpp"
#include "utils/print_helpers.hpp"
#include "utils/skyline.h"
#include "utils/validate_input.hpp"
#include "utils/time_utils.hpp"

#include "rangetree/range_struct_trunc_array.hpp"
#include "rangetree/range_struct_trunc_array_inc.hpp"


//
// Output is a string with :
// - nbpoint,
// - dim,
// - truncsize
// - seed,
// - datetime,
// - repet,
// - duration of the chosen algo,
namespace experiment {

template<typename Point>
void rangetree_d4_comparison2(std::vector<Point> skyvec, int seed, int nbpoint, int repet, 
int truncsize, std::string outfilename, int dim, std::string algo_name, std::string headersuffix) {
	typedef typename Point::value_type T;
	typedef std::vector<Point> Container;
// NB is in fact independent from dimension (is the same as function for d3, but we use only rangestruct , not rlower nor rlowerinc that make no sense for dimension 4 so we comment it out)
	auto valid = utils::hssp3dk2_validate_input<Point>(skyvec);
	if (valid.first) {
		std::string output;
		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(dim) + "," + std::to_string(truncsize) 
			+ "," + std::to_string(seed) + "," + std::to_string(repet) + "," + algo_name  + "," + headersuffix;
		output = headerstring + "," + utils::current_time();

		auto t1 = utils::current_clock();
		if (algo_name == "naive") {
			std::pair<T, std::vector<Point>> res_naive = utils::naive_hssp_k2<Point>(skyvec, dim);
		}
		else if (algo_name == "rtree") {
			std::tuple<Point, Point, T> res_struct = rangetree::range_struct<Point, Container>(skyvec.begin(), skyvec.end(), dim, truncsize);
		}
		else if (algo_name == "rtreeinc") {
			std::tuple<Point, Point, T> res_struct_inc = rangetree::range_struct_inc<Point, Container>(skyvec.begin(), skyvec.end(), dim, truncsize);
		}
		// else if (algo_name == "rlowerinc") {
		// 	std::tuple<Point, Point, T> res_inc = rangetree::range_struct_lower_inc<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
		// }
		// else if (algo_name == "rlowerinc2") {
		// 	std::tuple<Point, Point, T> res_inc = rangetree2::range_struct_lower_inc<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
		// }
		auto t2 = utils::current_clock();
		output += "," + utils::current_time();
		output += "," + utils::delay_between(t1, t2);
		std::ofstream out(outfilename, std::ios::app);
		if (out) {
			out << output << std::endl;
			out.close();
		}
		else {
			std::cout<<"Pb: writing in file impossible";
			std::cout<<output;
		}
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<", seed:"<<seed<<", nbpoint:"<<nbpoint<<std::endl;
	}
}



void running_time_hsspk2d4_range_varying_n_and_truncsize() { // The only difference with running_time_hsspk2d3_range() are the parameters. Also, rlowerinc is specific to dimension 3 and therefore cannot be used for dimension 4.
	typedef double T;
	typedef std::array<T, 4> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints_hsspk2d4_varn.txt");
	std::string const outfilename("results_hsspk2d4_varn.csv");
	int dim = 4;
	
	// int nbpoint_max = 1e6;
	// int trunc_max = 1e3;
	// int repet_max = 1;
	// int seed_max = 1;
	//
	// for (int truncsize = trunc_max; truncsize <= trunc_max; truncsize *= 10) {
	int nbpoint_max = 1e6;
	int trunc_max = 1e4;
	int repet_max = 1;
	int seed_max = 3;
	
	for (int truncsize = 10; truncsize <=trunc_max; truncsize *= 10) {
		for (int nbpoint = 2; nbpoint <= nbpoint_max; nbpoint *= 2) {
			for (int seed = 1; seed <= seed_max; ++seed) {
				for (std::string distrib : {"distr_hyperplane"}) {
				// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
					Container skyvec;
					if (distrib == "distr_hyperplane") {
						utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
					}
					if (distrib == "distr_convex") {
						utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					if (distrib == "distr_concave") {
						utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					utils::parse_file_to_list_of_arrays_size4(infilename, skyvec);
					
					for (int repet = 1; repet <= repet_max; ++ repet) {
						rangetree_d4_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "naive", distrib);
						rangetree_d4_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rtree", distrib);
						rangetree_d4_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rtreeinc", distrib);
					}
				}
			}
		}
	}
}

}  // namespace experiment

#endif /* REQ3_VAR_NBPOINT_H_*/