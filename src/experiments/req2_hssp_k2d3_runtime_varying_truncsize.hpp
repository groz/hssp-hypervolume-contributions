/*
* hsspk2d3... 
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef REQ2_VAR_TRUNCSIZE_H_
#define REQ2_VAR_TRUNCSIZE_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
// #include <set>
#include <sstream>
#include <string>

#include "utils/generators.hpp"
#include "utils/naive_implementations/naive_hssp.h"
#include "utils/parsers.hpp"
#include "utils/print_helpers.hpp"
#include "utils/skyline.h"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"

#include "rangetree/range_struct_trunc_array.hpp"
#include "rangetree/range_struct_trunc_array_inc.hpp"
#include "rangetree/range_struct_trunc_array_lower.hpp"
#include "rangetree/range_struct_trunc_array_lower_incremental.hpp"
#include "rangetree/range_struct_trunc_array_lower_incremental2.hpp"


namespace experiment {

template<typename Point>
void rangetree_d3_comparison(std::vector<Point> skyvec, int seed, int nbpoint, int repet, 
int truncsize, std::string outfilename, int dim, std::string algo_name, std::string headersuffix) {
	typedef std::vector<Point> Container;
	// NB is in fact independent from dimension (is the same as function for d4)
	auto valid = utils::hssp3dk2_validate_input<Point>(skyvec);
	if (valid.first) {
		std::string output;
		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(dim) + "," + std::to_string(truncsize) 
			+ "," + std::to_string(seed) + "," + std::to_string(repet) + "," + algo_name  + "," + headersuffix;
		output = headerstring + "," + utils::current_time();

		auto t1 = utils::current_clock();
		if (algo_name == "naive") {
			utils::naive_hssp_k2<Point>(skyvec, dim);
		}
		else if (algo_name == "rlower") {
			rangetree::range_struct_lower<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
		}
		else if (algo_name == "rlowerinc") {
			rangetree::range_struct_lower_inc<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
		}
		else if (algo_name == "rlowerinc2") {
			rangetree2::range_struct_lower_inc<Point, Container>(skyvec.begin(), skyvec.end(), truncsize);
		}
		else if (algo_name == "rstruct") {
			rangetree::range_struct<Point, Container>(skyvec.begin(), skyvec.end(), dim, truncsize);
		}
		else if (algo_name == "rstructinc") {
			rangetree::range_struct_inc<Point, Container>(skyvec.begin(), skyvec.end(), dim, truncsize);
		}
		auto t2 = utils::current_clock();
		output += "," + utils::current_time();
		output += "," + utils::delay_between(t1, t2);

		std::ofstream out(outfilename, std::ios::app);
		if (out) {
			out << output << std::endl;
			out.close();
		}
		else {
			std::cout<<"Pb: writing in file impossible";
			std::cout<<output;
		}
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<", seed:"<<seed<<", nbpoint:"<<nbpoint<<std::endl;
	}
}

void running_time_hsspk2d3_varying_truncsize() {
	typedef double T;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints_hsspk2d3_vartrunc.txt");
	std::string const outfilename("results_hsspk2d3_vartrunc.csv");
	int dim = 3;

	int nbpoint_max = 1e6;
	int trunc_max = 1e4;
	int repet_max = 1;
	int seed_max = 3;

	
	for (int truncsize = 1; truncsize <= trunc_max; truncsize *= 2) {
		for (int nbpoint = 1e5; nbpoint <= nbpoint_max; nbpoint *= 10) {
			for (int seed = 1; seed <= seed_max; ++seed) {
				for (std::string distrib : {"distr_hyperplane"}) {
				// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
					Container skyvec;
					if (distrib == "distr_hyperplane") {
						utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
					}
					if (distrib == "distr_convex") {
						utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					if (distrib == "distr_concave") {
						utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					utils::parse_file_to_list_of_arrays_size3(infilename, skyvec);

					for (int repet = 1; repet <= repet_max; ++ repet) {
						rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rlowerinc", distrib);
						rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rlowerinc2", distrib);
						rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rlower", distrib);
						rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rstructinc", distrib);
						rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "rstruct", distrib);
						// if (nbpoint<1e5 && truncsize == 1) rangetree_d3_comparison<Point>(skyvec, seed, nbpoint, repet, truncsize, outfilename, dim, "naive", distrib);
					}
				}
			}
		}
	}
}

}  // namespace experiment

#endif /* REQ2_VAR_TRUNCSIZE_H_*/