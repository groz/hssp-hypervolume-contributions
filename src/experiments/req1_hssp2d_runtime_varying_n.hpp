/*
 * main.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 */

#ifndef REQ1_VAR_NBPOINT_H_
#define REQ1_VAR_NBPOINT_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
// #include <set>
#include <sstream>
#include <string>

#include "hssp2d/contrib_matrix.hpp"
#include "hssp2d/hssp2d_extreme.hpp"
#include "hssp2d/hssp2d_smawk.hpp"
#include "shark_externals/interface_with_shark.hpp"
#include "shark_externals/sharklibrary_hssp2d.cpp"
#include "utils/generators.hpp"
#include "utils/other_utils.hpp"
#include "utils/parsers.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"


namespace experiment {

template<typename Point>
void print_running_time_hssp2d_shark_b(int k, int nbpoint, int seed, int repet, std::string outfilename, std::vector<shark_interface::VectorType2d> vect_shark, shark_interface::VectorType2d refpoint_shark, std::string headersuffix) {
	std::string algo_name = "shark";
	std::string output;
	std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(k) + "," + std::to_string(seed) + "," + std::to_string(repet) + "," + algo_name  + "," + headersuffix;
	output = headerstring + "," + utils::current_time();

	auto shark_instance = shark::HypervolumeSubsetSelection2D();
	std::vector<bool> selected(vect_shark.size(), false);

	auto t1 = utils::current_clock();
	shark_instance(vect_shark, selected, k, refpoint_shark);
	auto t2 = utils::current_clock();
	output += "," + utils::current_time();
	output += "," + utils::delay_between(t1, t2);

	std::ofstream out(outfilename, std::ios::app);
	if (out) {
		out<< output<<std::endl;
		out.close();
	}
	else {
		std::cout<<"Pb: writing in file impossible";
		std::cout<<output;
	}
}

template<typename Point>
void print_running_time_hssp2d_b(int k, std::vector<Point> skyvec, int nbpoint, int seed, int repet, std::string outfilename, std::string algo_name, std::string headersuffix) {
	std::vector<std::size_t> result_indexes({});
	auto valid = utils::hssp2d_validate_input<Point, std::size_t>(skyvec, result_indexes, k);
	if (valid.first) {
		std::string output;
		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(k) + "," + std::to_string(seed) + "," + std::to_string(repet) + "," + algo_name  + "," + headersuffix;
		output = headerstring + "," + utils::current_time();
		
		auto t1 = utils::current_clock();
		if (algo_name == "2d_extreme_vo") {
			hssp2d::hssp2dextremeValueOnly<Point>(skyvec, k);
		}
		else if (algo_name == "2d_extreme") {
			hssp2d::hssp2dextreme<Point>(skyvec, result_indexes, k);
		}
		else if (algo_name == "2d_smawk") {
			hssp2d::hssp2dsmawk<Point>(skyvec, result_indexes, k);
		}
		else if (algo_name == "2d_smawk_vo") {
			hssp2d::hssp2dsmawkValueOnly<Point>(skyvec, k);
		}
		auto t2 = utils::current_clock();
		output += "," + utils::current_time();
		output += "," + utils::delay_between(t1, t2);

		std::ofstream out(outfilename, std::ios::app);
		if (out) {
			out << output << std::endl;
			out.close();
		}
		else {
			std::cout<<"Pb: writing in file impossible";
			std::cout<<output;
		}
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<", k:"<<k<<", seed:"<<seed<<", nbpoint:"<<nbpoint<<std::endl;
	}
}

void running_time_hssp2d_varying_n() {
	typedef double T;
	typedef std::array<T, 2> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints_hssp2d_varn.txt");
	std::string const outfilename("results_hssp2d_varn.csv");
	int dim = 2;

	int repet_max = 1;
	int seed_max = 3;

	for (int nbpoint : {1e2,5e2,1e3,5e3,1e4,2e4,3e4,5e4,6e4,7e4,8e4,9e4,1e5}) {
	// for (int nbpoint = 100; nbpoint <= nbpoint_max; nbpoint *= 2) {
		// Genereating list of argument k relevant for this nb of point
		std::vector<int> list_of_k;
		list_of_k.push_back(nbpoint/2);

		for (int seed = 1; seed <= seed_max; ++seed) {
			for (std::string distrib : {"distr_hyperplane"}) {
			// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
				Container skyvec;
				Container skyvec_minimization;
				Point refpoint;
				if (distrib == "distr_hyperplane") {
					// utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
					utils::skyline_points_generator_gener_pos<T>(infilename, nbpoint, seed, 10);
				}
				if (distrib == "distr_convex") {
					utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
				}
				if (distrib == "distr_concave") {
					utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
				}
				utils::parse_file_to_list_of_arrays_size2(infilename, skyvec);
				std::sort(skyvec.begin(), skyvec.end(), [](Point lhs, Point rhs) {return lhs[0] < rhs[0];});

				
				utils::convert_maximization_to_positive_minimization_with_reference_array2d(skyvec, skyvec_minimization, refpoint);
				std::vector<shark_interface::VectorType2d> vect_shark = shark_interface::convert_vec_to_VectorType2d<Point>(skyvec_minimization);
				shark_interface::VectorType2d refpoint_shark(refpoint[0], refpoint[1]);

				
				for (int k : list_of_k) {
					for (int repet = 1; repet <= repet_max; ++repet) {
						print_running_time_hssp2d_b<Point>(k, skyvec, nbpoint, seed, repet, outfilename, "2d_extreme", distrib);
						print_running_time_hssp2d_b<Point>(k, skyvec, nbpoint, seed, repet, outfilename, "2d_smawk", distrib);
						// print_running_time_hssp2d<Point>(k, skyvec, nbpoint, seed, repet, outfilename, "2d_extreme_vo", distrib);
						// print_running_time_hssp2d<Point>(k, skyvec, nbpoint, seed, repet, outfilename, "2d_smawk_vo", distrib);
						print_running_time_hssp2d_shark_b<Point>(k, nbpoint, seed, repet, outfilename, vect_shark, refpoint_shark, distrib);
					}
				}
			}
		}
	}
}

}  // namespace experiment

#endif /* REQ1_VAR_NBPOINT_H_*/