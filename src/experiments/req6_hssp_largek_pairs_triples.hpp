/*
 * main.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: bgroz
 */

#ifndef REQ6_VAR_NBPOINT_H_
#define REQ6_VAR_NBPOINT_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>

#include "hssp_contribution/hypvcontributions.h"
#include "hssp2d/contrib_matrix.hpp"
#include "utils/generators.hpp"
#include "utils/parsers.hpp"
#include "utils/naive_implementations/naive_min_triple.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"
#include "weighted_hypergraph/min_triple.h"
#include "weighted_hypergraph/weighted_hypergraph.h"

namespace experiment {

template<typename Point, typename IndexType>
void print_running_time_minweight(std::vector<Point> skyvec, int seed, int nbpoint, int repet, 
std::string outfilename, std::string algo_name, std::string headersuffix) {
	typedef typename Point::value_type T;
	typedef typename hssp_contribution::ContribPoint<T> RCell;
	typedef typename std::unordered_map<std::pair<T,T>,RCell> ContainerOutput;
	typedef typename std::pair<std::vector<size_t>, ContainerOutput> ResContrib;
	typedef typename weighted_hypergraph::WGraph<T, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>> Graph;
	
	auto valid = utils::hssp3dk2_validate_input<Point, IndexType>(skyvec);
	if (valid.first) {
		std::string output;
		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(seed) + "," 
			+ std::to_string(repet) + "," + algo_name + "," + headersuffix;
		output = headerstring + "," + utils::current_time();
		int contribdepth = 3;
		
		auto t1 = utils::current_clock();
		ResContrib contribs = hssp_contribution::hypervolume_contributions<Point, ContainerOutput>(contribdepth, skyvec);
		auto t_step1 = utils::current_clock();
		Graph g(nbpoint);
		for (auto mapitem : contribs.second) {
			RCell c = mapitem.second;
			std::vector<unsigned long> s;
			unsigned long pt0 = c.up_points[0];
			T w0 = c.cumulative_vol[0];
			s = {pt0};
			g.add_hyperedge_sorted(s, w0);
			if (c.up_points.size() >= 2) {
				unsigned long pt1 = c.up_points[1];
				T w1 = c.cumulative_vol[1];
				s.push_back(pt1);
				std::sort(s.begin(), s.end());
				g.add_hyperedge_sorted(s, w1);
			}
			if (c.up_points.size() >= 3) {
				unsigned long pt2 = c.up_points[2];
				T w2 = c.cumulative_vol[2];
				s.push_back(pt2);
				std::sort(s.begin(), s.end());
				g.add_hyperedge_sorted(s, w2);
			}
		}
		auto t_step2 = utils::current_clock();
		if (algo_name == "naive_pair") {
			auto res = utils::compute_min_pair_naive<T, IndexType>(g);
		}
		else if (algo_name == "naive_triple") {
			auto res = utils::compute_min_triple_naive<T, IndexType>(g);
		}
		else if (algo_name == "fast_pair") {
			auto res = weighted_hypergraph::compute_min_pair<T, IndexType>(g);
		}
		else if (algo_name == "fast_triple") {
			auto res = weighted_hypergraph::compute_min_triple<T, IndexType>(g);
		}
		auto t2 = utils::current_clock();
		output += "," + utils::current_time();
		output += "," + utils::delay_between(t1, t2);
		output += "," + utils::delay_between(t1, t_step1);
		output += "," + utils::delay_between(t_step1, t_step2);
		output += "," + utils::delay_between(t_step2, t2);

		std::ofstream out(outfilename, std::ios::app);
		if (out) {
			out<< output<<std::endl;
			out.close();
		}
		else {
			std::cout<<"Pb: writing in file impossible";
			std::cout<<output;
		}
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<",seed:"<<seed<<",nbpoint:"<<nbpoint<<std::endl;
	}
}


void running_time_hssp_largek() {
	typedef double T;
	typedef unsigned long IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints_largek_varn.txt");
	std::string const outfilename("results_largek_varn.csv");
	int dim = 3;

	int nbpoint_max = 1e7;
	int repet_max = 1;
	int seed_max = 3;

	for (int nbpoint = 100; nbpoint <= nbpoint_max; nbpoint *= 2) {
		for (int seed = 1; seed <= seed_max; ++seed) {
			for (std::string distrib : {"distr_hyperplane"}) {
			// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
				Container skyvec;
				if (distrib == "distr_hyperplane") {
					utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
				}
				if (distrib == "distr_convex") {
					utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
				}
				if (distrib == "distr_concave") {
					utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
				}
				utils::parse_file_to_list_of_arrays_size3(infilename, skyvec);

				for (int repet = 1; repet <= repet_max; ++ repet) {
					if (nbpoint<=1e5) print_running_time_minweight<Point, IndexType>(skyvec, seed, nbpoint, repet, outfilename, "naive_pair", distrib);
					if (nbpoint<=5*1e6) print_running_time_minweight<Point, IndexType>(skyvec, seed, nbpoint, repet, outfilename, "fast_pair", distrib);
					if (nbpoint<=2*1e3) print_running_time_minweight<Point, IndexType>(skyvec, seed, nbpoint, repet, outfilename, "naive_triple", distrib);
					if (nbpoint<=5*1e5) print_running_time_minweight<Point, IndexType>(skyvec, seed, nbpoint, repet, outfilename, "fast_triple", distrib);
				}
			}
		}
	}
}

}  // namespace experiment

#endif /* REQ6_VAR_NBPOINT_H_*/