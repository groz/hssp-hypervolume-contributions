
/*
* main.cpp
*
*  Created on: Jul 13, 2016
*      Author: bgroz
*/

#ifndef REQ5s_H_
#define REQ5s_H_

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

#include <algorithm>
#include <fstream>
// #include <iterator>
#include <iomanip>
#include <iostream>
#include <limits>
#include <numeric> // used for iota
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "hssp_contribution/hypvcontributions_structure.h"
#include "hssp2d/contrib_matrix.hpp"
#include "hssp2d/hssp2d_extreme.hpp"
#include "hssp2d/hssp2d_smawk.hpp"
#include "shark_externals/interface_with_shark.hpp"
#include "shark_externals/sharklibrary_hssp2d.cpp"
#include "utils/generators.hpp"
#include "utils/naive_implementations/naive_hssp.h"
#include "utils/other_utils.hpp"
#include "utils/parsers.hpp"
#include "utils/time_utils.hpp"
#include "utils/validate_input.hpp"


namespace experiment {

template<typename Point, typename IndexType>
void print_running_time_contribs_struct(std::vector<Point> skyvec, int seed, int nbpoint, int nbrepet, 
int batchsize, int nbpoint_contrib, int contribdepth, std::string outfilename, std::string headersuffix) {
	typedef typename Point::value_type T;

	auto valid = utils::hssp3dk2_validate_input<Point, IndexType>(skyvec);
	if (valid.first) {
		std::string output;
		std::string headerstring = std::to_string(nbpoint) + "," + std::to_string(seed) + "," 
			+ std::to_string(nbrepet) + "," + std::to_string(contribdepth)+ ",hypvcontr_structure," + headersuffix;
		output = headerstring + "," + utils::current_time();

		auto t1 = utils::current_clock();
		auto c = hssp_contribution::Contribs_3d<T,Point>(contribdepth, skyvec);
		auto t2 = utils::current_clock();
		output += "," + utils::delay_between(t1, t2);


		std::vector<size_t> indices(skyvec.size());
		std::iota(indices.begin(), indices.end(), 0);
		std::vector<std::vector<size_t>> input_batch;
		for (int i = 0; i < batchsize; ++i) {
			std::random_shuffle(indices.begin(), indices.end());
			std::vector<size_t> vec;
			std::copy(indices.begin(), std::next(indices.begin(), nbpoint_contrib), std::back_inserter(vec));
			input_batch.push_back(vec);
		}
		std::vector<T> val;
		auto t3 = utils::current_clock();
		for (int i = 0; i < batchsize; ++i) {
			val.push_back(c.Contribs(input_batch[i]));
		}

		auto t4 = utils::current_clock();
		for (int i = 0; i < batchsize; ++i) {
//			std::cout<<val[i]<<std::endl;
		}
		std::cout<<"Time for "<<batchsize<<" contrib queries: "<<utils::delay_between(t3,t4)<<std::endl;
		output += "," + utils::delay_between(t1,t2);
//		for (auto c: contribs_v) {
//
//		}


		std::ofstream out(outfilename, std::ios::app);
		if (out) {
			out<< output<<std::endl;
			out.close();
		}
		else {
			std::cout<<"Pb: writing in file impossible";
			std::cout<<output;
		}
	}
	else {
		std::cout<<"Pb: Invalid input: "<<valid.second<<",seed:"<<seed<<",nbpoint:"<<nbpoint<<std::endl;
	}
}

void running_time_hsspd3_contrib_struct() {
	typedef double T;
	typedef int IndexType;
	typedef std::array<T, 3> Point;
	typedef std::vector<Point> Container;
	std::string const infilename("inputpoints.txt");
	std::string const outfilename("results.csv");
	int dim = 3;
	
	int nbpoint_max = 100000;
	int depth_max = 10000;
	int repet_max = 1;
	int seed_max = 3;
	int nbpointcontrib = 50;
	int batchsize = 100;
		
	for (int nbpoint = 1000; nbpoint <= nbpoint_max; nbpoint *= 10) {
		for (int contribdepth : {1000}) {
			for (int seed = 1; seed <= seed_max; ++seed) {
				for (std::string distrib : {"distr_hyperplane"}) {
				// for (std::string distrib : {"distr_hyperplane", "distr_convex", "distr_concave"}) {
					Container skyvec;
					std::vector<std::vector<T>> skyvecb;
					if (distrib == "distr_hyperplane") {
						utils::hyperplane_points_generator_gener_pos_dim_d(infilename, nbpoint, dim, seed, 10);
					}
					if (distrib == "distr_convex") {
						utils::convex_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					if (distrib == "distr_concave") {
						utils::concave_sphere_points_generator_dim_d(infilename, nbpoint, dim, seed);
					}
					utils::parse_file_to_list_of_arrays_size3(infilename, skyvec);
					utils::parse_file_to_list_of_vectors(infilename, skyvecb);
					for (int nbrepet = 1; nbrepet <= 1; ++ nbrepet) {
						print_running_time_contribs_struct<Point, IndexType>(skyvec, seed, nbpoint, nbrepet, batchsize, nbpointcontrib, contribdepth, outfilename, distrib);
					}
				}
			}
		}
	}
}

}  // namespace experiment

#endif /* REQ5s_H_*/
