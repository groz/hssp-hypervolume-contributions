/*
 * mintriangle.h
 *
 *  Created on: Aug 8, 2016
 *      Author: bgroz
 */

#ifndef MIN_TRIPLE_H_
#define MIN_TRIPLE_H_

#include <tuple>
#include <utility>
#include <vector>

#include "weighted_hypergraph/weighted_hypergraph.h"

namespace weighted_hypergraph {
// Returns a pair of distinct nodes with minimal weight.
// Output format is (weight of minimal pair, vector {id1,id2} representing the id of nodes in the pair).
// Complexity O(n\log n).
template<typename T, typename IndexType>
std::pair<T, std::vector<IndexType>> compute_min_pair(WGraph<T, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>> &g);


// Returns a triple of minimal weight
template<typename T, typename IndexType>
std::pair<T, std::vector<IndexType>> compute_min_triple(WGraph<T, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>> &g);

}  // namespace weighted_hypergraph
// ******************************************
// ***********  IMPLEMENTATION:  ************
#include "min_triple.hpp"

#endif /* MIN_TRIPLE_H*/
