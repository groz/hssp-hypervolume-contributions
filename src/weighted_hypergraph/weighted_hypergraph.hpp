/*
 * weightedhypergraphwedges.cpp
 *
 *  Created on: Sep 15, 2016
 *      Author: bgroz
 */


//#include "weighted_hypergraph.h"


#include <algorithm>
#include <iterator>
#include <limits>
#include <numeric>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <boost/functional/hash.hpp>

#include "utils/hash_functions.hpp"



namespace weighted_hypergraph{
	
template<typename T, typename IndexType, typename Edge, typename HEdge>
void WGraph<T, IndexType, Edge, HEdge>::add_hyperedge_sorted(std::vector<IndexType> hyp, T weight) {
	assert(hyp.size() >= 1 && hyp.size() <= 3);
	if (hyp.size() == 1) {
		auto n = *hyp.begin();
		nodes[n] = weight;
	}
	else if (hyp.size() == 2) {
		auto n1 = std::min(hyp[0], hyp[1]);
		auto n2 = std::max(hyp[0], hyp[1]);
		assert(edges.find({n1, n2}) == edges.end());
		edges.insert({{n1, n2}, weight});
		adj_nodes[n1].insert(n2);
		adj_nodes[n2].insert(n1);
	}
	else if (hyp.size() == 3) {
		std::sort(hyp.begin(), hyp.end());
		auto n1 = hyp[0];
		auto n2 = hyp[1];
		auto n3 = hyp[2];
		assert(hyperedges.find(std::tuple<IndexType,IndexType,IndexType>(n1, n2, n3)) == hyperedges.end());
		hyperedges.insert({std::tuple<IndexType,IndexType,IndexType>(n1, n2, n3), weight});
	}
}


template<typename T, typename IndexType, typename Edge, typename HEdge>
bool WGraph<T, IndexType, Edge, HEdge>::has_hyperedge(std::vector<IndexType> hyp) const {
	std::sort(hyp.begin(), hyp.end());
	auto n1 = hyp[0];
	auto n2 = hyp[1];
	auto n3 = hyp[2];
	return (hyperedges.find(std::tuple<IndexType,IndexType,IndexType>(n1, n2, n3)) != hyperedges.end());
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
bool WGraph<T, IndexType, Edge, HEdge>::has_hyperedge_sorted(HEdge &hyp) const {
	return (hyperedges.find(hyp) != hyperedges.end());
}



template<typename T, typename IndexType, typename Edge, typename HEdge>
bool WGraph<T, IndexType, Edge, HEdge>::has_edge_sorted(Edge e) const {
	return (edges.find(e) != edges.end());
}


template<typename T, typename IndexType, typename Edge, typename HEdge>
bool WGraph<T, IndexType, Edge, HEdge>::has_edge(IndexType n1, IndexType n2) const {
	return (adj_nodes[n1].find(n2) != adj_nodes[n1].end());
}


template<typename T, typename IndexType, typename Edge, typename HEdge>
std::vector<Edge> WGraph<T, IndexType, Edge, HEdge>::get_edges() const {
	std::vector<Edge> v;
	v.reserve(edges.size());
	for (auto pair : edges) {
		v.push_back(pair.first);
	}
	return v;
}


template<typename T, typename IndexType, typename Edge, typename HEdge>
std::vector<IndexType> WGraph<T, IndexType, Edge, HEdge>::get_nodes() {
	std::vector<IndexType> v(nodes.size());
	std::iota(v.begin(), v.end(), 0);
	return v;
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
std::vector<std::pair<IndexType,T>> WGraph<T, IndexType, Edge, HEdge>::get_nodes_with_weight() {
	std::vector<std::pair<IndexType,T>> v(nodes.size());
	int i = 0;
	for (T w : nodes) {
		v[i] = {i, w};
		++i;
	}
	return v;
}



template<typename T, typename IndexType, typename Edge, typename HEdge>
T WGraph<T, IndexType, Edge, HEdge>::get_h_weight_sorted(HEdge hyp) const{
	return hyperedges.at(hyp);
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
T WGraph<T, IndexType, Edge, HEdge>::get_n_weight(IndexType hyp) const {
	return nodes[hyp];
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
T WGraph<T, IndexType, Edge, HEdge>::get_e_weight(IndexType n1, IndexType n2) const {
	Edge h = {std::min(n1, n2), std::max(n1, n2)};
	return edges.at(h);
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
T WGraph<T, IndexType, Edge, HEdge>::get_e_weight_if_exists(IndexType n1, IndexType n2) const {
	Edge h = {std::min(n1, n2), std::max(n1, n2)};
	auto e = edges.find(h);
	if (e != edges.end()) {
		return e->second;
	}
	else {
		return 0;
	}
}


template<typename T, typename IndexType, typename Edge, typename HEdge>
T WGraph<T, IndexType, Edge, HEdge>::get_e_weight_sorted(Edge h) const {
	return edges.at(h);
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
int WGraph<T, IndexType, Edge, HEdge>::get_number_of_neighbors(IndexType node) const {
	return adj_nodes[node].size();
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
std::vector<IndexType> WGraph<T, IndexType, Edge, HEdge>::get_neighbors(IndexType node) const {
	return std::vector<IndexType> (adj_nodes[node].begin(), adj_nodes[node].end());
}

template<typename T, typename IndexType, typename Edge, typename HEdge>
IndexType WGraph<T, IndexType, Edge, HEdge>::number_of_nodes() const {
	return nodes.size();
}


template<typename T, typename IndexType, typename Edge, typename HEdge>
std::string WGraph<T, IndexType, Edge, HEdge>::to_string() {
	std::string s = "Nodes:\n";
	for (auto n : nodes) {
		s += std::to_string(n)+" ";
	}
	s += "\nEdges:\n";
	for (auto e : edges) {
		s += "edge: "+ std::to_string(e.first.first)+","+std::to_string(e.first.second)+" has weight: "+ std::to_string(e.second);
		s += "\n";
	}
	s += "\nHEdges:\n";
	for (auto e : hyperedges) {
		s += "edge: "+ std::to_string(std::get<0>(e.first))+","+std::to_string(std::get<1>(e.first))+","+std::to_string(std::get<2>(e.first))+" has weight: "+ std::to_string(e.second);
		s += "\n";
	}
	return s;
}


//TODO: corriger:
//vector vs tuple
//...

template<typename T, typename IndexType, typename Edge, typename HEdge>
std::pair<T, std::vector<IndexType>> WGraph<T, IndexType, Edge, HEdge>::get_min_weight_triangle_hyperedge() {
	T cost = std::numeric_limits<T>::max();
	bool assigned = false;
	IndexType node1;
	IndexType node2;
	IndexType node3;
	for (auto e : hyperedges) {
		auto n1 = std::get<0>(e.first);
		auto n2 = std::get<1>(e.first);
		auto n3 = std::get<2>(e.first);
		T tempcost = e.second
						+ nodes[n1] + nodes[n2] + nodes[n3]
						+ get_e_weight_if_exists(n1, n2) + get_e_weight_if_exists(n1, n3) + get_e_weight_if_exists(n2, n3);
		if (tempcost < cost) {
			cost = tempcost;
			node1 = n1;
			node2 = n2;
			node3 = n3;
			assigned = true;
		}
	}
	if (assigned) {
		return {cost, {node1, node2, node3}};
	}
	else {
		return {0,{}};
	}
}


}  // namespace weighted_hypergraph
