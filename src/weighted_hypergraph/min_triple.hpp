/*
 * min_triple.hpp
 *
 *  Created on: Aug 11, 2016
 *      Author: bgroz
 */


#include <algorithm>
#include <iterator>
#include <limits>
#include <tuple>
#include <utility>
#include <unordered_map>
#include <vector>

#include "utils/hash_functions.hpp"
#include "weighted_hypergraph/weighted_hypergraph.h"


namespace weighted_hypergraph {
	
template<typename T, typename IndexType>
std::pair<T, std::vector<IndexType>> compute_min_pair(WGraph<T, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>> &g) {
	T minimal_cost = std::numeric_limits<T>::max();
	IndexType node1 = 0;
	IndexType node2 = 0;

	if(g.number_of_nodes() >= 2) {
		std::vector<IndexType> nodeset = g.get_nodes();  // Copying g_nodeset so that it is sorted once and for all by node weight.
		std::sort(nodeset.begin(), nodeset.end(), [&g](IndexType i, IndexType j) {return g.get_n_weight(i) < g.get_n_weight(j);});
		// nodeset records all nodes sorted by increasing weight.


		if (!g.has_edge(*nodeset.begin(), *std::next(nodeset.begin(),1))) {
			node1 = *nodeset.begin();
			node2 = *std::next(nodeset.begin());
			minimal_cost = g.get_n_weight(node1) + g.get_n_weight(node2);
		}
		else {
			for (auto n1 = nodeset.begin(); n1 != nodeset.end(); ++n1) {
				auto n2 = std::next(n1, 1);
				while(n2 != nodeset.end() && g.has_edge(*n1, *n2)) {
					// We first check all edges (n1, x) that might be better than the best independent pair (n1, x)
					auto candidate_cost = g.get_n_weight(*n1) + g.get_n_weight(*n2) + g.get_e_weight(*n1, *n2);
					// Technically has_edge and get_e_weight do twice the job. Could be optimized with edges.find() if we we make edges public. But not worth it.
					if (candidate_cost < minimal_cost){
						minimal_cost = candidate_cost;
						node1 = *n1;
						node2 = *n2;
					}
					++n2;
				}
				// We next compute the best independent pair (n1, x)
				if (n2 != nodeset.end()) {
					auto candidate_cost = g.get_n_weight(*n1) + g.get_n_weight(*n2);
					if (candidate_cost < minimal_cost){
						minimal_cost = candidate_cost;
						node1 = *n1;
						node2 = *n2;
					}
				}
			}
		}
	}
	return {minimal_cost, {node1, node2}};
}



// Assumes 3-hyperedges induce at least 2 edges.
template<typename T, typename IndexType>
std::pair<T, std::vector<IndexType>> compute_min_triple(WGraph<T, IndexType, std::pair<IndexType, IndexType>, std::tuple<IndexType, IndexType, IndexType>> &g) {
	typedef typename std::tuple<IndexType, IndexType, IndexType> HEdge;
	int m = round(sqrt(g.number_of_nodes()));
	// nodeset records all nodes sorted by increasing node weight.
	std::vector<IndexType> nodeset = g.get_nodes();
	std::sort(nodeset.begin(), nodeset.end(), [&g](IndexType i, IndexType j) {return g.get_n_weight(i) < g.get_n_weight(j);});
	std::vector<IndexType> heavy_nodes;
	std::vector<IndexType> light_nodes;
	// Let E the set of edges (pair of nodes), V the vertices.
	// We first compute:
	//   - heavy nodes: having >sqrt(n) neighbors. There are at most 2 sqrt(|E|) such nodes.
	//   - light nodes: the other nodes. Most nodes are light.
	// Our insertion order guarantees that light_nodes are sorted. (actually, heavy ones too but this property is not exploited by our algorithm)
	for (auto n : nodeset) {
		if (g.get_number_of_neighbors(n) > m) {
			heavy_nodes.push_back(n);
		}
		else {
			light_nodes.push_back(n);
		}
	}
	
	// Next we do case analysis on number k of heavy nodes and h of edges:
	// h=0:
	// >>  k=0
	// >>  k=1,2 or3
	// h=1,2 or 3:
	// >>  there is a heavy vertex opposite an edge
	// >>  there is no heavy vertex opposite an edge:
	// >>>>  both edge extremities are light (hence k=0 since third vertex must be light by assumption)
	// >>>>  (exactly) 1 edge extremity is light, h=2
	// >>>>  0 or 1 edge extremity is light, h=1
	T minimal_cost = std::numeric_limits<T>::max();
	IndexType node1 = 0;
	IndexType node2 = 0;
	IndexType node3 = 0;



	//	// Case h=0, k=3. Complexity O(n^1.5), could be improved with matrix multiplication.
	//	// this case is now covered below.
	//	for (auto n1 = heavy_nodes.begin(); n1 != heavy_nodes.end(); ++n1) {
	//		for (auto n2 = std::next(n1, 1); n2 != heavy_nodes.end(); ++n2) {
	//			for (auto n3 = std::next(n2, 1); n3 != heavy_nodes.end(); ++n3) {
	//				if (!g.has_edge(*n1, *n2) && !g.has_edge(*n1, *n3) && !g.has_edge(*n2, *n3)) {
	//					//i.e., if nodes are independent:
	//					//in our setting we know that in that case there is also no hyperedge (n1, n2, n3)
	//					T weight = g.get_n_weight(*n1) + g.get_n_weight(*n2) + g.get_n_weight(*n3);
	//					if (weight<cost) {
	//						cost = weight;
	//						node1 = *n1;
	//						node2 = *n2;
	//						node3 = *n3;
	//					}
	//				}
	//			}
	//		}
	//	}

	// Datastructures used for the following cases.
	// neighbours(n1) records nodes that are neighbours of n1, sorted by increasing weight together with n1.
	std::unordered_map<IndexType, std::vector<IndexType>> neighbours;
	for (auto n1 : nodeset) {
		std::vector<IndexType> nb = g.get_neighbors(n1);
		std::sort(nb.begin(), nb.end(), [&](IndexType i, IndexType j) {
			return g.get_n_weight(i) + g.get_e_weight(n1, i) < g.get_n_weight(j) + g.get_e_weight(n1, j);});
		neighbours.insert({n1, nb});
	}

	// Case h=0, k=0. Complexity O(n^1.5), could be improved with matrix multiplication.
	int i = 0;
	//	std::cout<<to_string<std::vector<int>>(light_nodes)<<std::endl;
	for (auto n1 = light_nodes.begin(); n1 != light_nodes.end() && i < 2*m; ++n1) {
		++i;
		// i measures the distance between light_nodes.begin() and n1
		// so we could use a comma operator in the loop's increment. Same for j, l with n2, n3.
		// But this style seems more readable according to
		// http://www.stroustrup.com/JSF-AV-rules.pdf AV Rule 199.
		int j = 0;
		for (auto n2 = std::next(n1, 1); n2 != light_nodes.end() && j < 2*m; ++n2) {
			++j;
			int l = 0;
			for (auto n3 = std::next(n2, 1); n3 != light_nodes.end() && l <= 2*m; ++n3) {
				++l;
				if (!g.has_edge(*n1, *n2) && !g.has_edge(*n1, *n3) && !g.has_edge(*n2, *n3)) {
					// I.e., if nodes are independent:
					// in our setting we know that in that case there is also no hyperedge (n1, n2, n3).
					T weight = g.get_n_weight(*n1) + g.get_n_weight(*n2) + g.get_n_weight(*n3);
					if (weight < minimal_cost) {
						minimal_cost = weight;
						node1 = *n1;
						node2 = *n2;
						node3 = *n3;
					}
				}
			}
		}
	}

	// Case h=0, k=1 or k=2 or k=3. Complexity O(n^1.5), could be improved with smarter choice of m for heavy/light.
	for (auto n1 : heavy_nodes) {
		T part_cost_case_h0k123 = std::numeric_limits<T>::max();//records cost owed to the two other vertices (i.e., not including the cost of n1).
		IndexType cand2 = 0;
		IndexType cand3 = 0;
		bool found = false;
		//		std::cout<<"self edge"<<g.has_edge(n1,n1)<<std::endl;
		// Computing the best pair for n1.
		// Nonneighbours records nodes that are not neighbours of n1, sorted by increasing weight.
		std::vector<IndexType> nonneighbours;
		std::copy_if(nodeset.begin(), nodeset.end(), std::back_inserter(nonneighbours), [&](IndexType i) {return !g.has_edge(i, n1) && i != n1;});
		for (auto n2 = nonneighbours.begin(); n2 != nonneighbours.end(); ++n2) {
			auto n3 = std::next(n2, 1);
			// Here we should in general also iterate further if there is a 3-hyperedge
			// but in our setting there cannot be a 3-hyperedge because (n2, n3) is the only edge
			// and a 3-hyperedge induces at least 2 edges.
			// so for out hypervolume problem we need not test the hyperedge here.
			while (n3 != nonneighbours.end() && (g.has_edge(*n2, *n3) || g.has_hyperedge({n1,*n2,*n3}))) {
				//			while (n3 != nonneighbours.end() && g.has_edge(*n2, *n3)) { // Sufficient in the hypervolume setting.
				++n3;
			}
			if (n3 != nonneighbours.end() && (g.get_n_weight(*n2) + g.get_n_weight(*n3)) < part_cost_case_h0k123) {
				part_cost_case_h0k123 = g.get_n_weight(*n2) + g.get_n_weight(*n3);
				cand2 = *n2;
				cand3 = *n3;
				found = true;
			}
		}
		// At this point part_cost+g.get_n_weight(n1) is the minimal weight for a triple of independent nodes including n1.
		if (found && g.get_n_weight(n1) + part_cost_case_h0k123 < minimal_cost) {
			minimal_cost = g.get_n_weight(n1) + part_cost_case_h0k123;
			node1 = n1;
			node2 = cand2;
			node3 = cand3;
		}
	}

	// Case h=1 or 2 or 3, without hyperedge. Complexity O(n^1.5), could be improved with smarter choice of m for heavy/light.
	for (auto e : g.get_edges()) {
		
		IndexType n1 = e.first;
		IndexType n2 = e.second;
		T partial_cost_case_no_hyp = g.get_n_weight(n1) + g.get_n_weight(n2) + g.get_e_weight_sorted(e);
		if (partial_cost_case_no_hyp < minimal_cost) {  // pruning (no impact on asymptotic complexity but heuristic optimization to avoid performing many operations below for nothing)

			// Dealing with cases where triple features a heavy vertex opposite an edge (other edges may be included but no triple contrib).
			// Covers 1i, 2i, subcases of 1ii, 2ii, 2iii (all subcases where one of the unknown vertices is heavy), and 3.
			for (auto n3 : heavy_nodes) {
				T part_cost = partial_cost_case_no_hyp + g.get_n_weight(n3);
				std::vector<IndexType> candidsort = {n1, n2, n3};
				std::sort(candidsort.begin(), candidsort.end());
				HEdge candid(candidsort[0], candidsort[1], candidsort[2]);
				if (part_cost < minimal_cost && n3 != n1 && n3 != n2 && !g.has_hyperedge_sorted(candid)) { // Check parcost< cost: purely heuristic optimization.
					part_cost += g.get_e_weight_if_exists(n1, n3);
					part_cost += g.get_e_weight_if_exists(n2, n3);
					// At this point part_cost records the cost of best triple (n1, n2, n3)
					// where n3 heavy, and at least one edge (n1,n2).
					if (part_cost < minimal_cost) {
						minimal_cost = part_cost;
						node1 = n1;
						node2 = n2;
						node3 = n3;
					}
				}
			}

			// Dealing with remaining cases of 1ii, 2ii: triple does not have a heavy vertex opposite an edge.
			// First we deal with case where the 3 vertices are light.
			if (g.get_number_of_neighbors(n1) <= m && g.get_number_of_neighbors(n2) <= m) {
				
				// Here we could iterate n3 over nodeset instead of light_nodes. Complexity would be the same.
				auto n3 = light_nodes.begin();
				// We first deal with the case 3light vertices, at least 2 edges.
				while (n3 != light_nodes.end() && (g.has_edge(n1, *n3) || g.has_edge(n2, *n3) || n1 == *n3 || n2 == *n3)) {
					// We stop as soon as n3 satisfies none of the 4 conditions above right (.e., a distinct node n3 having edges with neither n1 nor n2) (case 1ii below) because remaining candidates for n3 will be worse.
					// Covers remaining subcase of 2ii and 3: all 3 vertices are light (we could also allow n3 to be heavy in which case it would be redundant).
					if (n1 != *n3 && n2 != *n3) {
						std::vector<IndexType> candidsort = {n1, n2, *n3};
						std::sort(candidsort.begin(), candidsort.end());
						HEdge candid(candidsort[0], candidsort[1], candidsort[2]);
						if (!g.has_hyperedge_sorted(candid)) {
							T part_cost = partial_cost_case_no_hyp + g.get_n_weight(*n3) + g.get_e_weight_if_exists(n1, *n3) + g.get_e_weight_if_exists(n2, *n3);
							if (part_cost < minimal_cost) {
								minimal_cost = part_cost;
								node1 = n1;
								node2 = n2;
								node3 = *n3;
							}
						}
					}
					++n3;
					// Here we should in general also iterate further if there is a 3-hyperedge
					// but in our setting there cannot be a 3-hyperedge because (n1, n2) is the only edge
					// and a 3-hyperedge induces at least 2 edges.
				}

				// Remaining Subcase of 1ii: the 3 vertices are light but there is only one edge (hence no edge (n1, n3) nor (n2,n3))
				if (n3 != light_nodes.end() && (partial_cost_case_no_hyp + g.get_n_weight(*n3)) < minimal_cost) {
					minimal_cost = partial_cost_case_no_hyp + g.get_n_weight(*n3);
					node1 = n1;
					node2 = n2;
					node3 = *n3;
				}
			}
			else {
				
				// Case 2iii: exactly one of the nodes n1, n2 is light;
				// i.e., exactly one of (g.get_number_of_neighbors(n1) <= m) or (g.get_number_of_neighbors(n2) <= m) is true.
				if (g.get_number_of_neighbors(n2) <= m) {  // Swapping n1, n2 so that n1 is the light one, and n2 the heavy one (this allows to factorize code).
					std::swap(n1, n2);
				}
				if (g.get_number_of_neighbors(n1) <= m) {
					// Here it would be sufficient to iterate n3 over light neighbours. But it has same complexity anyway (and we did not bother to precompute light_neighbours).
					auto n3 = neighbours.at(n2).begin();
					while (n3 != neighbours.at(n2).end() && (g.has_edge(n1, *n3) || g.has_hyperedge({n1, n2, *n3}) || *n3 == n1 || *n3 == n2)) {
						++n3;  // Skip without evaluating such triples because the heavy vertex n2 is opposite an edge, so case already considered.
					}
					if (n3 != neighbours.at(n2).end() && !g.has_hyperedge({n1, n2, *n3}) && g.has_edge(n2, *n3)) {
						auto candidate_cost = partial_cost_case_no_hyp + g.get_n_weight(*n3) + g.get_e_weight(n2, *n3);
						if (candidate_cost < minimal_cost){
							minimal_cost = candidate_cost;
							node1 = n1;
							node2 = n2;
							node3 = *n3;
						}
					}
				}
			}
		}
	}

	// Case: h=1, and 0 or 1 edge extremity is light, and the vertex opposite the edge is light.
	for (auto n1 : heavy_nodes) {
		// Case 1iii:
		for (auto n2 : light_nodes) {
			if (!g.has_edge(n1, n2)) {
				T cost_case1iii = g.get_n_weight(n1) + g.get_n_weight(n2);
				auto n3 = neighbours.at(n1).begin();
				while (n3 != neighbours.at(n1).end() && (g.has_edge(n2, *n3) || *n3 == n2)) {
					++n3;
				}
				// Here we should in general also iterate further if there is a 3-hyperedge
				// but in our setting there cannot be a 3-hyperedge because (n1, n3) is the only edge
				// and a 3-hyperedge induces at least 2 edges.
				if (n3 != neighbours.at(n1).end() && (cost_case1iii + g.get_n_weight(*n3) + g.get_e_weight(n1, *n3) < minimal_cost)) {
					minimal_cost = cost_case1iii + g.get_n_weight(*n3) + g.get_e_weight(n1, *n3);
					node1 = n1;
					node2 = n2;
					node3 = *n3;
				}
			}
		}
	}

	// Case hyperedge.
	auto res = g.get_min_weight_triangle_hyperedge();
	if (res.second.size() > 0 && res.first < minimal_cost) {
		minimal_cost = res.first;
		node1 = res.second[0];
		node2 = res.second[1];
		node3 = res.second[2];
	}
	return {minimal_cost, {node1, node2, node3}};
}

}  // namespace weighted_hypergraph
