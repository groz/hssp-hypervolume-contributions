/*
 * weighted_hypergraph.h
 *
 *  Created on: Jul 21, 2016
 *      Author: bgroz
 */





#ifndef WGRAPH_H_
#define WGRAPH_H_


#include <algorithm>
#include <numeric>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <boost/functional/hash.hpp>

#include "utils/hash_functions.hpp"

namespace weighted_hypergraph {
// Class implementing (undirected) weighted hypergraphs.
// Every point and relation tuple is represented as an IndexType through hashing (boost hashing for vectors).
// T is the type of hyperedge weights
// IndexType is the type of the id we use to represent a vertex.
// typically, IndexType = unsigned long, T is float.
// This implementation assumes the following types for edges and hyperedges.
//    typedef std::pair<IndexType, IndexType> Edge;
//    typedef std::tuple<IndexType, IndexType, IndexType> HEdge;
// TODO: fix either by generalizing code or using typedef instead of template parameters.
template<typename T, typename IndexType, typename Edge, typename HEdge>
class WGraph{
//	typedef std::pair<IndexType, IndexType> Edge;
//	typedef std::tuple<IndexType, IndexType, IndexType> HEdge;
public:
	std::vector<T> nodes;  // In our case, nodes are consecutive integers {0,...} fixed at initialization. Without this guarantee we should use std::unordered_map<IndexType,T>.
	std::vector<std::unordered_set<IndexType>> adj_nodes;  // Nodes adjacent through normal edge. Again, replace with std::unordered_map<IndexType,T> if nodes are arbitrary.
	std::unordered_map<Edge, T> edges;
	std::unordered_map<HEdge, T> hyperedges;
	// Creates a "Perfect hash" of hyperedge.
public:
	WGraph<T, IndexType, Edge, HEdge>(int nb_nodes) {
		nodes = std::vector<T>(nb_nodes, 0);
//        std::iota(nodes.begin(), nodes.end(), 0);
		adj_nodes = std::vector<std::unordered_set<IndexType>>(nb_nodes);
	}

	// Assumes vector hyp is sorted by increasing order.
	// And assumes nodes in hyperedge belong to {0,...,nodes.size()-1}
	// Adds the hyperedge, and its nodes, if not already present (otherwise, skips).
	// Complexity: O(1) for constant-size hyp; currently even  O(|hyp|).
	void add_hyperedge_sorted(std::vector<IndexType> hyp, T weight);

	// Assumes vector hyp is sorted in increasing order.
	// Complexity: O(|hyp|).
	bool has_hyperedge_sorted(HEdge &hyp) const;

	// Complexity: O(|hyp| log |hyp|).
	bool has_hyperedge(std::vector<IndexType> hyp) const;


	// Order does not matter here.
	// Complexity: O(1).
	bool has_edge_sorted(Edge e) const;
	
	// Order does not matter here.
	// Complexity: O(1).
	bool has_edge(IndexType n1, IndexType n2) const;

	// Returns edges as pairs in which the first is the smallest.
	// Complexity: O(|graph|).
	std::vector<Edge> get_edges() const;

    // Returns a container with all node Ids in sorted order: 0,1,...,nodes.size()-1
	// Complexity: O(n).
	std::vector<IndexType> get_nodes();

	// Returns a container with all node Ids.
	// Complexity: O(n).
	std::vector<std::pair<IndexType,T>> get_nodes_with_weight();

	// Assumes vector hyp is sorted in increasing order.
	// Returns weight of hyperedge.
	// Complexity: O(|hyp|).
	T get_h_weight_sorted(HEdge hyp) const;


	// Returns node_weight.
	// Assumes node exists.
	// Complexity: O(1).
	T get_n_weight(IndexType hyp) const;

	// Returns edge_weight.
	// Assumes edge exists.
	// Order does not matter.
	// Complexity: O(1).
	T get_e_weight(IndexType n1, IndexType n2) const;

	// Returns edge_weight.
	// If edge does not exist, returns 0.
	// Order does not matter.
	// Complexity: O(1).
	T get_e_weight_if_exists(IndexType n1, IndexType n2) const;

	T get_e_weight_sorted(Edge e) const;

// // Returns edge_weight if exists, 0 otherwise.
// 	// Order does not matter.
// 	T get_e_weight_if_exists(IndexType n1, IndexType n2) const;
//
// 	// Computes a 3hyperedge with minimal total weight.
// 	// Complexity linear.
	std::pair<T, std::vector<IndexType>> get_min_weight_triangle_hyperedge();

	// Complexity: O(1).
	int get_number_of_neighbors(IndexType node) const;

	// Complexity: linear in number of neighbors.
	// Is actually used only to build a map that records for every node its neighbors sorted by some specific order.
	std::vector<IndexType> get_neighbors(IndexType node) const;

	IndexType number_of_nodes() const;

	std::string to_string();
};

}  // namespace weighted_hypergraph

// ******************************************
// ***********  IMPLEMENTATION:  ************
#include "weighted_hypergraph.hpp"

#endif /* WGRAPH_H_ */

